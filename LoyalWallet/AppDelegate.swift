//
//  AppDelegate.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 14/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import CoreData
import GoogleSignIn
import UserNotifications
import GoogleMaps
import Firebase
import FBSDKCoreKit
import FBSDKLoginKit
import EasyRealm
import UserNotificationsUI
import SwiftySound

protocol PushDelegate {
    func appDelegate(shouldRefreshWallet:Bool)
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,  UNUserNotificationCenterDelegate, MessagingDelegate {

    var window: UIWindow?
    var walletVC = WalletViewController()
    var delegate : PushDelegate?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        ERManager.realmConfig()
        Utils.initialSetup()

        if #available(iOS 10.0, *) {
            let center  = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                if error == nil{
                    DispatchQueue.main.async(execute: {
                        application.registerForRemoteNotifications()
                    })
                }
            }
            UNUserNotificationCenter.current().delegate = self
        } else {
            let notificationTypes: UIUserNotificationType = [.sound, .alert, .badge]
            let notificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
            application.registerForRemoteNotifications()
            application.registerUserNotificationSettings(notificationSettings)
        }
        
        application.registerForRemoteNotifications()
        
        Messaging.messaging().delegate = self
        let token = Messaging.messaging().fcmToken
        //print("FCM token: \(token ?? "")")
        
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            
            if currentUser.phoneVerified {
                let vc = R.Storyboards.register.instantiateViewController(withIdentifier: "NominatePinViewController") as! NominatePinViewController
                let navController = UINavigationController(rootViewController: vc)
                Utils.setRootViewController(navController)
            } else {
                UserSession.sharedInstance.isMobileVerified = false
                let vc = R.Storyboards.register.instantiateViewController(withIdentifier: "RegisterCarouselViewController") as! RegisterCarouselViewController
                let navController = UINavigationController(rootViewController: vc)
                Utils.setRootViewController(navController)
            }
            
        } else {
            let vc = R.Storyboards.register.instantiateViewController(withIdentifier: "RegisterCarouselViewController") as! RegisterCarouselViewController
            let navController = UINavigationController(rootViewController: vc)
            Utils.setRootViewController(navController)
        }
        
        return true
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        //print("Firebase registration token: \(fcmToken)")
        
        UserDefaults.standard.set("\(fcmToken)", forKey: R.CacheKeys.fcmToken)
        UserDefaults.standard.synchronize()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
        LoginHelper.getUserProfile()
        
        Sound.play(file: "notification_sound", fileExtension: "mp3", numberOfLoops: 0)
        
    }

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken as Data
        print(deviceToken.base64EncodedString())
        UserDefaults.standard.set("\(deviceToken.base64EncodedString())", forKey: R.CacheKeys.deviceToken)
        UserDefaults.standard.synchronize()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        UserSession.sharedInstance.userShouldRefreshTrans = true
        UserSession.sharedInstance.userShouldRefreshBalance = true
        
        if application.applicationState == .active {
            
        } else {
            UserDefaults.standard.set(true, forKey: R.CacheKeys.shouldUpdate)
            UserDefaults.standard.synchronize()
        }
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        if let _ = RealmManager.realm?.objects(User.self).first {
            
        } else {
            return
        }
        UserDefaults.standard.set(Date(), forKey: R.CacheKeys.backgroundTimestamp)
        UserDefaults.standard.synchronize()
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.

        if let _ = RealmManager.realm?.objects(User.self).first {
            
        } else {
            return
        }
        
        let lastDate = UserDefaults.standard.object(forKey: R.CacheKeys.backgroundTimestamp) as? Date
        let elapsed = Date().timeIntervalSince(lastDate!)
        let duration = Int(elapsed)
        
        if duration >= 30 {
            if let topViewController = Utils.topViewController() {
                if topViewController.isKind(of: NominatePinViewController.self) {
                    return
                }
                
                let vc = R.Storyboards.register.instantiateViewController(withIdentifier: "NominatePinViewController") as! NominatePinViewController
                vc.isLockOut = true
                topViewController.present(vc, animated: false, completion: nil)
            }
        }
        
        LoginHelper.getUserProfile()
//        if let shouldUpdate : Bool = UserDefaults.standard.object(forKey: R.CacheKeys.shouldUpdate) as? Bool, shouldUpdate {
//            UserDefaults.standard.set(false, forKey: R.CacheKeys.shouldUpdate)
//            UserDefaults.standard.synchronize()
//
//        }
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        self.saveContext()
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        let handled = FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation) || GIDSignIn.sharedInstance().handle(url,sourceApplication: sourceApplication, annotation: annotation)
        
        return handled
    }
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "LoyalWallet")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    func setStatusBarColorRGB(_ redF: Float, _ greenF: Float, _ blueF: Float){
        //UIApplication.shared.statusBarView?.backgroundColor = UIColor.init(colorLiteralRed: redF/255.0, green: greenF/255.0, blue: blueF/255.0, alpha: 1)
        //UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
    }
    
    // MARK: - Core Data Saving support
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}
