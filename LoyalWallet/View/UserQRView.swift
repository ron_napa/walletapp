//
//  UserQRView.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 25/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import QRCode

protocol UserQRViewDelegate {
    func userQRView(_ viewController: UserQRView, didCloseQR: Bool)
}

class UserQRView: UIView {

    @IBOutlet weak var userQRImageView: UIImageView!
    var tapGesture = UITapGestureRecognizer()
    var delegate: UserQRViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            let fullAddres = "{ \"lylAddress\" : \"\(currentUser.lylAddress ?? "")\", \"customerId\": \"\(currentUser.id)\" }"
            let qrCode = QRCode(fullAddres)
            self.userQRImageView.image = qrCode?.image
        }
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(UserQRView.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        self.addGestureRecognizer(tapGesture)
        self.isUserInteractionEnabled = true
        
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        //self.removeFromSuperview()
        delegate?.userQRView(self, didCloseQR: true)
        let viewController = self.parentViewController
        viewController?.dismiss(animated: false, completion: nil)
    }

}

extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
}
