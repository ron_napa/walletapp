//
//  DatePickerView.swift
//  GlobeAtHome
//
//  Created by Ronald Napa on 19/10/2017.
//  Copyright © 2017 Globe Telecom, Inc. All rights reserved.
//

import UIKit

@objc protocol DatePickerViewDelegate {
    @objc optional func datePickerView(_ datePickerView: DatePickerView,
                                              didTapButton button: UIButton!)
    @objc optional func datePickerView(_ datePickerView: DatePickerView,
                                       didChangeDate datePicker: UIDatePicker!)
    @objc optional func datePickerView(_ datePickerView: DatePickerView,
                                       didShow shown: Bool)
    @objc optional func datePickerView(_ datePickerView: DatePickerView,
                                       didHide hidden: Bool)
}

class DatePickerView: UIView {

    weak var delegate: DatePickerViewDelegate!
    @IBOutlet weak fileprivate var okButton: UIButton!
    @IBOutlet weak fileprivate var cancelButton: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!
    var isShown : Bool?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        okButton.tag = 1
        cancelButton.tag = 2
        
        datePicker.locale = Locale(identifier : "EN")
        datePicker.minimumDate = "12/31/1901".dateValue(format: "MM/DD/YYYY")
        datePicker.maximumDate = "01/01/2101".dateValue(format: "MM/DD/YYYY")
        isShown = false
    }
    

    //MARK: - Showing Self
    func addDatePicker(parentView: UIView) {
        
        self.frame.origin = CGPoint(x: 0, y: parentView.frame.height)
        self.frame.size.height = 200
        self.frame.size.width = Device.Size.width
        parentView.addSubview(self)
        
    }
    
    func showDatePicker(selectedDate:Date){
        if !isShown! {
            delegate.datePickerView!(self, didShow: true)
            datePicker.date = selectedDate
            isShown = true
            self.frame.size.height = 200
            UIView.animate(withDuration: 0.2, animations: {
                self.frame.origin = CGPoint(x: 0, y: (self.superview?.frame.height)! - self.frame.size.height)
            })
        }
    }
    
    func hideDatePicker(){
        delegate.datePickerView!(self, didHide: true)
        isShown = false
        UIView.animate(withDuration: 0.2, animations: {
            self.frame.origin = CGPoint(x: 0, y: (self.superview?.frame.height)!)
        })
    }
    
    func removeDatePicker() {
        self.removeFromSuperview()
    }
    
    //MARk: - User Interaction
    @IBAction func didTappedCancel(_ sender: Any) {
        delegate.datePickerView!(self, didTapButton: sender as! UIButton)
        hideDatePicker()
    }
    
    @IBAction func didTappedOk(_ sender: Any) {
        delegate.datePickerView!(self, didTapButton: sender as! UIButton)
        delegate.datePickerView!(self, didChangeDate: datePicker)
        hideDatePicker()
    }
    
}
