//
//  LoginHelper.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 13/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SwiftSpinner
import SwiftyJSON
import Realm
import RealmSwift

class LoginHelper: NSObject {

    class func loginUser(viewController: UIViewController, willVerify: Bool = false) {
        if let currentUser = RegisterSession.sharedInstance.currentUser {
            RegisterService.login(mobileNumber: currentUser.mobileNumber!, password: currentUser.password!, completion: { (response, error) in
                print(response as Any)
                if error != nil {
                    SwiftSpinner.hide()
                    if let topController = Utils.topViewController() {
                        //AlertviewManager.showAlertInfo(message: (error?.description.description)!, view: topController)
                    }
                } else {
                    if response!["code"].int == 200 || response!["code"].int == 458{
                        let userResponse = response!["user"]
                        //updateUser(userResponse: userResponse)
                        print(userResponse as Any)
                        
                        LoginHelper.clearUserSession()
                        
                        //User(info: userResponse.dictionaryValue)
                        let user = User()
                        user.id = userResponse["cust_id"].int ?? 0
                        user.mobileNo = userResponse["mobile_no"].string ?? ""
                        user.email = userResponse["email"].string ?? ""
                        user.profileImage = userResponse["profile_image"].string ?? ""
                        user.activationCode = userResponse["activation_code"].string ?? ""
                        user.pinCode = userResponse["pin_code"].string ?? ""
                        user.lylAddress = userResponse["lyl_address"].string ?? ""
                        user.lylBalance = userResponse["lyl_balance"].double ?? 0.0
                        user.firstName = userResponse["first_name"].string ?? ""
                        user.middleName = userResponse["middle_name"].string ?? ""
                        user.lastName = userResponse["last_name"].string ?? ""
                        user.gender = userResponse["gender"].string ?? ""
                        user.dateOfBirth = userResponse["dateofbirth"].string ?? ""
                        user.birthPlace = userResponse["birthplace"].string ?? ""
                        user.nationality = userResponse["nationality"].string ?? ""
                        user.registeredLocation = userResponse["reg_location"].string ?? ""
                        user.lastTransactionDate = userResponse["last_transaction_date"].string ?? ""
                        user.phoneVerified = userResponse["phone_verified"].int == 1 ? true : false
                        user.emailVerified = userResponse["email_verified"].int == 1 ? true : false
                        user.addressVerified = userResponse["address_verified"].int == 1 ? true : false
                        user.selfieVerified = userResponse["selfie_verified"].int == 1 ? true : false
                        user.indentityVerified = userResponse["indentity_verified"].int == 1 ? true : false

                        RealmManager.save(object: user)
                        
                        UserDefaults.standard.set("\(response!["token"])", forKey: R.CacheKeys.userToken)
                        UserDefaults.standard.synchronize()
                        
                        UserDefaults.standard.set(0 , forKey: R.CacheKeys.exhaustCount + user.mobileNo!)
                        UserDefaults.standard.synchronize()
                        
                        RegisterService.addDownload(email: userResponse["email"].string ?? "", completion: { (response, error) in
                            print(response as Any)
                            print(error as Any)
                            if error != nil {
                                //AlertviewManager.showAlertInfo(message: (error?.description)!, view: viewController)
                            } else {
                                if willVerify {
                                    RegisterService.mobileSendCode(mobileNumber: currentUser.mobileNumber!, completion: { (response, error) in
                                        SwiftSpinner.hide()
                                        if error != nil {
                                            //AlertviewManager.showAlertInfo(message: (error?.description)!, view: viewController)
                                        } else {
                                            showPinVerification(viewController: viewController)
                                            //viewController.performSegue(withIdentifier: "verificationPinSegue", sender: self)
                                        }
                                    })
                                } else {
                                    SwiftSpinner.hide()
                                    self.showMainDashBoard()
                                }
                            }
                        })
                        
                    } else {
                    
                        SwiftSpinner.hide()
                        AlertviewManager.showAlertInfo(message: response!["message"].string!, view: viewController)
                        
                    }
                }
            })
        }
    }
    
    class func updateUser(userResponse: JSON) {
        
        let user = User()
        user.id = userResponse["cust_id"].int ?? 0
        user.mobileNo = userResponse["mobile_no"].string ?? ""
        user.email = userResponse["email"].string ?? ""
        user.profileImage = userResponse["profile_image"].string ?? ""
        user.activationCode = userResponse["activation_code"].string ?? ""
        user.pinCode = userResponse["pin_code"].string ?? ""
        user.lylAddress = userResponse["lyl_address"].string ?? ""
        user.lylBalance = userResponse["lyl_balance"].double ?? 0.0
        user.firstName = userResponse["first_name"].string ?? ""
        user.middleName = userResponse["middle_name"].string ?? ""
        user.lastName = userResponse["last_name"].string ?? ""
        user.gender = userResponse["gender"].string ?? ""
        user.dateOfBirth = userResponse["dateofbirth"].string ?? ""
        user.birthPlace = userResponse["birthplace"].string ?? ""
        user.nationality = userResponse["nationality"].string ?? ""
        user.registeredLocation = userResponse["reg_location"].string ?? ""
        user.lastTransactionDate = userResponse["last_transaction_date"].string ?? ""
        user.phoneVerified = userResponse["phone_verified"].int == 1 ? true : false
        user.emailVerified = userResponse["email_verified"].int == 1 ? true : false
        user.addressVerified = userResponse["address_verified"].int == 1 ? true : false
        user.selfieVerified = userResponse["selfie_verified"].int == 1 ? true : false
        user.indentityVerified = userResponse["indentity_verified"].int == 1 ? true : false
        
        RealmManager.save(object: user)
        
    }
    
    class func clearUserSession() {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
        UserSession.sharedInstance.userTransactionHistory.removeAll()
    }
    
    class func showMainDashBoard() {
        let vc = R.Storyboards.main.instantiateViewController(withIdentifier: "SWRevealView")
        Utils.setRootViewController(vc)
    }
    
    class func showPinVerification(viewController: UIViewController) {
        let vc = R.Storyboards.register.instantiateViewController(withIdentifier: "VerificationPinViewController")
        viewController.navigationController?.pushViewController(vc, animated: true)
    }
    
    class func createAccount(viewController: UIViewController) {
        
        RegisterSession.sharedInstance.isNewUser = true
        SwiftSpinner.show("")
        
        if let currentUser = RegisterSession.sharedInstance.currentUser {
            print(currentUser)
            
            RegisterService.register(mobileNumber: currentUser.mobileNumber!,
                                     password: currentUser.password!,
                                     type: currentUser.type!,
                                     first_name: currentUser.firstName!,
                                     lastname: currentUser.lastname!,
                                     socialId: currentUser.socialId!,
                                     dob: currentUser.dateOfBirth!,
                                     gender: currentUser.gender!,
                                     imageURL: currentUser.imageURL!,
                                     email: currentUser.email!) { (response, error) in
                                        
                                        print(response as Any)
                                        SwiftSpinner.hide()
                                        if error != nil {
                                            if let topController = Utils.topViewController() {
                                                //AlertviewManager.showAlertInfo(message: (error?.description)!, view: topController)
                                            }
                                        } else {
                                            if response!["code"].int == 200 {
                                                UserDefaults.standard.set("\(response!["token"])", forKey: R.CacheKeys.userToken)
                                                UserDefaults.standard.synchronize()
                                                viewController.performSegue(withIdentifier: "verificationPinSegue", sender: viewController)
                                            } else {
                                                if let topController = Utils.topViewController() {
                                                    AlertviewManager.showAlertInfo(message: response!["message"].string!, view: topController)
                                                }
                                            }
                                        }
                                        
            }
        } else {
            print("No user")
        }
    }
    
    class func updateUserInfo(viewController: UIViewController , completion: @escaping CompletionBlock) {
        SwiftSpinner.show("")
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            UserService.getUserProfile(customerId: "\(currentUser.id)", completion: { (response, error) in
                print(response as Any)
                SwiftSpinner.hide()
                if error != nil {
                    //AlertviewManager.showAlertInfo(message: (error?.description)!, view: viewController)
                    completion(nil, error)
                } else {
                    
                    let userResponse = response!["customer"]
                    
                    try! RealmManager.realm?.write {
                        currentUser.mobileNo = userResponse["mobile_no"].string ?? ""
                        currentUser.email = userResponse["email"].string ?? ""
                        currentUser.profileImage = userResponse["profile_image"].string ?? ""
                        currentUser.activationCode = userResponse["activation_code"].string ?? ""
                        currentUser.pinCode = userResponse["pin_code"].string ?? ""
                        currentUser.lylAddress = userResponse["lyl_address"].string ?? ""
                        currentUser.lylBalance = userResponse["lyl_balance"].double ?? 0.0
                        currentUser.firstName = userResponse["first_name"].string ?? ""
                        currentUser.middleName = userResponse["middle_name"].string ?? ""
                        currentUser.lastName = userResponse["last_name"].string ?? ""
                        currentUser.gender = userResponse["gender"].string ?? ""
                        currentUser.dateOfBirth = userResponse["dateofbirth"].string ?? ""
                        currentUser.birthPlace = userResponse["birthplace"].string ?? ""
                        currentUser.nationality = userResponse["nationality"].string ?? ""
                        currentUser.registeredLocation = userResponse["reg_location"].string ?? ""
                        currentUser.lastTransactionDate = userResponse["last_transaction_date"].string ?? ""
                        currentUser.phoneVerified = userResponse["phone_verified"].int == 1 ? true : false
                        currentUser.emailVerified = userResponse["email_verified"].int == 1 ? true : false
                        currentUser.addressVerified = userResponse["address_verified"].int == 1 ? true : false
                        currentUser.selfieVerified = userResponse["selfie_verified"].int == 1 ? true : false
                        currentUser.indentityVerified = userResponse["indentity_verified"].int == 1 ? true : false
                    }
                    
                    completion(response, nil)
                    
                }
            })
        }
    }
    
    class func verifyMobile(mobileNumber: String, viewController: UIViewController) {
        RegisterService.mobileSendCode(mobileNumber: mobileNumber, completion: { (response, error) in
            SwiftSpinner.hide()
            if error == nil {
                showPinVerification(viewController: viewController)
            }
        })
    }
    
    class func getUserProfile() {
        UserSession.sharedInstance.userShouldRefreshBalance = false 
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            SwiftSpinner.show("")
            UserService.getUserProfile(customerId: "\(currentUser.id)", completion: { (response, error) in
                print(response as Any)
                SwiftSpinner.hide()
                if error != nil {
                    //AlertviewManager.showAlertInfo(message: (error?.description)!, view: self)
                } else {
                    
                    let userResponse = response!["customer"]
                    
                    try! RealmManager.realm?.write {
                        currentUser.mobileNo = userResponse["mobile_no"].string ?? ""
                        currentUser.email = userResponse["email"].string ?? ""
                        currentUser.profileImage = userResponse["profile_image"].string ?? ""
                        currentUser.activationCode = userResponse["activation_code"].string ?? ""
                        currentUser.pinCode = userResponse["pin_code"].string ?? ""
                        currentUser.lylAddress = userResponse["lyl_address"].string ?? ""
                        currentUser.lylBalance = userResponse["lyl_balance"].double ?? 0.0
                        currentUser.firstName = userResponse["first_name"].string ?? ""
                        currentUser.middleName = userResponse["middle_name"].string ?? ""
                        currentUser.lastName = userResponse["last_name"].string ?? ""
                        currentUser.gender = userResponse["gender"].string ?? ""
                        currentUser.dateOfBirth = userResponse["dateofbirth"].string ?? ""
                        currentUser.birthPlace = userResponse["birthplace"].string ?? ""
                        currentUser.nationality = userResponse["nationality"].string ?? ""
                        currentUser.registeredLocation = userResponse["reg_location"].string ?? ""
                        currentUser.lastTransactionDate = userResponse["last_transaction_date"].string ?? ""
                        currentUser.phoneVerified = userResponse["phone_verified"].int == 1 ? true : false
                        currentUser.emailVerified = userResponse["email_verified"].int == 1 ? true : false
                        currentUser.addressVerified = userResponse["address_verified"].int == 1 ? true : false
                        currentUser.selfieVerified = userResponse["selfie_verified"].int == 1 ? true : false
                        currentUser.indentityVerified = userResponse["indentity_verified"].int == 1 ? true : false
                    }
                    
                    PushForegroundHelper.updateBalance()
                    PushRewardHelper.updateBalance()
                    
                }
            })
        }
    }
}
