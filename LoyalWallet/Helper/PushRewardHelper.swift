//
//  PushRewardHelper.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 27/05/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import SwiftSpinner

protocol PushRewardHelperDelegate {
    func pushRewardHelper(shouldRefreshWallet:Bool)
    func pushRewardHelper(didUpdateBalance:Bool)
}

class PushRewardHelper: NSObject {
    // MARK: - Shared Instance
    static let sharedInstance: PushRewardHelper = {
        let instance = PushRewardHelper()
        // setup code
        return instance
    }()
    
    // MARK: - Initialization Method
    override init() {
        super.init()
    }
    
    var delegate : PushRewardHelperDelegate?
    
    class func shouldPush() {
        self.sharedInstance.delegate?.pushRewardHelper(shouldRefreshWallet: true)
    }
    
    class func updateBalance() {
        self.sharedInstance.delegate?.pushRewardHelper(didUpdateBalance: true)
    }
    
}
