//
//  RegisterSession.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 19/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit

class RegisterSession: NSObject {
    // MARK: - Shared Instance
    static let sharedInstance: RegisterSession = {
        let instance = RegisterSession()
        // setup code
        return instance
    }()
    
    // MARK: - Initialization Method
    override init() {
        super.init()
    }
    
    var loginType = LoginType.phone
    var existingUserPhone = ""
    var currentUser: RegisterUser?
    var isNewUser = false
    var isHomeVerification = false
    
}
