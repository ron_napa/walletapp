//
//  UIKitExtensions.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 04/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    /**
     Helper to round corners of a UIView.
     
     - Parameter corners: The target corner(s) to be rounded.
     - Parameter cornerRadii: The corner radius of each corner set to be rounded.
     */
    func round(corners: UIRectCorner, cornerRadii: CGSize = CGSize(width: 5, height:  5) ) {
        let path = UIBezierPath(roundedRect:self.bounds,
                                byRoundingCorners: corners,
                                cornerRadii: cornerRadii)
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
    
    func addGradient(colors: [UIColor]) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = colors
        //    gradientLayer.locations = [0.7, 1.0]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        self.layer.addSublayer(gradientLayer)
    }
    
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.4
        animation.values = [-15, 15, -15, 15, -10, 10, -5, 5, 0]
        layer.add(animation, forKey: "shake")
    }
    
    func spinClockWise() {
        spin(clockwise: false)
    }
    
    func spinCounterClockWise() {
        spin(clockwise: true)
    }
    
    func stopSpin() {
        layer.removeAnimation(forKey: "rotate")
    }
    
    //MARK: - Private
    private func spin(clockwise: Bool) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        animation.fromValue = 0
        
        let value = 2 * Double.pi
        
        animation.toValue = clockwise ? value * -1  : value
        animation.duration = 1.0
        animation.repeatCount = MAXFLOAT
        layer.add(animation, forKey: "rotate")
    }
}

enum LayoutAttribute {
    case leading
    case trailing
    case top
    case bottom
}

extension UINavigationBar {
    func setTheme(color: UIColor) {
        let statusBarView = viewWithTag(100)
        
        if let statusBarView = statusBarView {
            statusBarView.backgroundColor = color
        } else {
            let statusBarView = UIView(frame: CGRect(x: 0, y: -20, width: UIScreen.main.bounds.width, height: 20))
            statusBarView.backgroundColor = color
            statusBarView.tag = 100
            addSubview(statusBarView)
        }
        
        set(color: color)
    }
    
    func set(color: UIColor) {
        isTranslucent     = true
        tintColor       = UIColor.white
        backgroundColor = color
        barTintColor    = color
        
        let image = UIImage()
        setBackgroundImage(image, for: .default)
        setBackgroundImage(image, for: .any, barMetrics: .default)
        shadowImage = image
    }
    
}

extension UIButton {
    
    func setEnabled(_ enabled: Bool, color: UIColor? = nil) {
        
        if let color = color {
            backgroundColor = color
        } else {
            backgroundColor = enabled ? UIColor(hex: 0x77B5FF) : UIColor(hex: 0x949494)
        }
        
        isEnabled = enabled
    }
    
}

@IBDesignable extension UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}

extension UITableView {
    /*
    func adjustOffset(extra: CGFloat = 0) {
        if Device.isiPhone5OrLess {
            self.contentOffset = CGPoint(x: self.contentOffset.x, y: 30.0 + extra)
        } else if Device.isiPhone6 {
            self.contentOffset = CGPoint(x: self.contentOffset.x, y: 22.0 + extra)
        }
    }
    
    func adjustVerifyOffset(extra: CGFloat = 0) {
        if Device.isiPhone5OrLess{
            self.contentOffset = CGPoint(x: self.contentOffset.x, y: 45.0 + extra)
        } else {
            self.contentOffset = CGPoint(x: self.contentOffset.x, y: 22.0 + extra)
        }
    }
    */
}

extension NSAttributedString {
    func stringWithString(stringToReplace:String, replacedWithString newStringPart:String) -> NSMutableAttributedString
    {
        let mutableAttributedString = mutableCopy() as! NSMutableAttributedString
        let mutableString = mutableAttributedString.mutableString
        
        while mutableString.contains(stringToReplace) {
            let rangeOfStringToBeReplaced = mutableString.range(of: stringToReplace)
            mutableAttributedString.replaceCharacters(in: rangeOfStringToBeReplaced, with: newStringPart)
        }
        return mutableAttributedString
    }
}

extension UIViewController {
    var className: String {
        return NSStringFromClass(type(of: self))
    }
}

extension UIScrollView {
    func scrollToBottom() {
        let bottomOffset = CGPoint(x: 0, y: contentSize.height - bounds.size.height + contentInset.bottom)
        setContentOffset(bottomOffset, animated: true)
    }
    
    func scrollToTop() {
        let bottomOffset = CGPoint(x: 0, y: 0)
        setContentOffset(bottomOffset, animated: true)
    }
}

extension UITapGestureRecognizer {
    
    func didTapAttributedTextInLabel(label: UILabel, inRange range: NSRange) -> Bool {
        //        let textStorage = NSTextStorage(attributedString: label.attributedText!)
        //
        //        let layoutManager = NSLayoutManager()
        //        textStorage.addLayoutManager(layoutManager)
        //
        //        let textContainer = NSTextContainer(size: label.frame.size)
        //
        //        textContainer.lineFragmentPadding = 0
        //
        //        var glyphRange = NSMakeRange(0, 1)
        //
        //        layoutManager.characterRange(forGlyphRange: targetRange, actualGlyphRange: &glyphRange)
        //
        //        let glyphRect = layoutManager.boundingRect(forGlyphRange: glyphRange, in: textContainer)
        //
        //        let touchPoint = self.location(ofTouch: 0, in: label)
        //
        //        return glyphRect.contains(touchPoint)
        
        guard let attributedText = label.attributedText else {
            return false
        }
        
        // create instances of NSLayoutManager, NSTextContainer and NSTextStorage
        let layoutManager = NSLayoutManager()
        let textContainer = NSTextContainer(size: .zero)
        let textStorage = NSTextStorage(attributedString: attributedText)
        
        layoutManager.addTextContainer(textContainer)
        textStorage.addLayoutManager(layoutManager)
        
        // configure textContainer for the label
        let labelSize = label.bounds.size
        
        textContainer.lineFragmentPadding = 0.0
        textContainer.lineBreakMode = label.lineBreakMode
        textContainer.maximumNumberOfLines = label.numberOfLines
        textContainer.size = labelSize
        
        // find the tapped character location and compare it to the specified range
        let labelTouchLocation = location(in: label)
        let textBoundingBox = layoutManager.usedRect(for: textContainer)
        let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x, y: (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y)
        let textContainerTouchLocation = CGPoint(x: labelTouchLocation.x - textContainerOffset.x, y: labelTouchLocation.y - textContainerOffset.y)
        let indexOfCharacter = layoutManager.characterIndex(for: textContainerTouchLocation, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
        
        return NSLocationInRange(indexOfCharacter, range)
    }
}

extension UILabel {
    func setAttributedText(primaryString: String, textColor: UIColor, font: UIFont, secondaryString: String, secondaryTextColor: UIColor, secondaryFont: UIFont) {
        
        let completeString = "\(primaryString) \(secondaryString)"
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center
        
        let completeAttributedString = NSMutableAttributedString(
            string: completeString, attributes: [
                .font: font,
                .foregroundColor: textColor,
                .paragraphStyle: paragraphStyle
            ]
        )
        
        let secondStringAttribute: [NSAttributedStringKey: Any] = [
            .font: secondaryFont,
            .foregroundColor: secondaryTextColor,
            .paragraphStyle: paragraphStyle
        ]
        
        let range = (completeString as NSString).range(of: secondaryString)
        
        completeAttributedString.addAttributes(secondStringAttribute, range: range)
        
        self.attributedText = completeAttributedString
    }
}
