//
//  NumberExtension.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 07/05/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Foundation

extension Double {
    
    var abbreviated: String {
        let abbrev = "KMBTPE"
        return abbrev.enumerated().reversed().reduce(nil as String?) { accum, tuple in
            let factor = Double(self) / pow(10, Double(tuple.0 + 1) * 3)
            let format = (factor.truncatingRemainder(dividingBy: 1)  == 0 ? "%.0f%@" : "%.1f%@")
            return accum ?? (factor > 1 ? String(format: format, factor, String(tuple.1)) : nil)
            } ?? String(self)
    }
    
    func format(f: String) -> String{
        return NSString(format: "%\(f)f" as NSString, self) as String
    }
    
    mutating func roundTo(f: String){
        self = NSString(format: "%\(f)f" as NSString, self).doubleValue
    }
    
    func noDecimal() -> String{
        return NSString(format: "%g" as NSString, self) as String
    }
    
    func roundToInt() -> Int{
        return Int(Darwin.nearbyint(self))
    }
    
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1.0) == 0 ? String(format: "%.0f", self) : String(self)
    }
    
}

extension Int {
    
    func formatUsingAbbrevation () -> String {
        let numFormatter = NumberFormatter()
        
        typealias Abbrevation = (threshold:Double, divisor:Double, suffix:String)
        let abbreviations:[Abbrevation] = [(0, 1, ""),
                                           (1000.0, 1000.0, "K"),
                                           (100_000.0, 1_000_000.0, "M"),
                                           (100_000_000.0, 1_000_000_000.0, "B")]
        // you can add more !
        
        let startValue = Double (abs(self))
        let abbreviation:Abbrevation = {
            var prevAbbreviation = abbreviations[0]
            for tmpAbbreviation in abbreviations {
                if (startValue < tmpAbbreviation.threshold) {
                    break
                }
                prevAbbreviation = tmpAbbreviation
            }
            return prevAbbreviation
        } ()
        
        let value = Double(self) / abbreviation.divisor
        
        numFormatter.positiveSuffix = abbreviation.suffix
        numFormatter.negativeSuffix = abbreviation.suffix
        numFormatter.allowsFloats = true
        numFormatter.minimumIntegerDigits = 1
        numFormatter.minimumFractionDigits = 0
        numFormatter.maximumFractionDigits = 1

        return numFormatter.string(from: NSNumber (value:value))!
    }
    
}
