//
//  UIColorExtensions.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 04/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    static let successColor = UIColor(hex: 0x669966)
    static let defaultYellow = UIColor(hex: 0xF7C900)
    static let linkOutButton = UIColor(hex: 0x4FACDD)
    
    static let gradientVioletColor = UIColor(hex: 0xC86DD7)
    
    static let textColor = UIColor.white
    static let lightBlue = UIColor(hex: 0x2DB9FF)
    
    static let blueBackground = UIColor(hex: 0x275CBB)
    static let errorTextColor = UIColor(hex: 0x8B9C90)
    static let redErrorColor = UIColor(hex: 0xFF324B)
    
    static let blueTextColor =  UIColor(hex: 0x275CBB)
    static let alertTextColor = UIColor(hex: 0x4a4a4a)
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(hex: Int, alpha: Float = 1.0) {
        self.init(
            red: (hex >> 16) & 0xFF,
            green: (hex >> 8) & 0xFF,
            blue: hex & 0xFF)
    }
}
