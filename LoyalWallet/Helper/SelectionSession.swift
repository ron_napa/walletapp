//
//  SelectionSession.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 04/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Foundation
import MapKit

class SelectionSession: NSObject {
    // MARK: - Shared Instance
    static let sharedInstance: SelectionSession = {
        let instance = SelectionSession()
        // setup code
        return instance
    }()
    
    // MARK: - Initialization Method
    override init() {
        super.init()
    }
    
    var selectedMerchant : Merchant?
    var merchantCode : Int = 1
    var currentLocation: CLLocation?
    
}
