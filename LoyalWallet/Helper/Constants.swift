//
//  Constants.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 03/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Foundation
import UIKit

enum BuildEnvironment {
    case dev, test, prod
}

enum Environment: String {
    case dev = "development"
    case prod = "production"
    case local = "local"
    case devSecurity = "devSecurity"
}

let BUILD_ENV = BuildEnvironment.test
let API_ENV = Environment.devSecurity

let BASE_URL_DEV = "https://apidev.loyalcoin.io/"
let BASE_URL_QA = "https://apiqa.loyalcoin.io/"
let BASE_URL_PROD = "https://nodeapi.loyalcoin.io/"

let BASE_URL = BASE_URL_PROD

typealias Coordinate = (latitude: Double, longitude: Double)

struct R {
    struct strings {
        //status
    }
    
    struct Storyboards {
        static let accountVerification = UIStoryboard(name: "AccountVerification", bundle: Bundle.main)
        static let main = UIStoryboard(name: "Main", bundle: Bundle.main)
        static let register = UIStoryboard(name: "Register", bundle: Bundle.main)
    }
    
    struct CacheKeys {
        //Keys
        static let userToken = "user_token"
        static let deviceToken = "device_token"
        static let fcmToken = "fcm_token"
        static let backgroundTimestamp = "background_timestamp"
        static let exhaustCount = "exhaust_count_"
        
        static let shouldUpdate = "received_push"
    }
    
    struct RegexPattern {
        //Regex Pattern
        static let patternMobileNumber = "^(\\+639)\\d{9}$"
        static let patternName = "^\\p{L}+(['-]\\p{L}+)*\\.?(\\s\\p{L}+(['-]\\p{L}+)*\\.?)*$"
        static let patternPasswordWithSpecial = "^(?=.*\\d)(?=.*[a-z])(?=.*[a-zA-Z]).{8,}$"
        static let patternPasswordAlpha = "^(?=.*[a-zA-Z]).{8,}$"
        static let patternPassword = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$"
        static let patternEmail2 = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}"
        static let patternEmail = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" + "\\@" + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" + "(" + "\\." + "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" + ")+"
        static let patternEmail3 = "^([A-Z|a-z|0-9](\\.|_){0,1})+[A-Z|a-z|0-9]\\@([A-Z|a-z|0-9])+((\\.){0,1}[A-Z|a-z|0-9]){2}\\.[a-z]{2,3}$"
        static let patternDouble = "(?:\\.|(\\\")|[^\"\"\n])*"
    }
    
    struct NotificationKeys {
        static let didReceiveRemoteNotification = NSNotification.Name(rawValue: "application.didreceiveremotenotification")
        static let didReceiveDismissErrorAlert = NSNotification.Name(rawValue: "application.diddismisserroralert")
    }
    
    struct TimerDefault {
        
    }
    
    struct Url {
        //linkOut
        static let myAccount = "https://accounts.globe.com.ph/login"
    }
    
    struct ErrorCount {
        //error limit
        static let exhaustLimit = 5
    }
    
    static let timeFormat = "h:mm a"
    static let defaultDateFormat = "MMM. d, yyyy, h:mm a"
    static let trackerDateFormat = "MMMM d, yyyy (EEEE)"
    
}

enum AuthorizationStatus {
    case authorized, notDetermined, denied, restricted
}

enum DashboardCardStatus {
    case normal, refreshing, refreshFailed
}

// MARK: - Verification Types
enum VerificationType: String {
    case phone = "Phone Verification"
    case email = "Email Verification"
    case indentity = "Indentity Verification"
    case selfie = "Selfie Verification"
    case address = "Address Verification"
}

/// Used to identify the necessary views/ functions for the reused registration module.
enum RegistrationType: String {
    case forgotPassword = "forgot_password"
    case firstAccount = "first_account"
    case addAccount = "add_account"
}

struct SecurityUpgradeStatus {
    /// Show security upgrade alert
    static var introduction = 0
    
    /// security upgrade alert was shown atleast once, this means app should always redirect to choose access
    static var notStarted = 1
    
    /// in progress if user has successfully verified an account
    static var inProgress = 2
    
    /// completed if 'Do this later' is selected from 'Account Verified' alert
    static var completed = 3
}

let kEmailMaxLength = 256

let lockOutDuration = 30
