//
//  SocialLoginHelper.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 19/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Foundation
import GoogleSignIn
import SWRevealViewController
import SwiftSpinner
import FBSDKLoginKit

enum LoginType: String {
    case fb = "fb"
    case google = "google"
    case phone = "phone"
}

@objc protocol SocialLoginHelperDelegate {
    @objc optional func socialLoginHelper(_ socialLoginHelper: SocialLoginHelper, didVerifyOTP: Bool)
    func socialLoginHelper(_ helper: SocialLoginHelper)
    func socialLoginHelper(_ helper: SocialLoginHelper, socialIdExist: Bool)
}

class SocialLoginHelper: NSObject, GIDSignInDelegate, GIDSignInUIDelegate {
    
    private var vc:UIViewController? = nil
    private var forReg:Bool = true
    private var fbID : String?
    var delegate: SocialLoginHelperDelegate?
    
    init(_ viewController: UIViewController) {
        super.init()
        vc = viewController
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
    }
    
    // MARK: - GOOGLE
    func loginWithGoogle(){
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().clientID = "774095261078-uq1su31guoqp1d9t9ipl9eb35b6cnpss.apps.googleusercontent.com"
        GIDSignIn.sharedInstance().signIn()
    }
    
    // MARK:  GIDSignInDelegate methods
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if user != nil{
            
            print(user)
            
            let newRegister =  RegisterUser.init("",
                                                 "",
                                                 LoginType.google.rawValue,
                                                 user.profile.givenName,
                                                 user.profile.familyName,
                                                 user.userID,
                                                 "",
                                                 "",
                                                 user.profile.imageURL(withDimension: 120).absoluteString,
                                                 user.profile.email)
            
            RegisterSession.sharedInstance.currentUser = newRegister
            RegisterSession.sharedInstance.loginType = LoginType.google
            
            SwiftSpinner.show("")
            RegisterService.getMobile(loginType: LoginType.google.rawValue, socialId: newRegister.socialId!, completion: { (response, error) in
                if error == nil {
                    SwiftSpinner.hide()
                    if response!["code"].int == 200 {
                        RegisterSession.sharedInstance.existingUserPhone = response!["mobile_no"].string!.replacingOccurrences(of: "+63", with: "")
                        self.delegate?.socialLoginHelper(self, socialIdExist: true)
                    } else {
                        self.delegate?.socialLoginHelper(self, socialIdExist: false)
                    }
                }
            })
        }
    }
    
    func sign(inWillDispatch signIn: GIDSignIn!, error: Error!) {
        print("Google Sign In Error-----\n ")
        print("Error: \(error)\n\n")
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("\n\n GoogleSign In Disconnect: Did Disconnect With Google Sign In")
    }
    
    // MARK:  GIDSignINUIDelegate methods
    
    func sign(_ signIn: GIDSignIn!, present viewController: UIViewController!) {
        vc?.present(viewController, animated: true, completion: nil)
    }
    
    func sign(_ signIn: GIDSignIn!, dismiss viewController: UIViewController!) {
        vc?.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - FB
    func loginWithFacebook(){
        
        var _fbLoginManager: FBSDKLoginManager?
        
        var login: FBSDKLoginManager {
            get {
                if _fbLoginManager == nil {
                    _fbLoginManager = FBSDKLoginManager()
                }
                return _fbLoginManager!
            }
        }
        
        login.logIn(withReadPermissions: ["public_profile", "email"], from: vc, handler: {(result, error) -> Void in
            if error != nil {
                print(error!)
                if let fbAccessToken = FBSDKAccessToken.current() {
                    print("error:")
                    print(fbAccessToken)
                }
                print(error!.localizedDescription)
                if let resultVar = result {
                    print("result:")
                    print(resultVar)
                }
            } else if (result?.isCancelled)! {
                print("FB Login is Cancelled")
            } else {
                print("FB Login Succeeded")
                
                SwiftSpinner.show("")
                if((FBSDKAccessToken.current()) != nil){
                    FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name, picture.type(large), email, gender"]).start(completionHandler: { (connection, result, error) -> Void in

                        print(result!)
                        
                        let res = result as! Dictionary<String, Any>
                        
                        let newRegister = RegisterUser.init(info: res)
                        RegisterSession.sharedInstance.currentUser = newRegister
                        RegisterSession.sharedInstance.loginType = LoginType.fb
                        
                        RegisterService.getMobile(loginType: LoginType.fb.rawValue, socialId: newRegister.socialId!, completion: { (response, error) in
                            if error == nil {
                                print(response as Any)
                                SwiftSpinner.hide()
                                if response!["code"].int == 200 {
                                    RegisterSession.sharedInstance.existingUserPhone = response!["mobile_no"].string!.replacingOccurrences(of: "+63", with: "")
                                    self.delegate?.socialLoginHelper(self, socialIdExist: true)
                                } else {
                                    self.delegate?.socialLoginHelper(self, socialIdExist: false)
                                }
                            }
                        })
                    })
                    
                }
            }
        })
    }
}
