//
//  AlertviewManager.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 10/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit

class AlertviewManager: NSObject {
    // MARK: - Shared Instance
    static let sharedInstance: AlertviewManager = {
        let instance = AlertviewManager()
        // setup code
        return instance
    }()
    
    // MARK: - Initialization Method
    override init() {
        super.init()
    }
    
    var alert = UIAlertController()
    
    class func showAlertInfo(_ appName:String = "LoyalWallet", message:String,view:UIViewController) {
        
//        var viewWillShow = view
//        if viewWillShow == nil {
//            if let topController = Utils.topViewController() {
//                viewWillShow = topController
//            }
//        }
        
        let alert = UIAlertController(title: appName, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        view.present(alert, animated: true, completion: nil)
        UserSession.sharedInstance.allAlertController.append(alert)
    }
    
}
