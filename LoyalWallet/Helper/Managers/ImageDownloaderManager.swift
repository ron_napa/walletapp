//
//  ImageDownloaderManager.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 05/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class ImageDownloaderManager: NSObject {
    
    // MARK: - Shared Instance
    static let sharedInstance: ImageDownloaderManager = {
        let instance = ImageDownloaderManager()
        // setup code
        return instance
    }()
    
    // MARK: - Initialization Method
    override init() {
        super.init()
    }
    
    var imageCache = AutoPurgingImageCache()
    
//
//    let imageCache = AutoPurgingImageCache()
//    let avatarImage = UIImage(data: data)!
//
//    // Add
//    imageCache.add(avatarImage, withIdentifier: "avatar")
//
//    // Fetch
//    let cachedAvatar = imageCache.image(withIdentifier: "avatar")
//
//    // Remove
//    imageCache.removeImage(withIdentifier: "avatar")
//

    class func downloadImage(urlString:String, completion: @escaping ImageCompletionBlock) {

        let sliceArray = urlString.components(separatedBy: "?")
        
        if sliceArray.count > 0 {
            
            let imageName = sliceArray[0].components(separatedBy: "/")
            let imageNameString : String = imageName[imageName.count-1]
            
            if let cachedImage = self.sharedInstance.imageCache.image(withIdentifier: imageNameString) {
                completion(cachedImage)
            } else {
                Alamofire.request(urlString).responseImage { response in
                    if let image = response.result.value {
                        self.sharedInstance.imageCache.add(image, withIdentifier: imageNameString)
                        completion(image)
                    }
                }
            }
        } else {
            //completion(nil)
        }
    }
    
    class func downloadImageNonCache(urlString:String, completion: @escaping ImageCompletionBlock) {
        var downloadImage : UIImage? = nil
        Alamofire.request(urlString).responseImage { response in
            if let image = response.result.value {
                downloadImage = image
                completion(downloadImage!)
            }
        }
    }
    
    
}
