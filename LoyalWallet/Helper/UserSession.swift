//
//  UserSession.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 25/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import SwiftSpinner

class UserSession: NSObject {
    // MARK: - Shared Instance
    static let sharedInstance: UserSession = {
        let instance = UserSession()
        // setup code
        return instance
    }()
    
    // MARK: - Initialization Method
    override init() {
        super.init()
    }
    
    var userTransactionHistory : [UserTransaction] = []
    var userTransactionCount = 0
    var userTransactionPage = 0
    var userShouldRefreshTrans = false
    var userShouldRefreshBalance = false
    let userTransactionPerPage = 5
    let userTransactionCellHeight = 70
    var selectedTransaction : UserTransaction?
    
    var isMobileVerified = true
    var willShowLogin = false
    var willShowCreate = false
    
    var allAlertController : [UIAlertController] = []
}
