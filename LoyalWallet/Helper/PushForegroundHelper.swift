//
//  PushForegroundHelper.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 26/05/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Foundation
import Realm
import RealmSwift
import SwiftSpinner

protocol PushForegroundHelperDelegate {
    func pushForegroundHelper(shouldRefreshWallet:Bool)
    func pushForegroundHelper(didUpdateBalance:Bool)
}

class PushForegroundHelper: NSObject {
    // MARK: - Shared Instance
    static let sharedInstance: PushForegroundHelper = {
        let instance = PushForegroundHelper()
        // setup code
        return instance
    }()
    
    // MARK: - Initialization Method
    override init() {
        super.init()
    }
    
    var delegate : PushForegroundHelperDelegate?
    
    class func shouldPush() {
        self.sharedInstance.delegate?.pushForegroundHelper(shouldRefreshWallet: true)
    }
    
    class func updateBalance() {
        self.sharedInstance.delegate?.pushForegroundHelper(didUpdateBalance: true)
    }
    
}
