//
//  ERManager.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 19/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import RealmSwift
import EasyRealm

struct ERManager {
    
    //TODO: Versioning /Migration handling
    
    /*
     var users:Array<User>
     
     do {
     users = try User.er.all().toArray()
     } catch _ {
     users = []
     }
     
     do {
     let actionSave = try report.er.saved(update: true)
     } catch _ {}
     */
    
    static func save(object: Object) {
        do {
            _ = try object.er.saved(update: true)
        } catch _ {
            print("Unexpected error occured")
        }
    }
    
    static func save(objects: [Object]) {
        guard objects.count > 0 else {
            return
        }
        
        do {
            for object in objects{
                _ = try object.er.saved(update: true)
            }
        } catch _ {
            print("Unexpected error occured")
        }
        
    }
    //
    static func delete(object: Object) {
        do {
            try object.er.delete()
        } catch {
            print("Unexpected error occured")
        }
    }
    
    static func delete(objects: [Object]) {
        do {
            for object in objects{
                try object.er.delete()
            }
        } catch {
            print("Unexpected error occured")
        }
    }
    
    static func write(object:Object, operation: (() -> Void)?) {
        do {
            try object.er.edit { _ in
                operation?()
            }
        } catch {
            print("Unexpected error occured")
        }
    }
    
    static func realmConfig(){
        let config = Realm.Configuration(
            schemaVersion: 2,
            
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 2) {
                    //
                }
        })
        
        Realm.Configuration.defaultConfiguration = config
        let _ = try! Realm()
    }
    
}
