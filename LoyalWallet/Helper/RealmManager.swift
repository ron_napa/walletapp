//
//  RealmManager.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 03/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift


class RealmManager: NSObject {
    
    static var realm: Realm? {
        do {
            return try Realm(configuration: Realm.Configuration.defaultConfiguration)
        } catch let error {
            print("Error: \(error.localizedDescription)")
            return nil
        }
    }
    
    static func save(object: Object) {
        do {
            try realm?.write {
                realm?.add(object, update: true)
            }
        } catch let error {
            print("Error performing write operation: \(error.localizedDescription)")
        }
    }
    
    
    static func save(objects: [Object]) {
        guard objects.count > 0 else {
            return
        }
        
        do {
            try realm?.write {
                
                realm?.add(objects, update: true)
            }
        } catch let error {
            print("Error performing write operation: \(error.localizedDescription)")
        }
    }
    
    static func delete(object: Object, completion: ((_ error: Error?) -> Void)? = nil) {
        do {
            try realm?.write {
                realm?.delete(object)
            }
            completion?(nil)
        } catch let error {
            print("Error performing write operation: \(error.localizedDescription)")
            completion?(error)
        }
    }
    
    static func delete(objects: [Object], completion: ((_ error: Error?) -> Void)? = nil) {
        guard objects.count > 0 else {
            return
        }
        
        do {
            try realm?.write {
                realm?.delete(objects)
            }
            completion?(nil)
        } catch let error {
            print("Error performing write operation: \(error.localizedDescription)")
            completion?(error)
        }
    }
    
    static func write(operation: ((_ realm: Realm?) -> Void)?) {
        do {
            try realm?.write {
                operation?(realm)
            }
        } catch let error {
            print("Error performing write operation: \(error.localizedDescription)")
        }
    }
    
}
