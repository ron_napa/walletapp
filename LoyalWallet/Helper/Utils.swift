//
//  Utils.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 19/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Foundation
import CoreImage
import SystemConfiguration
import UIKit
import CoreData
import GoogleSignIn
import UserNotifications
import GoogleMaps
import Firebase
import IQKeyboardManagerSwift
import Crashlytics
import Fabric

class Utils: NSObject {

    class func initialSetup() {
        setupFirebase()
        setupGoogleMap()
        setupGoogleSignIn()
        setupIQKeyboard()
        setupCrashlytics()
    }
    
    class func setupFirebase() {
        if(FirebaseApp.app() == nil){
            FirebaseApp.configure()
        }
        //FirebaseApp.configure()
    }
    
    class func setupGoogleMap() {
        GMSServices.provideAPIKey("AIzaSyD6S9hpQLyim4_TJqBZHb0x5XccRn9KJsI")
    }
    
    class func setupGoogleSignIn() {
        GIDSignIn.sharedInstance().clientID = "820853439346-9abomv5fds9cr4ciu00gk4kb55b1echo.apps.googleusercontent.com"
    }
    
    class func setupIQKeyboard() {
        IQKeyboardManager.sharedManager().enable = true
    }
    
    class func setupCrashlytics() {
        Fabric.with([Crashlytics.self])
    }
    
}

extension Utils {
    static func setRootViewController(_ viewController: UIViewController, completion: (() -> Void)? = nil) {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        
        let snapshot: UIView = (delegate.window?.snapshotView(afterScreenUpdates: true))!
        viewController.view.addSubview(snapshot)
        
        delegate.window?.rootViewController = viewController
        
        UIView.animate(withDuration: 0.5, animations: {() in
            snapshot.layer.opacity = 0;
            snapshot.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.5);
        }, completion: {
            (value: Bool) in
            snapshot.removeFromSuperview();
            completion?()
        });
    }
    
    static func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

