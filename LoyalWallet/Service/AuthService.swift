//
//  AuthService.swift
//  GlobeAtHome
//
//  Created by Mylene Bayan on 14/09/2017.
//  Copyright © 2017 Globe Telecom, Inc. All rights reserved.
//

import UIKit
import SwiftyJSON

class AuthService: Service {
    
    class func login(_ customerId: String, token: String, code: String? = nil, completion: @escaping (_ clientId: String?, _ developerId: String?, _ error: NSError?) -> Void) {
        
        var parameters = ["token": token]
        
        if let code = code, !code.isEmpty {
            parameters["code"] = code
        }
        
        perform(task: .login(parameters), isTokenRequired: false) { (response, error) in
            
            if let error = error {
                completion(nil, nil, error)
            } else {
                if let clientId = response?["client_id"].string, let developerId = response?["developer_id"].string {
                    
                    //KeychainUtil.set(clientId, forKey: "\(R.CacheKeys.clientId)\(customerId)")
                    //KeychainUtil.set(developerId, forKey: "\(R.CacheKeys.developerId)\(customerId)")
                    
                    completion(clientId, developerId, nil)
                } else {
                    completion(nil, nil, BBError.Message.missingCredentials.asError())
                }
            }
            
        }
    }
    
    class func getAccessToken(_ clientId: String, developerId: String, completion: @escaping (_ error: NSError?) -> Void) {
        perform(task: .accessToken(["client_id": clientId, "developer_id": developerId]), isTokenRequired: false) { (response, error) in
            if let error = error {
                completion(error)
            } else {
                if let accessToken = response?["access_token"].string, let accessTokenLifespan = response?["expires_in"].number {
                    
                /*
                let customerId = User.main().id
                    
                    Session.setAccessToken(accessToken, timestamp: Date(), lifespan: accessTokenLifespan, customerId: customerId)
                    Session.removeTemporaryAccessToken(forCustomerId: customerId)
                */
                    completion(nil)
                } else {
                    completion(BBError.Message.missingAccessToken.asError())
                }
            }
        }
    }
}
