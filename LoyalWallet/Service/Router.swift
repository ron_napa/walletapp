//
//  Router.swift
//  GongchaWallet
//
//  Created by Ronald Napa on 16/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Alamofire
import SwiftyJSON

enum Router {
    
    case login(Parameters)
    case logout(Parameters)
    case getAccessToken(Parameters)
    case register(Parameters)
    case getMobile(Parameters)
    case verifyCode(Parameters)
    case resendCode(Parameters)
    case createPin(Parameters)
    case mobileSendCode(Parameters)
    case addDownload(Parameters)
    case resetPassword(Parameters)
    case verifyPasswordCode(Parameters)
    
    case editProfile(Parameters)
    case getUserProfile(Parameters)
    case updateEmail(Parameters)
    case sendFunds(Parameters)
    case customerValidity(Parameters)
    case getUserTransactions(Parameters)
    case clearPin(Parameters)
    
    //Merchant
    case merchantList(Parameters)
    case merchantRewards(Parameters)
    case merchantLocations(Parameters)
    
    //Account Verification
    case sendEmailCode(Parameters)
    case verifyEmailCode(Parameters)
    
    //Announcements
    case getAnnouncements(Parameters)
    
    struct Endpoint {
        
        static let login                = "customer/login"
        static let logout               = "customer/logout"
        static let accessToken          = "customer/accesstoken"
        
        static let register             = "customer/register"
        static let getMobile            = "customer/getMobile"
        static let verifyCode           = "customer/verifyCode"
        static let resendCode           = "customer/resendCode"
        static let createPin            = "customer/createPin"
        static let mobileSendCode       = "customer/sendCode"
        static let addDownload          = "customer/addDownload"
        static let resetPassword        = "customer/resetPassword"
        static let verifyPasswordCode   = "customer/verifyPasswordCode"
        
        static let editProfile          = "customer/editProfile"
        static let getUserProfile       = "customer"
        static let updateEmail          = "customer/updateEmail"
        static let sendFunds            = "customer/send"
        static let customerValidity     = "customer/valid"
        static let getUserTransactions  = "customer/transaction"
        static let clearPin             = "customer/clearPin"
        
        //Merchant
        static let merchantList         = "customer/merchantList"
        static let merchantRewards      = "customer/rewards"
        static let merchantLocations    = "merchant/locations"
        
        //Account Verification
        static let sendEmailCode        = "customer/sendEmailCode"
        static let verifyEmailCode      = "customer/verifyEmailCode"
        
        //Announcements
        static let getAnnouncements     = "customer/announcements"
        
    }
    
    var method: HTTPMethod {
        switch self {
        case .register, .login, .verifyCode, .merchantList, .merchantLocations, .createPin, .mobileSendCode, .updateEmail, .sendFunds, .customerValidity, .sendEmailCode, .verifyEmailCode, .addDownload, .resetPassword, .getMobile, .verifyPasswordCode:
            return .post
        
        case .editProfile:
            return .put
         /*
        case :
            return .delete
            */
        default:
            return .get
        }
    }
}

//MARK: - URLRequestConvertible Protocol Conformance
extension Router: URLRequestConvertible {
    func asURLRequest() throws -> URLRequest {
        
        let result: (path: String, parameters: Parameters?) = {
            switch self {
            case let .login(parameters):
                return (Router.Endpoint.login, parameters)
                
            case let .logout(parameters):
                return (Router.Endpoint.logout, parameters)
                
            case let .getAccessToken(parameters):
                return (Router.Endpoint.accessToken, parameters)
                
            case let .register(parameters):
                return (Router.Endpoint.register, parameters)
                
            case let .getMobile(parameters):
                return (Router.Endpoint.getMobile, parameters)
                
            case let .verifyCode(parameters):
                return (Router.Endpoint.verifyCode, parameters)
                
            case let .resendCode(parameters):
                return (Router.Endpoint.resendCode, parameters)
                
            case let .createPin(parameters):
                return (Router.Endpoint.createPin, parameters)
                
            case let .mobileSendCode(parameters):
                return (Router.Endpoint.mobileSendCode, parameters)
            
            case let .addDownload(parameters):
                return (Router.Endpoint.addDownload, parameters)
               
            case let .verifyPasswordCode(parameters):
                return (Router.Endpoint.verifyPasswordCode, parameters)
                
            case let .resetPassword(parameters):
                return (Router.Endpoint.resetPassword, parameters)
                
            case let .editProfile(parameters):
                return (Router.Endpoint.editProfile, parameters)
                
            case let .getUserProfile(parameters):
                return (Router.Endpoint.getUserProfile, parameters)
             
            case let .getUserTransactions(parameters):
                return (Router.Endpoint.getUserTransactions, parameters)
                
            case let .clearPin(parameters):
                return (Router.Endpoint.clearPin, parameters)
                
            case let .updateEmail(parameters):
                return (Router.Endpoint.updateEmail, parameters)
                
            case let .sendFunds(parameters):
                return (Router.Endpoint.sendFunds, parameters)
                
            case let .customerValidity(parameters):
                return (Router.Endpoint.customerValidity, parameters)
                
            case let .merchantList(parameters):
                return (Router.Endpoint.merchantList, parameters)
                
            case let .merchantRewards(parameters):
                return (Router.Endpoint.merchantRewards, parameters)
                
            case let .merchantLocations(parameters):
                return (Router.Endpoint.merchantLocations, parameters)
                
            case let .sendEmailCode(parameters):
                return (Router.Endpoint.sendEmailCode, parameters)
                
            case let .verifyEmailCode(parameters):
                return (Router.Endpoint.verifyEmailCode, parameters)
                
            case let .getAnnouncements(parameters):
                return (Router.Endpoint.getAnnouncements, parameters)
            }
        }()
        
        //api.loyalcoin.io
        //apidev.loyalcoin.io
        
        let url = BASE_URL + "\(result.path)"
        var urlRequest = URLRequest(url: URL(string: url)!)
        urlRequest.httpMethod = method.rawValue
        
        print(url)
        // set user-agent
        /*
        let modelName = UIDevice.current.modelName
        let systemName = UIDevice.current.systemName
        let systemVersion = UIDevice.current.systemVersion
        
        let dictionary = Bundle.main.infoDictionary
        let appVersion = dictionary!["CFBundleShortVersionString"] as! String
        let appBuild = dictionary!["CFBundleVersion"] as! String
        
        urlRequest.setValue("BBAPP/\(systemName) \(systemVersion)/(\(modelName))/\(appVersion)(\(appBuild))", forHTTPHeaderField: "User-Agent")
        */
        
        //Utils.setupFirebase()
        if method.rawValue == "GET" {
            
            if let userToken = UserDefaults.standard.string(forKey: R.CacheKeys.userToken) {
                //print("JWT \(userToken)")
                urlRequest.setValue("JWT \(userToken)", forHTTPHeaderField: "Authorization")
                print(userToken)
            }
            
            if let parameters = result.parameters {
                var urlWithCode = urlRequest.url?.absoluteString
                urlWithCode = "\(urlWithCode ?? "")/\(parameters["code"]!)"
                urlRequest.url = URL.init(string: urlWithCode!)
                print(urlRequest.url as Any)
            }
            return try URLEncoding.default.encode(urlRequest, with: result.parameters)
        } else {
            urlRequest.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
            
            if let userToken = UserDefaults.standard.string(forKey: R.CacheKeys.userToken) {
                //print("JWT \(userToken)")
                urlRequest.setValue("JWT \(userToken)", forHTTPHeaderField: "Authorization")
            }
            
            if let parameters = result.parameters {
                urlRequest.httpBody = asString(jsonDictionary: parameters).data(using: .utf8, allowLossyConversion: false)
            }
            
            return urlRequest
        }
    }
    
    func asString(jsonDictionary: Parameters) -> String {
        do {
            let data = try JSONSerialization.data(withJSONObject: jsonDictionary, options: .prettyPrinted)
            return String(data: data, encoding: String.Encoding.utf8) ?? ""
        } catch {
            return ""
        }
    }
}
