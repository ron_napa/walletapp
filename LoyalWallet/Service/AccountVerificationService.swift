//
//  AccountVerificationService.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 26/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

class AccountVerificationService: Service {
    
    class func sendEmailCode(email:String, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["email"] = email
        
        perform(task: .sendEmailCode(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response,nil)
            }
        }
    }
    
    class func verifyEmailCode(code:String, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["code"] = code
        
        perform(task: .verifyEmailCode(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response,nil)
            }
        }
    }
    
}
