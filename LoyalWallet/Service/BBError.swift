
//
//  BBError.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 03/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit

struct BBError {
    static let domain = "com.appsolutely.loyaltywallet.error"
    
    struct Code {
        //Generic
        static let unknownError = -7000
        static let emptyResponse = -7001
        static let networkError = -7002
        static let requestTimedOut = -7003
        
        //MARK: - New Error Codes
        static let invalidCode = 405
        static let noUserFound = 406
        static let mismatchedAnswers = 407
        
        //MARK -
        static let invalidToken = 504
        static let pendingUpgradeRequest = 508
        
        //MARK: - New In App Error Codes
        static let unknown = 700
        static let missingDeveloperInfo = 701
        static let missingAccessToken = 702
        static let missingCredentials = 703
    }
    
    struct Message {
        static let unknownError = "Unexpected error occured."
        static let emptyResponse = "Missing content on 'results' object"
        static let requestTimedOut = "The request timed out."
        static let noUserFound = "No user found"
        static let missingQuotaThreshold = "Missing quota or threshold on account's details"
        static let mismatchedAnswers = "Mismatched answers to security questions"
        static let networkError = "The Internet connection appears to be offline."
        
        static let invalidCode = "Invalid Code"
        static let invalidToken = "Invalid login token. Token is expired."
        
        //In App Error
        static let missingDeveloperInfo = "Missing developer info."
        static let missingAccessToken = "Missing access token."
        
        //
        static let missingCredentials = "Missing credentials."
    }
    
}
