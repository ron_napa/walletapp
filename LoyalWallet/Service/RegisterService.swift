//
//  RegisterService.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 19/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire

final class RegisterService: Service {
    
    //MARK: - Security Fix
    class func register(mobileNumber: String, password: String, type: String, first_name: String, lastname: String, socialId: String, dob: String, gender: String, imageURL: String,  email: String, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["social_id"] = socialId
        parameters["mobile_number"] = mobileNumber
        parameters["password"] = password
        parameters["type"] = type
        parameters["first_name"] = first_name
        parameters["last_name"] = lastname
        parameters["gender"] = gender
        parameters["imageURL"] = imageURL
        parameters["email"] = email
        
        //print("register User \(parameters)")
        
        perform(task: .register(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response,nil)
            }
        }
    }
    
    class func getMobile( loginType:String, socialId:String, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["type"] = loginType
        parameters["value"] = socialId
        
        perform(task: .getMobile(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response,nil)
            }
        }
        
    }
    
    class func logout(completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["code"] = ""
        
        perform(task: .logout(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response,nil)
            }
        }
    }
    
    class func login(mobileNumber: String, password: String, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["mobile_number"] = mobileNumber
        parameters["password"] = password
        
        print(parameters)
        
        perform(task: .login(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response,nil)
            }
        }
    }
    
    class func verifyCode(verificationCode: String, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["code"] = verificationCode
        
        perform(task: .verifyCode(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response, nil)
            }
        }
    }
    
    class func verifyPasswordCode(verificationCode: String, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["verification_code"] = verificationCode
        
        perform(task: .verifyPasswordCode(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response, nil)
            }
        }
    }
    
    class func resendCode(completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["code"] = ""
        
        perform(task: .resendCode(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response, nil)
            }
        }
    }
    
    class func createPin(pinCode: String, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["code"] = pinCode
        
        perform(task: .createPin(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response, nil)
            }
        }
    }
    
    class func mobileSendCode(mobileNumber: String, completion: @escaping CompletionBlock) {
        
        //customer/sendCode
        var parameters = Parameters()
        parameters["value"] = mobileNumber
        
        perform(task: .mobileSendCode(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response, nil)
            }
        }
    }
    
    class func addDownload(email: String, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        if email.count != 0 {
            parameters["email"] = email
        }
        
        if let fcmToken = UserDefaults.standard.string(forKey: R.CacheKeys.fcmToken) {
            parameters["fcm_token"] = fcmToken
        }
        
        if let deviceToken = UserDefaults.standard.string(forKey: R.CacheKeys.deviceToken) {
            parameters["device_id"] = deviceToken
        }
        
        parameters["platform"] = "ios"
        
        print(parameters)
        
        perform(task: .addDownload(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response,nil)
            }
        }
    }
    
    class func resetPassword(newPassword: String, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["new_password"] = newPassword
        
        perform(task: .resetPassword(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response, nil)
            }
        }
    }
    
    class func clearPin(completion: @escaping CompletionBlock) {
        
        //customer/sendCode
        var parameters = Parameters()
        parameters["code"] = ""
        
        perform(task: .clearPin(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response, nil)
            }
        }
    }
    
}
