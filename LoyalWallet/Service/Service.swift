//
//  Service.swift
//  GongchaWallet
//
//  Created by Ronald Napa on 16/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Alamofire
import SwiftyJSON

enum Task {
    
    case login(Parameters)
    case logout(Parameters)
    case accessToken(Parameters)
    case register(Parameters)
    case getMobile(Parameters)
    case verifyCode(Parameters)
    case resendCode(Parameters)
    case createPin(Parameters)
    case mobileSendCode(Parameters)
    case addDownload(Parameters)
    case resetPassword(Parameters)
    case verifyPasswordCode(Parameters)
    
    case editProfile(Parameters)
    case getUserProfile(Parameters)
    case updateEmail(Parameters)
    case sendFunds(Parameters)
    case customerValidity(Parameters)
    case getUserTransactions(Parameters)
    case clearPin(Parameters)
    
    //Merchant
    case merchantList(Parameters)
    case merchantRewards(Parameters)
    case merchantLocations(Parameters)
    
    //Account Verification
    case sendEmailCode(Parameters)
    case verifyEmailCode(Parameters)
    
    //Announcements
    case getAnnouncements(Parameters)
    
}

// MARK: - Util
extension Task {
    func isRegister() -> Bool {
        /*
        if case .sendOTP = self {
            return true
        }
        
        if case .securityQuestions = self {
            return true
        }
        */
        return false
    }
}

typealias SuccessBlock = (_ response: Any) -> Void
typealias FailureBlock = (_ error: NSError) -> Void
typealias CompletionBlock = (_ response: JSON?, _ error: NSError?) -> Void
typealias ImageCompletionBlock = (_ response: UIImage) -> Void

class Service {
    static let sessionManager = SessionManager()
    static var isFetchingAccessToken = false
    
    /**
     Main handler of all the requests
     
     - Parameter url: a type conforming to `URLRequestConvertible`.
     - Parameter authenticate: boolean that identifies if the `URLRequest` needs an access token.
     - Parameter completion: a callback that contains and an optional `JSON` or an optional `NSError`, depending if the the requests succeeded or not.
     
     - Returns: an instance of the `URLRequest` created with the supplied parameters.
     */
    @discardableResult
    final class func performRequest(with url: URLRequestConvertible,
                                    authenticate: Bool = true,
                                    completion: @escaping (_ response: JSON?, _ error: NSError?) -> Void) -> URLRequest? {
        /*
        if authenticate {
            if let accessToken = KeychainUtil.value(forKey: "\(R.CacheKeys.accessToken)\(User.main().id)") as? String {
                sessionManager.adapter = AccessTokenAdapter(accessToken: accessToken)
            }
        }
        */
        
        return sessionManager.request(url).responseJSON(completionHandler: { (response) in
            // Do something with the response, example, convert error messages to native Error type.
            
            if let error = response.error {
                completion(nil, error.localizedDescription.asError())
                return
            }
            
            guard let response = response.result.value else {
                completion(nil, BBError.Message.unknownError.asError())
                return
            }
            
            let responseJSON = JSON(response)
            print(responseJSON as Any)
            
            if let error = mapError(from: responseJSON) {
                /*
                if let viewController = Util.topViewController(), error.code == BBError.Code.unknownError  {
                    Util.showError(with: .unknown, from: viewController)
                }
                */
                completion(nil, error)
            } else {
                let results = responseJSON
                if results != JSON.null {
                    completion(results, nil)
                } else {
                    /*
                    if let viewController = Util.topViewController()  {
                        Util.showError(with: .unknown, from: viewController)
                    }
                    */
                    completion(nil, BBError.Message.emptyResponse.asError())
                }
            }
        }).request
    }
    
    //MARK: - Error Mapping
    fileprivate class func mapError(from response: JSON) -> NSError? {
        let errorInfo = response["error"]
        print(errorInfo as Any)
        return errorInfo != JSON.null ? errorInfo.asError() : nil
    }
    
    class func showAlertIfNeeded(forError error: NSError) {
        
        if let viewController = Utils.topViewController() {
            
            if error.code == BBError.Code.networkError ||
                error.code == BBError.Code.requestTimedOut {
                
                AlertviewManager.showAlertInfo(message: "The Internet connection appears to be offline.", view: viewController)
                
            } else if error.code == BBError.Code.unknownError || error.code == BBError.Code.emptyResponse || error.code == 505 {
                
                AlertviewManager.showAlertInfo(message: "Unknown error encountered, please try again later.", view: viewController)
            }
        }
        
    }
}

//MARK: - Access Token Handling
extension Service {
    class func perform(task: Task, isTokenRequired: Bool = true, completion: @escaping CompletionBlock) {
        
        self.invoke(task: task, completion: completion)
        /*
        if isTokenRequired {
            
            let semaphore = DispatchSemaphore(value: 1)
            
            let getAccessToken: ((String, String) -> Void) = { clientId, developerId in
                
                AuthService.getAccessToken(clientId, developerId: developerId, completion: { (error) in
                    
                    isFetchingAccessToken = false
                    
                    if let error = error {
                        
                        completion(nil, error)
                        semaphore.signal()
                        
                    } else {
                        
                        self.invoke(task: task, completion: completion)
                        semaphore.signal()
                        
                    }
                    
                })
            }
            
            isFetchingAccessToken = true
            
            /*
            let clientId = Session.clientIdForMainUser()
            let developerId = Session.developerIdForMainUser()
            
            if clientId.isValid && developerId.isValid {
                // get access token using existing client key - secret pair
                getAccessToken(clientId, developerId)
            } else {
                print("If this happened, something went horribly wrong")
            }
            */
            
            _ = semaphore.wait(timeout: DispatchTime.now() + .seconds(5))
            
        } else {
            
            self.invoke(task: task, completion: completion)
            
        }
         */
    }
    
    private class func invoke(task: Task, completion: @escaping CompletionBlock) {
        let bbTask: BBTask = {
            
            switch task {
            case let .login(parameters):
                return Client.shared.login(with: parameters)
                
            case let .logout(parameters):
                return Client.shared.logout(with: parameters)
                
            case let .register(parameters):
                return Client.shared.register(with: parameters)
                
            case let .getMobile(parameters):
                return Client.shared.getMobile(with: parameters)
                
            case let .accessToken(parameters):
                return Client.shared.getAccessToken(with: parameters)

            case let .verifyCode(parameters):
                return Client.shared.verifyCode(with: parameters)
                
            case let .resendCode(parameters):
                return Client.shared.resendCode(with: parameters)
                
            case let .createPin(parameters):
                return Client.shared.createPin(with: parameters)
                
            case let .mobileSendCode(parameters):
                return Client.shared.mobileSendCode(with: parameters)
                
            case let .addDownload(parameters):
                return Client.shared.addDownload(with: parameters)
                
            case let .verifyPasswordCode(parameters):
                return Client.shared.verifyPasswordCode(with: parameters)
                
            case let .resetPassword(parameters):
                return Client.shared.resetPassword(with: parameters)
                
            case let .editProfile(parameters):
                return Client.shared.editProfile(with: parameters)
             
            case let .sendFunds(parameters):
                return Client.shared.sendFunds(with: parameters)
                
            case let .getUserProfile(parameters):
                return Client.shared.getUserProfile(with: parameters)
                
            case let .updateEmail(parameters):
                return Client.shared.updateEmail(with: parameters)
                
            case let .customerValidity(parameters):
                return Client.shared.customerValidity(with: parameters)
             
            case let .getUserTransactions(parameters):
                return Client.shared.getUserTransactions(with: parameters)
                
            case let .clearPin(parameters):
                return Client.shared.clearPin(with: parameters)
                
            case let .merchantList(parameters):
                return Client.shared.merchantList(with: parameters)
                
            case let .merchantRewards(parameters):
                return Client.shared.merchantRewards(with: parameters)
                
            case let .merchantLocations(parameters):
                return Client.shared.merchantLocations(with: parameters)
                
            case let .sendEmailCode(parameters):
                return Client.shared.sendEmailCode(with: parameters)
                
            case let .verifyEmailCode(parameters):
                return Client.shared.verifyEmailCode(with: parameters)
                
            case let .getAnnouncements(parameters):
                return Client.shared.getAnnouncements(with: parameters)
            }
            
        }()
        
        bbTask.perform({ (response) in
            
            let responseJSON = JSON(response)
            //print("result form task \(responseJSON as Any))")
            
            if let error = mapError(from: responseJSON) {
                // show error if needed
                
                if bbTask.shouldShowError {
                    showAlertIfNeeded(forError: error)
                }
                
                completion(nil, error)
            } else {
                
                let results = responseJSON
                
                if results != JSON.null {
                    completion(results, nil)
                } else {
                    // create NSError from message
                    let error = BBError.Message.emptyResponse.asError()
                    
                    // show error if needed
                    if bbTask.shouldShowError {
                        showAlertIfNeeded(forError: error)
                    }
                    
                    completion(nil, error)
                }
            }
        }) { (error) in
            // show error if needed
            
            //print("here \(error as Any)")
            if bbTask.shouldShowError {
                showAlertIfNeeded(forError: error)
            }
            completion(nil, error)
        }
    }
}

//MARK: - RequestAdapter
/**
 Class that conforms to Alamofire's RequestAdapter protocol. This sets access token to a URLRequest if necessary.
 */
class AccessTokenAdapter: RequestAdapter {
    private let accessToken: String
    
    init(accessToken: String) {
        self.accessToken = accessToken
    }
    
    func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
        var urlRequest = urlRequest
        urlRequest.setValue(accessToken, forHTTPHeaderField: "Authorization")
        urlRequest.timeoutInterval = 30
        return urlRequest
    }
}


//MARK: - JSON Extension
extension JSON {
    func asError() -> NSError {
        var errorCode = BBError.Code.unknownError
        if let code = self["code"].int {
            errorCode = code
            if self["message"].string == "The network connection was lost." {
                errorCode = -7003
            }
        }
        
        var errorDescription = BBError.Message.unknownError
        if let message = self["message"].string {
            errorDescription = message
        }
        
        var errorReason = errorDescription
        if let message = self["message"]["fault"]["detail"]["errorMessage"].string {
            errorReason = message
        }
        
        return NSError(domain: BBError.domain,
                       code: errorCode,
                       userInfo: [NSLocalizedDescriptionKey: errorDescription,
                                  NSLocalizedFailureReasonErrorKey: errorReason])
    }
}

// MARK: - String Extension
extension String {
    
    // Find Error Code equivalent of Error String
    func asErrorCode() -> Int {
        switch self {
        case BBError.Message.missingDeveloperInfo:
            return BBError.Code.missingDeveloperInfo
            
        case BBError.Message.noUserFound:
            return BBError.Code.noUserFound
            
        case BBError.Message.mismatchedAnswers:
            return BBError.Code.mismatchedAnswers
            
        case BBError.Message.networkError:
            return BBError.Code.networkError
            
        case BBError.Message.missingCredentials:
            return BBError.Code.missingCredentials
            
        default:
            return BBError.Code.unknownError
        }
    }
    
    func asError() -> NSError {
        return NSError(domain: BBError.domain,
                       code: self.asErrorCode(),
                       userInfo: [NSLocalizedDescriptionKey: self])
    }
}
