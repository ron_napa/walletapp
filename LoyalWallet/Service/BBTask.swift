//
//  BBTask.swift
//  GlobeAtHome
//
//  Created by Mylene Bayan on 18/09/2017.
//  Copyright © 2017 Globe Telecom, Inc. All rights reserved.
//

import Alamofire
import SwiftyJSON

class BBTask {
    private(set) var shouldShowError: Bool!
    private(set) var shouldAuthenticate: Bool!
    private(set) var urlRequest: URLRequestConvertible!
    
    private static let sessionManager = SessionManager()
    
    convenience init(urlRequest: URLRequestConvertible, shouldAuthenticate: Bool = true, shouldShowError: Bool = true) {
        self.init()
        
        self.urlRequest = urlRequest
        self.shouldAuthenticate = shouldAuthenticate
        self.shouldShowError = shouldShowError
        
    }
    
    static var accessToken: String {
        return "asd"
    }
    
    /**
     Main handler of all the requests
     
     - Parameter url: a type conforming to `URLRequestConvertible`.
     - Parameter authenticate: boolean that identifies if the `URLRequest` needs an access token.
     - Parameter completion: a callback that contains and an optional `JSON` or an optional `NSError`, depending if the the requests succeeded or not.
     
     - Returns: an instance of the `URLRequest` created with the supplied parameters.
     */
    @discardableResult
    final func perform(_ success: @escaping SuccessBlock, failure: @escaping FailureBlock) -> URLRequest? {
        
//        if let users = User.all(), users.count > 0,
//            let accessToken = KeychainUtil.value(forKey: "\(R.CacheKeys.accessToken)\(User.main().id)") as? String, shouldAuthenticate {
//
//            BBTask.sessionManager.adapter = AccessTokenAdapter(accessToken: accessToken)
//        }
        
        return BBTask.sessionManager.request(urlRequest).responseJSON(completionHandler: { (response) in
            // Do something with the response, example, convert error messages to native Error type.
            
            if let error = response.error {
                failure(self.makeError(with: error.localizedDescription))
                return
            }
            
            if let response = response.result.value {
                success(response)
            } else {
                failure(self.makeError(with: BBError.Message.unknownError))
            }
        }).request
    }
    
    //MARK: - Error Mapping
    private func mapError(from response: JSON) -> NSError? {
        if let reason = response["error"].string {
            return makeError(with: reason)
        }
        return nil
    }
    
    private func makeError(with reason: String? = nil) -> NSError {
        var code = BBError.Code.unknownError
        let reason = reason ?? BBError.Message.unknownError
        
        if reason == BBError.Message.noUserFound {
            code = BBError.Code.noUserFound
        } else if reason == BBError.Message.mismatchedAnswers {
            code = BBError.Code.mismatchedAnswers
        } else if reason == BBError.Message.networkError {
            code = BBError.Code.networkError
        } else if reason == BBError.Message.requestTimedOut {
            code = BBError.Code.requestTimedOut
        }
        
        return NSError(domain: BBError.domain,
                       code: code,
                       userInfo: [NSLocalizedDescriptionKey: reason])
    }
}
