//
//  MerchantService.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 03/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Foundation
import Alamofire

final class MerchantService: Service {
    
    //MARK:
    
    class func getMerchantList(userLat: String, userLong: String, completion: @escaping CompletionBlock) {
    
        var parameters = Parameters()
        parameters["lat"] = userLat
        parameters["lon"] = userLong
        
        perform(task: .merchantList(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response,nil)
            }
        }
    }
    
    class func getMerchantRewards(merchantCode: Int, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["code"] = merchantCode
        
        perform(task: .merchantRewards(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response,nil)
            }
        }
    }
    
    class func getRewards(completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["code"] = ""
        
        perform(task: .merchantRewards(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response,nil)
            }
        }
    }
    
    class func getMerchantLocations(userLat: String, userLong: String, merchantCode: Int, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        if userLat.count > 0 {
            parameters["lat"] = userLat
        }
        if userLong.count > 0 {
            parameters["lon"] = userLong
        }
        parameters["merchantCode"] = merchantCode
        
        print(parameters)
        
        perform(task: .merchantLocations(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response,nil)
            }
        }
    }
}
