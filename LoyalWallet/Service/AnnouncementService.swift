//
//  AnnouncementService.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 26/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import SwiftyJSON

class AnnouncementService: Service {
    
    class func getAnnouncements(code:String, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["code"] = code
        
        perform(task: .getAnnouncements(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response,nil)
            }
        }
    }
}
