//
//  UserService.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 18/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import WXImageCompress

class UserService: Service {
    
    class func editProfile(firstName: String, lastName: String, mobileNumber: String, email: String, gender: String, dateOfBirth: String, birthPlace: String, nationality: String, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["first_name"] = firstName
        parameters["last_name"] = lastName
        parameters["mobile_no"] = mobileNumber
        parameters["email"] = email
        parameters["gender"] = gender
        parameters["dateofbirth"] = dateOfBirth
        parameters["birthplace"] = birthPlace
        parameters["nationality"] = nationality
        parameters["profileImage"] = nationality
        
        perform(task: .editProfile(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response,nil)
            }
        }
    }
    
    class func editProfileMultiPart(firstName: String, lastName: String, mobileNumber: String, email: String, gender: String, dateOfBirth: String, birthPlace: String, nationality: String, selectedImage:UIImage, completion: @escaping CompletionBlock) {
        
        var tokenUser = ""
        if let userToken = UserDefaults.standard.string(forKey: R.CacheKeys.userToken) {
            tokenUser = "JWT \(userToken)"
        }
        
        print(tokenUser)
        
        let params: [String:Any] = ["first_name" : firstName,
                                    "last_name" : lastName,
                                    "mobile_no" : mobileNumber,
                                    "email" : email,
                                    "gender" : gender,
                                    "dateofbirth" : dateOfBirth,
                                    "birthplace" : birthPlace,
                                    "nationality" : nationality]
        
        
        let url = URL.init(string: BASE_URL + "customer/editProfile")
        
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                for (key, value) in params {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key )
                }
                
                print(params)
                
                let size = CGSize(width: 400.0, height: 400.0)
                let aspectScaledToFitImage = selectedImage.af_imageAspectScaled(toFill: size)
                
                if let imageData = UIImageJPEGRepresentation(aspectScaledToFitImage.wxCompress(), 1) {
                    multipartFormData.append(imageData, withName: "profileImage", fileName: "file\(Date()).jpeg", mimeType: "image/jpeg")
                }

        },
            to: url!,
            method:HTTPMethod.put,
            headers:["Authorization": tokenUser,
                     "Content-Type": "application/x-www-form-urlencoded"],
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseString { response in
                        
                        if let data = response.result.value{
                            completion(JSON.init(parseJSON: data),nil)
                        }

                        }
                        .uploadProgress { progress in
                            // main queue by default
                            //self.img1Progress.alpha = 1.0
                            //self.img1Progress.progress = Float(progress.fractionCompleted)
                            
                            //print("Upload Progress: \(progress.fractionCompleted)")
                    }
                    return
                case .failure(let encodingError):
                    completion(nil, encodingError as NSError)
                }
                
        })
    }
    
    class func getUserProfile(customerId:String, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["code"] = customerId
        
        perform(task: .getUserProfile(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response,nil)
            }
        }
    }
    
    class func getUserTransactions(count:Int, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["code"] = count
        
        perform(task: .getUserTransactions(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response,nil)
            }
        }
    }
    
    class func updateEmail(email:String, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["email"] = email
        
        perform(task: .updateEmail(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response,nil)
            }
        }
    }
    
    class func sendFunds(type:String, value:String, amount:String, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["type"] = type
        parameters["value"] = value
        parameters["amount"] = amount
        
        print(parameters)
        
        perform(task: .sendFunds(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response,nil)
            }
        }
    }
    
    class func customerValidity(type:String, value:String, completion: @escaping CompletionBlock) {
        
        var parameters = Parameters()
        parameters["type"] = type
        parameters["value"] = value
        
        perform(task: .customerValidity(parameters)) { (response, error) in
            if let error = error {
                completion(nil, error)
            } else {
                completion(response,nil)
            }
        }
    }
    
}
