//
//  Client.swift
//  GlobeAtHome
//
//  Created by Mylene Bayan on 18/09/2017.
//  Copyright © 2017 Globe Telecom, Inc. All rights reserved.
//

import Foundation
import Alamofire

class Client {
    static let shared = Client()
    
    //MARK: - Auth Service
    func login(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.login(parameters), shouldAuthenticate: false)
    }
    
    func logout(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.logout(parameters), shouldAuthenticate: false)
    }
    
    func getAccessToken(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.getAccessToken(parameters), shouldAuthenticate: false)
    }
    
    func register(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.register(parameters), shouldAuthenticate: false)
    }
    
    func getMobile(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.getMobile(parameters), shouldAuthenticate: false)
    }
    
    func verifyCode(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.verifyCode(parameters), shouldAuthenticate: false)
    }
    
    func resendCode(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.resendCode(parameters), shouldAuthenticate: false)
    }
    
    func createPin(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.createPin(parameters), shouldAuthenticate: false)
    }
    
    func mobileSendCode(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.mobileSendCode(parameters), shouldAuthenticate: false)
    }
    
    func addDownload(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.addDownload(parameters), shouldAuthenticate: false)
    }
    
    func verifyPasswordCode(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.verifyPasswordCode(parameters), shouldAuthenticate: false)
    }
    
    func resetPassword(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.resetPassword(parameters), shouldAuthenticate: false)
    }
    
    func editProfile(with parameteres: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.editProfile(parameteres), shouldAuthenticate: false)
    }
    
    func getUserProfile(with parameteres: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.getUserProfile(parameteres), shouldAuthenticate: false)
    }
    
    func updateEmail(with parameteres: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.updateEmail(parameteres), shouldAuthenticate: false)
    }
    
    func sendFunds(with parameteres: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.sendFunds(parameteres), shouldAuthenticate: false)
    }
 
    func customerValidity(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.customerValidity(parameters), shouldAuthenticate: false)
    }
    
    func getUserTransactions(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.getUserTransactions(parameters), shouldAuthenticate: false)
    }
    
    func clearPin(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.clearPin(parameters), shouldAuthenticate: false)
    }
    
    func merchantList(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.merchantList(parameters), shouldAuthenticate: false)
    }
    
    func merchantRewards(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.merchantRewards(parameters), shouldAuthenticate: false)
    }
    
    func merchantLocations(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.merchantLocations(parameters), shouldAuthenticate: false)
    }
    
    func sendEmailCode(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.sendEmailCode(parameters), shouldAuthenticate: false)
    }
    
    func verifyEmailCode(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.verifyEmailCode(parameters), shouldAuthenticate: false)
    }
    
    func getAnnouncements(with parameters: Parameters) -> BBTask {
        return BBTask(urlRequest: Router.getAnnouncements(parameters), shouldAuthenticate: false)
    }

}
