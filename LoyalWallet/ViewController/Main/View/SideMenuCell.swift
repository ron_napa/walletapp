//
//  SideMenuCell.swift
//  GongchaWallet
//
//  Created by Ronald Napa on 08/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit

class SideMenuCell: UITableViewCell {

    @IBOutlet weak var menuLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
