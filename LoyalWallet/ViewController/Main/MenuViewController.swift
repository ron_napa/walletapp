//
//  Menu.swift
//  GongchaWallet
//
//  Created by Nestor Calizar Jr. on 19/09/2017.
//  Copyright © 2017 Appsolutely Inc. All rights reserved.
//

import Foundation
import UIKit
import SWRevealViewController
import Firebase
import SwiftSpinner
import Realm
import RealmSwift

class MenuViewController: UIViewController {
    
    @IBOutlet weak fileprivate var tableView: UITableView!
    let sideMenuItems : [String] = ["Account Verification",
                                    "Announcements",
                                    "About Us",
                                    "Terms & Conditions",
                                    "Contact Us",
                                    "Log out"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Private Functions
    func logOut() {
        
        let refreshAlert = UIAlertController(title: "LoyalWallet", message: "Are you sure you want to log out?", preferredStyle: UIAlertControllerStyle.alert)
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            SwiftSpinner.show("")
            RegisterService.logout { (response, error) in
                SwiftSpinner.hide()
                print(response as Any)
                if error != nil {
                    
                } else {
                    
                    let realm = try! Realm()
                    try! realm.write {
                        realm.deleteAll()
                    }
                    
                    let vc = R.Storyboards.register.instantiateViewController(withIdentifier: "RegisterCarouselViewController") as! RegisterCarouselViewController
                    UserSession.sharedInstance.willShowLogin = true
                    let navController = UINavigationController(rootViewController: vc)
                    Utils.setRootViewController(navController)
                }
            }
        }))
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            //print("Handle Cancel Logic here")
        }))
        present(refreshAlert, animated: true, completion: nil)
        UserSession.sharedInstance.allAlertController.append(refreshAlert)
    }
    
    func deleteDeviceToken(){
//
//        let params: NSMutableDictionary = NSMutableDictionary()
//
//        let cusomterID = Utils.getValueFromPreferencesWith(Key: "CUSTOMER_ID") as! String
//
//        params[RegisterDeviceKey.customer_id.rawValue] = cusomterID
//        params[RegisterDeviceKey.deviceID.rawValue] = Utils.getDeviceID()
//
//        let urlString = mainURL + Endpoint.deleteDeviceToken.rawValue
//        let apiRequest = APIRequest()
//
//        apiRequest.sendRequest(urlString, params as! [String : Any]) { (Success) in
//
//            let response = apiRequest.Response
//
//            if Success {
//
//
//
//            } else {
//
//
//            }
//        }
//
    }
    
    func openProducts() {
//
//        UIView.animate(withDuration: 0.3, animations: {
//            self.revealViewController().revealToggle(animated: true)
//        }) { (Success) in
//            self.revealViewController().addFadeTransition()
//            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//            let productsView = storyboard.instantiateViewController(withIdentifier: "productsVC") as! ProductsView
//
//            self.revealViewController().present(productsView, animated: false, completion: nil)
//        }
    }
    
    func openLocations() {
//        UIView.animate(withDuration: 0.3, animations: {
//            self.revealViewController().revealToggle(animated: true)
//        }) { (Success) in
//            self.revealViewController().addFadeTransition()
//            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//            let productsView = storyboard.instantiateViewController(withIdentifier: "LocationsVC") as! Locations
//
//            self.revealViewController().present(productsView, animated: false, completion: nil)
//        }
    }
    
    func openPromos() {
//        UIView.animate(withDuration: 0.3, animations: {
//            self.revealViewController().revealToggle(animated: true)
//        }) { (Success) in
//            self.revealViewController().addFadeTransition()
//            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
//            let productsView = storyboard.instantiateViewController(withIdentifier: "promosVC") as! PromosVC
//
//            self.revealViewController().present(productsView, animated: false, completion: nil)
//        }
    }
    
    func openAboutUs(){
        UIView.animate(withDuration: 0.3, animations: {
            self.revealViewController().revealToggle(animated: true)
        }) { (Success) in
            //self.revealViewController().addFadeTransition()
            let showItemStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let showItemVc = showItemStoryboard.instantiateViewController(withIdentifier: "AboutUsViewController")
            self.present(showItemVc, animated: false, completion: nil)
        }
    }
    
    func openAccountVerification(){
        UIView.animate(withDuration: 0.3, animations: {
            self.revealViewController().revealToggle(animated: true)
        }) { (Success) in
            //self.revealViewController().addFadeTransition()
            let showItemStoryboard = UIStoryboard(name: "AccountVerification", bundle: nil)
            let showItemVc = showItemStoryboard.instantiateViewController(withIdentifier: "AccountVerificationNavigation")
            self.present(showItemVc, animated: false, completion: nil)
        }
    }
    
    func openAnnouncements(){
        UIView.animate(withDuration: 0.3, animations: {
            self.revealViewController().revealToggle(animated: true)
        }) { (Success) in
            //self.revealViewController().addFadeTransition()
            let showItemStoryboard = UIStoryboard(name: "Announcements", bundle: nil)
            let showItemVc = showItemStoryboard.instantiateViewController(withIdentifier: "AnnouncementsNavigation")
            self.present(showItemVc, animated: false, completion: nil)
        }
    }
    
    func openContactUs(){
        UIView.animate(withDuration: 0.3, animations: {
            self.revealViewController().revealToggle(animated: true)
        }) { (Success) in
            //self.revealViewController().addFadeTransition()
            let showItemStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let showItemVc = showItemStoryboard.instantiateViewController(withIdentifier: "ContactUsViewController")
            self.present(showItemVc, animated: false, completion: nil)
        }
    }
    
    func openTermsAndCondition(){
        UIView.animate(withDuration: 0.3, animations: {
            self.revealViewController().revealToggle(animated: true)
        }) { (Success) in
            //self.revealViewController().addFadeTransition()
            let showItemStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let showItemVc = showItemStoryboard.instantiateViewController(withIdentifier: "TermsAndConditionsViewController")
            self.present(showItemVc, animated: false, completion: nil)
        }
    }
    
    //MARK: - Social Media
    @IBAction func twitter(_ sender: Any) {
        if let url = URL(string: "https://mobile.twitter.com/loyalcoin?lang=en"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func facebook(_ sender: Any) {
        if let url = URL(string: "https://www.facebook.com/LYLcoin/"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
       
    }

    @IBAction func instagram(_ sender: Any) {
        if let url = URL(string: "https://www.instagram.com/gongchaphils/"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
       
    }

}

extension MenuViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuItems.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let sideMenuCell = tableView.dequeueReusableCell(withIdentifier: "sideMenuCell", for: indexPath) as? SideMenuCell
        sideMenuCell?.menuLabel.text = sideMenuItems[indexPath.row]
        sideMenuCell?.selectionStyle = .none
        return sideMenuCell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        /*
         "Account Verification",
         "Announcements",
         "Vouchers",
         "Locations",
         "About Us",
         "Terms & Conditions",
         "Contact Us",
         "Log out"
         */
        print(indexPath.row)
        switch indexPath.row {
        case 0:
            self.openAccountVerification()
        case 1:
            self.openAnnouncements()
        case 2:
            self.openAboutUs()
        case 3:
            self.openTermsAndCondition()
        case 4:
            self.openContactUs()
        case 5:
            self.logOut()
        default:
            print("Impossibru")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
}

