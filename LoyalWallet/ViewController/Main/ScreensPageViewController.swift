//
//  ScreensPageViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 12/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit

protocol ScreensPageViewControllerDelegate {
    func screensPageViewController(_ viewController: ScreensPageViewController, didUpdateTab index: Int)
    func screensPageViewController(_ viewController: ScreensPageViewController, showMerchantInfo index: Int)
    func screensPageViewController(_ viewController: ScreensPageViewController, walletAction index: Int)
    func screensPageViewController(_ viewController: ScreensPageViewController, didSelectTransaction index: Int)
}

class ScreensPageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    var screenPageViewDelegate: ScreensPageViewControllerDelegate?
    
    // MARK: UIPageViewControllerDataSource
    var orderedViewControllers: [UIViewController] = []
    /*
    lazy var orderedViewControllers: [UIViewController] = {
        return [R.Storyboards.main.instantiateViewController(withIdentifier: "MerchantListViewController"),
                R.Storyboards.main.instantiateViewController(withIdentifier: "RewardsListViewController"),
                R.Storyboards.register.instantiateViewController(withIdentifier: "RegisterCarouselContentViewController"),
                R.Storyboards.register.instantiateViewController(withIdentifier: "RegisterCarouselContentViewController")]
    }()
    */
    
    var merchantListViewController : MerchantListViewController?
    var rewardsListViewController : RewardsListViewController?
    var profileViewController : ProfileViewController?
    var currentPageIndex : Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        
        //disable the horizontal scrolling of PageView
        for view in self.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
        
        setupEmbeddedScreens()
        
        // This sets up the first view that will show up on our page control
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
        
        // Do any additional setup after loading the view.
    }
    
    func newVc(viewController: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: viewController)
    }
    
    func scrollToViewController(index newIndex: Int) {
        
        if currentPageIndex == 3 && newIndex != 3 {
            if let isChanged = profileViewController?.checkIfChangedInfo(), isChanged {
                if let topViewController = Utils.topViewController() {
                    let refreshAlert = UIAlertController(title: "LoyalWallet", message: "Your changes will be discarded.", preferredStyle: UIAlertControllerStyle.alert)
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                        self.profileViewController?.updateDisplay()
                        self.profileViewController?.selectedUserProfileImageView.image = nil
                        self.scrollToNewViewController(index: newIndex)
                    }))
                    refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                        return
                    }))
                    topViewController.present(refreshAlert, animated: true, completion: nil)
                    UserSession.sharedInstance.allAlertController.append(refreshAlert)
                }
            } else {
                scrollToNewViewController(index: newIndex)
            }
        } else {
            scrollToNewViewController(index: newIndex)
        }
        
    }
    
    func scrollToNewViewController(index newIndex: Int) {
        
        currentPageIndex = newIndex
        
        if let firstViewController = viewControllers?.first,
            let currentIndex = orderedViewControllers.index(of: firstViewController) {
            let direction: UIPageViewControllerNavigationDirection = newIndex >= currentIndex ? .forward : .reverse
            let nextViewController = orderedViewControllers[newIndex]
            self.scrollToViewController(viewController: nextViewController, direction: direction)
        }
    }
    
    func scrollToViewController(viewController: UIViewController,
                                        direction: UIPageViewControllerNavigationDirection) {
        setViewControllers([viewController],
                           direction: direction,
                           animated: true,
                           completion: { (finished) -> Void in
                            self.notifyTutorialDelegateOfNewIndex()
        })
    }
    
    func notifyTutorialDelegateOfNewIndex() {
        if let firstViewController = viewControllers?.first,
            let index = orderedViewControllers.index(of:firstViewController) {
            //mainPageDelegate?.pageViewController(self, didUpdatePageIndex: index)
            screenPageViewDelegate?.screensPageViewController(self, didUpdateTab: index)
        }
    }
    
    func setupEmbeddedScreens() {
        
        let merchantViewController = R.Storyboards.main.instantiateViewController(withIdentifier: "MerchantListViewController") as! MerchantListViewController
        merchantViewController.delegate = self
        merchantListViewController = merchantViewController
        self.orderedViewControllers.append(merchantViewController)
        
        let rewardsViewController = R.Storyboards.main.instantiateViewController(withIdentifier: "RewardsListViewController") as! RewardsListViewController
        rewardsListViewController = rewardsViewController
        self.orderedViewControllers.append(rewardsViewController)
        
        let walletController = R.Storyboards.main.instantiateViewController(withIdentifier: "WalletViewController") as! WalletViewController
        walletController.delegate = self
        self.orderedViewControllers.append(walletController)
        
        let profilesViewController = R.Storyboards.main.instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
        self.profileViewController = profilesViewController
        self.orderedViewControllers.append(profilesViewController)
        
    }
    
    func searchItem(searchKey : String, retainPosition: Bool) {
        
        switch currentPageIndex {
        case 0:
            merchantListViewController?.searchMerchant(searchKey: searchKey, retainPosition: retainPosition)
        case 1:
            rewardsListViewController?.searchReward(searchKey: searchKey)
        default:
            merchantListViewController?.searchMerchant(searchKey: searchKey, retainPosition: retainPosition)
        }
        
    }
    
    // MARK: Delegate methords
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        //self.pageControl.currentPage = orderedViewControllers.index(of: pageContentViewController)!
        //return page index
        screenPageViewDelegate?.screensPageViewController(self, didUpdateTab: orderedViewControllers.index(of: pageContentViewController)!)
    }
    
    // MARK: Data source functions.
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        // User is on the first view controller and swiped left to loop to
        // the last view controller.
        guard previousIndex >= 0 else {
            //return orderedViewControllers.last
            // Uncommment the line below, remove the line above if you don't want the page control to loop.
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        // User is on the last view controller and swiped right to loop to
        // the first view controller.
        guard orderedViewControllersCount != nextIndex else {
            //return orderedViewControllers.first
            //Uncommment the line below, remove the line above if you don't want the page control to loop.
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }

}

extension ScreensPageViewController : MerchantListViewControllerDelegate {
    func merchantListViewController(_ viewController: MerchantListViewController, didTapMerchant index: Int) {
        self.screenPageViewDelegate?.screensPageViewController(self, showMerchantInfo: index)
    }
}

extension ScreensPageViewController : WalletViewControllerDelegate {
    func walletViewController(_ viewController: WalletViewController, didTapActionButton index: Int) {
        self.screenPageViewDelegate?.screensPageViewController(self, walletAction: index)
    }
    
    func walletViewController(_ viewController: WalletViewController, didSelectTransaction index: Int) {
        self.screenPageViewDelegate?.screensPageViewController(self, didSelectTransaction: index)
    }
    
    func walletViewController(_ viewController: WalletViewController, updatedTableViewHeight check: Bool) {
        
    }
    
}
