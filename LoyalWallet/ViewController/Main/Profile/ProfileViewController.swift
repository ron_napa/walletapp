//
//  ProfileViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 13/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Alamofire
import AlamofireImage
import SwiftSpinner
import SwiftyJSON

class ProfileViewController: MainViewController{

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var userProfileImageView: UIImageView!
    @IBOutlet weak var selectedUserProfileImageView: UIImageView!
    @IBOutlet weak var profileImageIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var firstNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var mobileNumberTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var radio1Button: UIButton!
    @IBOutlet weak var radio2Button: UIButton!
    @IBOutlet weak var birthdateTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var birthdateTextFieldLine: UIView!
    @IBOutlet weak var birthPlaceTextField: IQDropDownTextField!
    @IBOutlet weak var birthPlaceTextFieldLine: UIView!
    @IBOutlet weak var nationalityTextField: IQDropDownTextField!
    @IBOutlet weak var nationalityTextFieldLine: UIView!
    
    fileprivate var datePicker: DatePickerView!
    var selectedDate: Date!
    
    var isPickerSelect = false
    var radioButtonArray : [UIButton] = []
    var selectedGender = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        radioButtonArray = [radio1Button,
                       radio2Button]
        
        self.updateDisplay()
        
        self.scrollView.spr_setIndicatorHeader { [weak self] in
            self?.selectedUserProfileImageView.image = nil
            self?.getUserProfile(isReload: false)
        }

        birthdateTextField.addTarget(self, action: #selector(didRecognizeTapGesture(_:)), for:  UIControlEvents.touchUpInside)
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        updateDisplay()
        
        if !isPickerSelect {
            //selectedUserProfileImageView.image = nil
            //selectedUserProfileImageView.setNeedsDisplay()
        }
        
        datePicker = Bundle.main.loadNibNamed("DatePickerView", owner: self, options: nil)?.first as! DatePickerView
        datePicker.delegate = self
        datePicker.addDatePicker(parentView: self.view)
        
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            emailTextField.isEnabled = !currentUser.emailVerified
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        datePicker.removeDatePicker()
    }
    
    //MARK: Private Functions
    func updateDisplay() {
        
        self.scrollView.spr_endRefreshing()
        
        birthPlaceTextField.isOptionalDropDown = false
        nationalityTextField.isOptionalDropDown = false
        
        let path = Bundle.main.path(forResource: "countries", ofType: "json")!
        let jsonString = try? String(contentsOfFile: path, encoding: String.Encoding.utf8)
        let json = JSON(parseJSON: jsonString!)
        
        var countries : [String] = []
        for country in json {
            let countryJSON = country.1
            countries.append(countryJSON["Name"].string!)
        }
        
        let nationality : [String] = ["Select","Afghan","Albanian","Algerian","American","Andorran","Angolan","Antiguans","Argentinean","Armenian","Australian","Austrian","Azerbaijani","Bahamian","Bahraini","Bangladeshi","Barbadian","Barbudans","Batswana","Belarusian","Belgian","Belizean","Beninese","Bhutanese","Bolivian","Bosnian","Brazilian","British","Bruneian","Bulgarian","Burkinabe","Burmese","Burundian","Cambodian","Cameroonian","Canadian","Cape Verdean","Central African","Chadian","Chilean","Chinese","Colombian","Comoran","Congolese","Costa Rican","Croatian","Cuban","Cypriot","Czech","Danish","Djibouti","Dominican","Dutch","East Timorese","Ecuadorean","Egyptian","Emirian","Equatorial Guinean","Eritrean","Estonian","Ethiopian","Fijian","Filipino","Finnish","French","Gabonese","Gambian","Georgian","German","Ghanaian","Greek","Grenadian","Guatemalan","Guinea-Bissauan","Guinean","Guyanese","Haitian","Herzegovinian","Honduran","Hungarian","I-Kiribati","Icelander","Indian","Indonesian","Iranian","Iraqi","Irish","Israeli","Italian","Ivorian","Jamaican","Japanese","Jordanian","Kazakhstani","Kenyan","Kittian and Nevisian","Kuwaiti","Kyrgyz","Laotian","Latvian","Lebanese","Liberian","Libyan","Liechtensteiner","Lithuanian","Luxembourger","Macedonian","Malagasy","Malawian","Malaysian","Maldivian","Malian","Maltese","Marshallese","Mauritanian","Mauritian","Mexican","Micronesian","Moldovan","Monacan","Mongolian","Moroccan","Mosotho","Motswana","Mozambican","Namibian","Nauruan","Nepalese","New Zealander","Ni-Vanuatu","Nicaraguan","Nigerian","Nigerien","North Korean","Northern Irish","Norwegian","Omani","Pakistani","Palauan","Panamanian","Papua New Guinean","Paraguayan","Peruvian","Polish","Portuguese","Qatari","Romanian","Russian","Rwandan","Saint Lucian","Salvadoran","Samoan","San Marinese","Sao Tomean","Saudi","Scottish","Senegalese","Serbian","Seychellois","Sierra Leonean","Singaporean","Slovakian","Slovenian","Solomon Islander","Somali","South African","South Korean","Spanish","Sri Lankan","Sudanese","Surinamer","Swazi","Swedish","Swiss","Syrian","Taiwanese","Tajik","Tanzanian","Thai","Togolese","Tongan","Trinidadian or Tobagonian","Tunisian","Turkish","Tuvaluan","Ugandan","Ukrainian","Uruguayan","Uzbekistani","Venezuelan","Vietnamese","Welsh","Yemenite","Zambian","Zimbabwean"]
        
        birthPlaceTextField.itemList = countries
        birthPlaceTextField.delegate = self
        nationalityTextField.itemList = nationality
        nationalityTextField.delegate = self
        
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            
            firstNameTextField.text = currentUser.firstName ?? ""
            lastNameTextField.text = currentUser.lastName ?? ""
            mobileNumberTextField.text = currentUser.mobileNo ?? ""
            emailTextField.text = currentUser.email ?? ""
            
            birthPlaceTextField.selectedItem = currentUser.birthPlace?.count == 0 ? "Select" : currentUser.birthPlace
            nationalityTextField.selectedItem = currentUser.nationality?.count == 0 ? "Select" : currentUser.nationality
            selectedDate = currentUser.dateOfBirth?.dateValue()
            
            if currentUser.dateOfBirth == "0000-00-00" || currentUser.dateOfBirth?.count == 0 {
                selectedDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
                birthdateTextField.text = "YYYY/MM/DD"
            } else {
                selectedDate = currentUser.dateOfBirth?.dateValue()
                birthdateTextField.text = selectedDate.stringValue(format: "YYYY/MM/dd")
            }
            
            for radioButton in radioButtonArray {
                radioButton.setImage(UIImage(named: "ellipse_default"), for: UIControlState.normal)
            }
            
            selectedGender = ""
            
            if currentUser.gender == "m" {
                radio1Button.sendActions(for: .touchUpInside)
                selectedGender = "m"
            } else if currentUser.gender == "f" {
                radio2Button.sendActions(for: .touchUpInside)
                selectedGender = "f"
            }
            
            self.userProfileImageView.image = nil
            self.userProfileImageView.layer.cornerRadius = self.userProfileImageView.frame.size.width / 2
            self.userProfileImageView.clipsToBounds = true
            
            if currentUser.profileImage != "" {
                userProfileImageView.image = UIImage(named: "default_profile")
                userProfileImageView.layer.cornerRadius = userProfileImageView.frame.size.width / 2
                userProfileImageView.clipsToBounds = true
                self.profileImageIndicatorView.isHidden = false
                ImageDownloaderManager.downloadImage(urlString: (currentUser.profileImage)!) { (image) in
                    self.userProfileImageView.image = image
                    self.userProfileImageView.layer.cornerRadius = self.userProfileImageView.frame.size.width / 2
                    self.userProfileImageView.clipsToBounds = true
                    self.profileImageIndicatorView.isHidden = true
                }
            } else {
                userProfileImageView.layer.cornerRadius = userProfileImageView.frame.size.width / 2
                userProfileImageView.clipsToBounds = true
                userProfileImageView.image = UIImage(named: "default_profile")
            }
            
        }
    }
    
    func getUserProfile(isReload:Bool) {
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            UserService.getUserProfile(customerId: "\(currentUser.id)", completion: { (response, error) in
                print(response as Any)
                SwiftSpinner.hide()
                if error != nil {
                    //AlertviewManager.showAlertInfo(message: (error?.description)!, view: self)
                } else {
                    
                    let userResponse = response!["customer"]
                    
                    try! RealmManager.realm?.write {
                        currentUser.mobileNo = userResponse["mobile_no"].string ?? ""
                        currentUser.email = userResponse["email"].string ?? ""
                        currentUser.profileImage = userResponse["profile_image"].string ?? ""
                        currentUser.activationCode = userResponse["activation_code"].string ?? ""
                        currentUser.pinCode = userResponse["pin_code"].string ?? ""
                        currentUser.lylAddress = userResponse["lyl_address"].string ?? ""
                        currentUser.lylBalance = userResponse["lyl_balance"].double ?? 0.0
                        currentUser.firstName = userResponse["first_name"].string ?? ""
                        currentUser.middleName = userResponse["middle_name"].string ?? ""
                        currentUser.lastName = userResponse["last_name"].string ?? ""
                        currentUser.gender = userResponse["gender"].string ?? ""
                        currentUser.dateOfBirth = userResponse["dateofbirth"].string ?? ""
                        currentUser.birthPlace = userResponse["birthplace"].string ?? ""
                        currentUser.nationality = userResponse["nationality"].string ?? ""
                        currentUser.registeredLocation = userResponse["reg_location"].string ?? ""
                        currentUser.lastTransactionDate = userResponse["last_transaction_date"].string ?? ""
                        currentUser.phoneVerified = userResponse["phone_verified"].int == 1 ? true : false
                        currentUser.emailVerified = userResponse["email_verified"].int == 1 ? true : false
                        currentUser.addressVerified = userResponse["address_verified"].int == 1 ? true : false
                        currentUser.selfieVerified = userResponse["selfie_verified"].int == 1 ? true : false
                        currentUser.indentityVerified = userResponse["indentity_verified"].int == 1 ? true : false
                    }
                    
                    if isReload {
                        AlertviewManager.showAlertInfo(message: "You have successfully updated your profile", view: self)
                    }
                    
                    self.updateDisplay()
                }
            })
        }
    }
    
    @objc private dynamic func didRecognizeTapGesture(_ gesture: UITapGestureRecognizer) {
        //view.endEditing(true)
        let point = gesture.location(in: gesture.view)
        guard gesture.state == .ended, birthdateTextField.frame.contains(point) else { return }
        datePicker.showDatePicker(selectedDate: selectedDate)
    }
    
    func checkIfChangedInfo() -> Bool {
        
        if firstNameTextField == nil {
            return false
        }
        
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            
            if selectedUserProfileImageView.image != nil {
                return true
            }
            
            if firstNameTextField.text != currentUser.firstName {
                return true
            }
            
            if lastNameTextField.text != currentUser.lastName {
                return true
            }
            
            if mobileNumberTextField.text != currentUser.mobileNo {
                return true
            }
            
            if emailTextField.text != currentUser.email {
                return true
            }
            
            if birthdateTextField.text != "YYYY/MM/DD" {
                if birthdateTextField.text != currentUser.dateOfBirth?.dateValue()?.stringValue(format: "YYYY/MM/dd") {
                    return true
                }
            }
            
            if birthPlaceTextField.selectedItem != "Select" {
                if birthPlaceTextField.selectedItem != currentUser.birthPlace {
                    return true
                }
            }
            
            if nationalityTextField.selectedItem != "Select" {
                if nationalityTextField.selectedItem != currentUser.nationality {
                    return true
                }
            }
            
            if selectedGender != currentUser.gender {
                return true
            }
            
        }
        
        return false
    }
    
    func setAllLinesBlack() {
        self.birthdateTextFieldLine.backgroundColor = UIColor.black
        self.birthPlaceTextFieldLine.backgroundColor = UIColor.black
        self.nationalityTextFieldLine.backgroundColor = UIColor.black
    }
    
    func updateUserProfile() {
        UserService.editProfileMultiPart(firstName: firstNameTextField.text!,
                                         lastName: lastNameTextField.text!,
                                         mobileNumber: mobileNumberTextField.text!,
                                         email: emailTextField.text!,
                                         gender: selectedGender,
                                         dateOfBirth: birthdateTextField.text!,
                                         birthPlace: birthPlaceTextField.selectedItem!,
                                         nationality: nationalityTextField.selectedItem!,
                                         selectedImage: (selectedUserProfileImageView.image != nil ? selectedUserProfileImageView.image : userProfileImageView.image)!,
                                         completion: { (response, error) in
                                            if error != nil {
                                                SwiftSpinner.hide()
                                            } else {
                                                if response!["code"].int != 200 {
                                                    SwiftSpinner.hide()
                                                    AlertviewManager.showAlertInfo(message: "Unable to update profile.", view: self)
                                                } else {
                                                    self.selectedUserProfileImageView.image = nil
                                                    self.getUserProfile(isReload: true)
                                                }
                                            }
        })
    }
    
    //MARK: User Interactions
    @IBAction func didStartEditing(_ sender: Any) {
        view.endEditing(true)
        scrollView.scrollToBottom()
        setAllLinesBlack()
        datePicker.showDatePicker(selectedDate: selectedDate)
    }
    
    @IBAction func didTapNationality(_ sender: Any) {
        setAllLinesBlack()
        self.nationalityTextFieldLine.backgroundColor = UIColor.defaultYellow
    }
    
    @IBAction func didHideDropDown(_ sender: Any) {
        setAllLinesBlack()
    }
    
    @IBAction func didTapBirthplace(_ sender: Any) {
        setAllLinesBlack()
        self.birthPlaceTextFieldLine.backgroundColor = UIColor.defaultYellow
    }
    
    @IBAction func didTapContactUs(_ sender: Any) {
        let showItemStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let showItemVc = showItemStoryboard.instantiateViewController(withIdentifier: "ContactUsViewController")
        self.present(showItemVc, animated: false, completion: nil)
    }
    
    @IBAction func didTapSelectPhoto(_ sender: Any) {
        isPickerSelect = true
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func didTapGenderSelect(_ sender: Any) {
        
        let selectedButton = sender as! UIButton
        selectedGender = selectedButton.tag == 0 ? "m" : "f"
        
        for radioButton in radioButtonArray {
            radioButton.setImage(UIImage(named: "ellipse_default"), for: UIControlState.normal)
        }
        
        selectedButton.setImage(UIImage(named: "ellipse_selected"), for: UIControlState.normal)
        
    }
    
    @IBAction func didTapCreate(_ sender: Any) {
        
        if !checkIfChangedInfo() {
            return
        }
        
        if firstNameTextField.text?.count == 0 &&
           lastNameTextField.text?.count == 0 &&
           mobileNumberTextField.text?.count == 0 &&
           birthdateTextField.text?.count == 0 &&
           emailTextField.text?.count == 0 {
            AlertviewManager.showAlertInfo(message: "Please fill in required fields.", view: self)
            return
        }
        
        if !emailTextField.text!.matchExists(for: R.RegexPattern.patternEmail){
            AlertviewManager.showAlertInfo(message: "Please provide valid email address.", view: self)
            return
        }
        
        if !mobileNumberTextField.text!.matchExists(for: R.RegexPattern.patternMobileNumber) {
            AlertviewManager.showAlertInfo(message: "Please provide valid mobile number.", view: self)
            return
        }
        
        if !firstNameTextField.text!.matchExists(for: R.RegexPattern.patternName){
            AlertviewManager.showAlertInfo(message: "Please provide valid first name.", view: self)
            return
        }
        
        if !lastNameTextField.text!.matchExists(for: R.RegexPattern.patternName){
            AlertviewManager.showAlertInfo(message: "Please provide valid last name.", view: self)
            return
        }
        
        if birthdateTextField.text == "YYYY/MM/DD" {
            AlertviewManager.showAlertInfo(message: "Please enter your birthday.", view: self)
            return
        }
        
        SwiftSpinner.show("")
        
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            if currentUser.email == self.emailTextField.text {
                self.updateUserProfile()
            } else {
                UserService.updateEmail(email: self.emailTextField.text!,
                                        completion: { (response, error) in
                                            print(response as Any)
                                            if error != nil {
                                                SwiftSpinner.hide()
                                            } else {
                                                if response!["code"].int != 200 {
                                                    SwiftSpinner.hide()
                                                    if response!["code"].int == 400 {
                                                        if "Invalid Email" == response!["message"].string!{
                                                            AlertviewManager.showAlertInfo(message: "Please provide valid email address.", view: self)
                                                        } else {
                                                            AlertviewManager.showAlertInfo(message: "Email address provided is already registered.", view: self)
                                                        }
                                                    } else {
                                                        AlertviewManager.showAlertInfo(message: response!["message"].string!, view: self)
                                                    }
                                                } else {
                                                    self.updateUserProfile()
                                                }
                                            }
                })
            }
        }
        
    }
    
}

extension ProfileViewController : UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let selectedImage = info["UIImagePickerControllerOriginalImage"] as! UIImage
        self.selectedUserProfileImageView.image = nil
        self.selectedUserProfileImageView.image = selectedImage
        self.selectedUserProfileImageView.layer.cornerRadius = self.selectedUserProfileImageView.frame.size.width / 2
        self.selectedUserProfileImageView.clipsToBounds = true
        self.selectedUserProfileImageView.setNeedsDisplay()
        isPickerSelect = false
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        isPickerSelect = false
        picker.dismiss(animated: true, completion: nil)
    }

}

extension ProfileViewController : UINavigationControllerDelegate {
    
}

//MARK: - DatePickerDelegate
extension ProfileViewController: DatePickerViewDelegate {
    func datePickerView(_ datePickerView: DatePickerView, didTapButton button: UIButton!) {
        datePicker.hideDatePicker()
    }
    
    func datePickerView(_ datePickerView: DatePickerView, didChangeDate datePicker: UIDatePicker!) {
        selectedDate = datePicker.date
        self.birthdateTextField?.text = datePicker.date.stringValue(format: "YYYY/MM/dd")
    }
    
    func datePickerView(_ datePickerView: DatePickerView, didShow shown: Bool) {
        view.endEditing(true)
        birthdateTextFieldLine.backgroundColor = UIColor.defaultYellow
    }
    
    func datePickerView(_ datePickerView: DatePickerView, didHide hidden: Bool) {
        birthdateTextFieldLine.backgroundColor = UIColor.black
    }
    
}

//MARK: IQ DropDownTextField
extension ProfileViewController: IQDropDownTextFieldDelegate {
    override func didChangeValue(forKey key: String) {
        
    }
    
    
}
