//
//  RewardsListViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 06/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftPullToRefresh
import SwiftSpinner

class RewardsListViewController: UIViewController {

    @IBOutlet weak fileprivate var userBalanceLabel: UILabel!
    @IBOutlet weak fileprivate var collectionView: UICollectionView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var collectionConstraint: NSLayoutConstraint!
    var colCellSize : CGFloat = 0.0
    
    fileprivate var searchString = ""
    fileprivate var searchFilter = ""
    
    fileprivate var offset : CGFloat = 70
    
    override func viewDidLoad() {
        super.viewDidLoad()
        colCellSize = ( Device.Size.width - 30 ) / 2
        
        PushRewardHelper.sharedInstance.delegate = self
        
        if RealmManager.realm!.objects(Reward.self).count == 0 {
            getAllRewards()
        } else {
            collectionView.reloadData()
            resetConstraint()
        }
        
        self.scrollView.spr_setIndicatorHeader { [weak self] in
            self?.getAllRewards()
        }
        
        searchReward(searchKey: "")
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        updateUserBalance()
    }
    
    //MARK: - Private Functions
    func updateUserBalance() {
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            userBalanceLabel.text = "\(currentUser.lylBalance.format(f: ".6"))"
            userBalanceLabel.adjustsFontSizeToFitWidth = true
        }
    }
    
    func getAllRewards(){
        
        MerchantService.getRewards(completion: { (response, error) in
            //print(response as Any)
            self.scrollView.spr_endRefreshing()
            
            if let error = error {
                print(error)
            } else {

                let rewardsList = response?["rewards"]
                let rewardsArray = rewardsList?.array
                for merchant in rewardsArray! {
                    let newReward = Reward(info: merchant.dictionaryObject!)
                    print(newReward)
                    RealmManager.save(object: newReward)
                }
                
                self.collectionView.reloadData()
                self.resetConstraint()
            }
            
        })
    }
    
    func resetConstraint() {
        self.collectionConstraint.constant = ((CGFloat(RealmManager.realm!.objects(Reward.self).count / 2) + CGFloat(RealmManager.realm!.objects(Reward.self).count % 2)) * (self.colCellSize + offset)) + 30
    }
    
    func searchReward(searchKey : String) {
        
        searchString = searchKey
        if searchKey.count != 0 {
            searchFilter = "name CONTAINS[c] '\(self.searchString)'"
        } else {
            searchFilter = "code >= 0"
        }
        self.collectionView.reloadData()
        
    }
    
    //MARK: - User Interactions
    
}

extension RewardsListViewController : UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let rewardCell = collectionView.dequeueReusableCell(withReuseIdentifier: "rewardCell", for: indexPath) as? RewardCell
        let rewards = RealmManager.realm!.objects(Reward.self).filter(searchFilter)
        let currentReward = rewards[indexPath.row]
        
        rewardCell?.rewardNameLabel.text = currentReward.name
        rewardCell?.rewardPriceLabel.text = "LYL \(currentReward.lylAmount!)"
        rewardCell?.rewardPriceLabel.adjustsFontSizeToFitWidth = true
        rewardCell?.rewardMerchantImageView.image = nil
        rewardCell?.rewardImageView.image = nil
        
        ImageDownloaderManager.downloadImage(urlString: (currentReward.logoUrl)!) { (image) in
            rewardCell?.rewardMerchantImageView.image = image
        }
        
        ImageDownloaderManager.downloadImageNonCache(urlString: (currentReward.imageUrl)!) { (image) in
            rewardCell?.rewardImageView.image = image
        }
        
        return rewardCell!
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return RealmManager.realm!.objects(Reward.self).filter(searchFilter).count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
//        if collectionView.numberOfItems(inSection: 0) != 0 {
//
//            let rewards = RealmManager.realm!.objects(Reward.self)
//            let currentReward = rewards[indexPath.row]
//            if (currentReward.name?.count)! > 15 {
//                return CGSize(width: colCellSize, height: colCellSize + 90)
//            }
//            //let merchantCell = collectionView.cellForItem(at: indexPath) as! RewardCell
//        }
        return CGSize(width: colCellSize, height: colCellSize + offset)
    }
    
}

extension RewardsListViewController : PushRewardHelperDelegate {
    func pushRewardHelper(didUpdateBalance: Bool) {
        self.updateUserBalance()
        //print("Did listen")
    }
    
    func pushRewardHelper(shouldRefreshWallet: Bool) {
        //self.updateUserBalance()
        //print("Did listen")
    }
}
