//
//  RewardCell.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 06/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit


class RewardCell: UICollectionViewCell {
    
    @IBOutlet weak var rewardMerchantImageView: UIImageView!
    @IBOutlet weak var rewardImageView: UIImageView!
    @IBOutlet weak var rewardNameLabel: UILabel!
    @IBOutlet weak var rewardPriceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        rewardMerchantImageView.layer.cornerRadius = rewardMerchantImageView.frame.size.width / 2
        rewardMerchantImageView.clipsToBounds = true
        rewardMerchantImageView.layer.borderWidth = 1
        rewardMerchantImageView.layer.borderColor = UIColor.lightGray.cgColor
        // Initialization code
    }
    
}
