//
//  MainPageViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 03/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import iCarousel

class MainPageViewController: UIPageViewController {
    
    //MARK : UIPageViewDelagate
    
    weak var mainPageDelegate: MainPageViewControllerDelegate?
    
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        // The view controllers will be shown in this order
        return [self.newViewController(viewController: "MerchantList"),
                self.newViewController(viewController: "RewardsList"),
                self.newViewController(viewController: "RewardsList"),
                self.newViewController(viewController: "RewardsList")]
    }()
    
    // Put this after "Payments"
    // self.newViewController("Movies"),
    //    self.newViewController("Feeds"),
    //MARK : Properties
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        delegate = self
        
        if let initialViewController = orderedViewControllers.first {
            //scrollToViewController(index: initialViewController)
        }
        
        //mainPageDelegate?.pageViewController(self, didUpdatePageCount: orderedViewControllers.count)
    }
    
    //MARK : Private Methods
    func scrollToNextViewController() {
        //, let nextViewController = pageViewController(self, viewControllerAfterViewController: visibleViewController)
        if let visibleViewController = viewControllers?.first {
            //scrollToViewController(nextViewController)
        }
    }
    
    func scrollToViewController(index newIndex: Int) {
        if let firstViewController = viewControllers?.first,
            let currentIndex = orderedViewControllers.index(of: firstViewController) {
            let direction: UIPageViewControllerNavigationDirection = newIndex >= currentIndex ? .forward : .reverse
            let nextViewController = orderedViewControllers[newIndex]
            //scrollToViewController(nextViewController, direction: direction)
        }
    }
    
    private func newViewController(viewController: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewController(withIdentifier: "\(viewController)ViewController")
    }
    
    private func scrollToViewController(viewController: UIViewController,
                                        direction: UIPageViewControllerNavigationDirection = .forward) {
        setViewControllers([viewController],
                           direction: direction,
                           animated: true,
                           completion: { (finished) -> Void in
                            self.notifyTutorialDelegateOfNewIndex()
        })
    }
    
    private func notifyTutorialDelegateOfNewIndex() {
        if let firstViewController = viewControllers?.first, let index = orderedViewControllers.index(of: firstViewController) {
            //mainPageDelegate?.pageViewController(self, didUpdatePageIndex: index)
        }
    }
    
}

// MARK: UIPageViewControllerDataSource

extension MainPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = orderedViewControllers.index(of:viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of:viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
}

extension MainPageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(pageViewController: UIPageViewController,
                            didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController],
                            transitionCompleted completed: Bool) {
        notifyTutorialDelegateOfNewIndex()
    }
    
}

protocol MainPageViewControllerDelegate: class {
    
    func mainPageViewController(mainPageViewController: MainPageViewController,
                            didUpdatePageCount count: Int)
    
    func mainPageViewController(mainPageViewController: MainPageViewController,
                            didUpdatePageIndex index: Int)
    
}
