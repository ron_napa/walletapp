//
//  MerchantInfoViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 04/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import RealmSwift
import Alamofire
import AlamofireImage

class MerchantInfoViewController: MainViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var merchantTitleLabel: UILabel!
    @IBOutlet fileprivate weak var merchantBannerImageView: UIImageView!
    @IBOutlet fileprivate weak var merchantLogoImageView: UIImageView!
    @IBOutlet fileprivate weak var merchantNameLabel: UILabel!
    @IBOutlet fileprivate weak var merchantDescriptionLabel: UILabel!
    
    //Dynamic Constraints
    @IBOutlet fileprivate weak var rewardMatrixConstraint: NSLayoutConstraint!
    @IBOutlet fileprivate weak var branchesConstraint: NSLayoutConstraint!
    
    fileprivate var currentMerchant: Merchant?
    
    var merchantRewardsVC : MerchantRewardsViewController?
    var merchantBranchesVC : MerchantBranchesViewController?

    var refreshingRewards = false
    var refreshingBranches = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let merchants = RealmManager.realm?.objects(Merchant.self).filter("code = \(SelectionSession.sharedInstance.merchantCode)")
        let dummyMerchant = merchants![0]
        currentMerchant = dummyMerchant
        
        displayMerchantInfo()
        
        self.merchantBranchesVC?.refreshMerchantBranchList()
        self.merchantRewardsVC?.getAllMerchantRewards()
        
        self.scrollView.spr_setIndicatorHeader { [weak self] in
            self?.refreshingRewards = false
            self?.refreshingBranches = false
            self?.merchantBranchesVC?.refreshMerchantBranchList()
            self?.merchantRewardsVC?.getAllMerchantRewards()
        }
        
        if currentMerchant?.status == 2 {
            AlertviewManager.showAlertInfo("Coming Soon!", message: "Stay tuned for this merchant.", view: self)
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        merchantLogoImageView.layer.cornerRadius = merchantLogoImageView.frame.size.width / 2
        merchantLogoImageView.clipsToBounds = true
        merchantLogoImageView.layer.borderWidth = 1
        merchantLogoImageView.layer.borderColor = UIColor.lightGray.cgColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "merchantBranchSegue" {
            let merchantBranchesView = segue.destination as? MerchantBranchesViewController
            merchantBranchesView?.delegate = self
            merchantBranchesVC = merchantBranchesView
        }
        
        if segue.identifier == "merchantRewardSegue" {
            let merchantRewardsView = segue.destination as? MerchantRewardsViewController
            merchantRewardsView?.delegate = self
            merchantRewardsVC = merchantRewardsView
        }
    }
    
    //https://www.google.com/maps/dir/14.584736,121.059/14.583439,121.056456/@14.5839489,121.0555551,17z/data=!3m1!4b1!4m2!4m1!3e2
    
    //MARK: - Private Functions
    func checkIfDoneRefreshing() {
        if refreshingRewards && refreshingBranches {
            self.scrollView.spr_endRefreshing()
            self.scrollView.contentInset = UIEdgeInsets(top: 60.0, left: 0.0, bottom: 0.0, right: 0.0)
            self.scrollView.contentOffset = CGPoint(x: 0.0, y: 0.0)
            self.scrollView.scrollToTop()
        }
    }
    
    func displayMerchantInfo() {
        
        merchantTitleLabel.text = currentMerchant?.name
        merchantNameLabel.text = currentMerchant?.name
        merchantDescriptionLabel.text = currentMerchant?.merchantDescription
        
        merchantLogoImageView.image = nil
        
        ImageDownloaderManager.downloadImage(urlString: (currentMerchant?.logoUrl)!) { (image) in
            self.merchantLogoImageView.image = image
        }
        
        merchantBannerImageView.image = nil
        
        ImageDownloaderManager.downloadImage(urlString: (currentMerchant?.bannerUrl)!) { (image) in
            self.merchantBannerImageView.image = image
        }
        
    }
    
    func updateConstraintRewards(newHeight:CGFloat, needToFlag:Bool) {
        
        if let merchantCode = currentMerchant?.code {
            if RealmManager.realm!.objects(Reward.self).filter("merchantCode = \(merchantCode)").count == 0 {
                rewardMatrixConstraint.constant = 40
            } else {
                rewardMatrixConstraint.constant = newHeight
            }
        }
        
        if needToFlag {
            refreshingRewards = true
            checkIfDoneRefreshing()
        }
        
    }
    
    func updateConstraintBranchesTable(cellsHeight: CGFloat) {
        
        if let merchantCode = currentMerchant?.code {
            if RealmManager.realm!.objects(MerchantBranch.self).filter("merchantCode = \(merchantCode)").count == 0 {
                branchesConstraint.constant = 40
            } else {
                branchesConstraint.constant = cellsHeight
            }
        }
        
        refreshingBranches = true
        checkIfDoneRefreshing()
        
    }
    
    func openTrackerInBrowser(branchLat: Double, branchLong: Double, userLat: Double, userLong:Double){
        
        let currentLocation = SelectionSession.sharedInstance.currentLocation
        
        if currentLocation?.coordinate.longitude != nil && currentLocation?.coordinate.latitude != nil {

            if let urlDestination = URL.init(string: "https://www.google.co.in/maps/dir/?saddr=\(userLat),\(userLong)&daddr=\(branchLat),\(branchLong)&directionsmode=driving") {
                UIApplication.shared.openURL(urlDestination)
            }
        }
    }

}

extension MerchantInfoViewController : MerchantBranchesViewControllerDelegate {
    
    func merchantBranchesViewController(_ viewController: MerchantBranchesViewController, didTapBranch index: Int) {
        
        let currentLocation = SelectionSession.sharedInstance.currentLocation
        let branch = RealmManager.realm!.objects(MerchantBranch.self).filter("merchantCode = \(self.currentMerchant?.code ?? 0)")
        let currentBranch = branch[index]
        
        if let UrlNavigation = URL.init(string: "comgooglemaps://") {
            if UIApplication.shared.canOpenURL(UrlNavigation){
                if currentLocation?.coordinate.longitude != nil && currentLocation?.coordinate.latitude != nil {
                    
                    let lat   =   currentLocation?.coordinate.latitude
                    let longi =   currentLocation?.coordinate.longitude
                    let branchLat = currentBranch.latitude
                    let branchLong = currentBranch.longitude
                    
                    if let urlDestination = URL.init(string: "comgooglemaps://?saddr=\(lat ?? 0.0),\(longi ?? 0.0)&daddr=\(branchLat),\(branchLong)&directionsmode=driving") {
                        UIApplication.shared.openURL(urlDestination)
                    }
                }
            } else {
                NSLog("Can't use comgooglemaps://");
                self.openTrackerInBrowser(branchLat: (currentLocation?.coordinate.latitude)!,
                                          branchLong: (currentLocation?.coordinate.longitude)!,
                                          userLat: currentBranch.latitude,
                                          userLong: currentBranch.longitude)
            }
        } else {
            NSLog("Can't use comgooglemaps://");
            self.openTrackerInBrowser(branchLat: (currentLocation?.coordinate.latitude)!,
                                      branchLong: (currentLocation?.coordinate.longitude)!,
                                      userLat: currentBranch.latitude,
                                      userLong: currentBranch.longitude)
        }
        
    }
    
    func merchantBranchesViewController(_ viewController: MerchantBranchesViewController, didUpdateBranches branchesCount: Int) {
        //updateConstraintBranches(branchesCount: branchesCount)
    }
    
    func merchantBranchesViewController(_ viewController: MerchantBranchesViewController, didGetAllCells tableHeight: CGFloat) {
        updateConstraintBranchesTable(cellsHeight: tableHeight)
    }
}

extension MerchantInfoViewController : MerchantRewardsViewControllerDelegate {
    func merchantRewardsViewController(_ viewController: MerchantRewardsViewController, didUpdateRewards updatedHeight: CGFloat, needToFlag: Bool) {
        updateConstraintRewards(newHeight: updatedHeight, needToFlag: needToFlag)
    }
}
