//
//  MerchantRewardCell.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 04/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit

class MerchantRewardCell: UITableViewCell {

    @IBOutlet weak var rewardCount: UILabel!
    @IBOutlet weak var rewardDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }

}
