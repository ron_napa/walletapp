//
//  MerchantCell.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 03/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit

protocol MerchantCellDelegate {
    func merchantCell(_ cell: MerchantCell, didTapLocation index: Int)
}

class MerchantCell: UITableViewCell {

    @IBOutlet weak var merchantLogoImageView: UIImageView!
    @IBOutlet weak var merchantCategoryLabel: UILabel!
    @IBOutlet weak var merchantNameLabel: UILabel!
    @IBOutlet weak var merchantDistanceLabel: UILabel!
    @IBOutlet weak var merchantLocationLabel: UILabel!
    @IBOutlet weak var merchantRewardCountLabel: UILabel!
    @IBOutlet weak var merchantBranchCountLabel: UILabel!
    
    @IBOutlet weak var merchantLocationView: UIView!
    @IBOutlet weak var appLocationSettings: UIButton!
    
    var delegate: MerchantCellDelegate?
    
    var merchantDisplay : Merchant?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        merchantLogoImageView.layer.cornerRadius = merchantLogoImageView.frame.size.width / 2
        merchantLogoImageView.clipsToBounds = true
        merchantLogoImageView.layer.borderWidth = 1
        merchantLogoImageView.layer.borderColor = UIColor.lightGray.cgColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func didTapLocationSettings(_ sender: Any) {
        self.delegate?.merchantCell(self, didTapLocation: 0)
    }

}
