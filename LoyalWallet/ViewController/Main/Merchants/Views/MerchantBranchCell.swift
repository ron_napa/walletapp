//
//  MerchantBranchCell.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 04/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit

class MerchantBranchCell: UITableViewCell {

    @IBOutlet weak var branchLocationLabel: UILabel!
    @IBOutlet weak var branchAddressLabel: UILabel!
    @IBOutlet weak var branchContactNoLabel: UILabel!
    @IBOutlet weak var branchScheduleLabel: UILabel!
    @IBOutlet weak var branchDistanceLabel: UILabel!
    @IBOutlet weak var branchDirectionButton: UIButton!
    @IBOutlet weak var appSettingsButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
    //MARK: - User Interactions
    @IBAction func didTapDirection(_ sender: Any) {
        
    }

}
