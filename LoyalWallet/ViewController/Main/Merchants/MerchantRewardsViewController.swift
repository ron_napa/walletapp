//
//  MerchantRewardsViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 04/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit

protocol MerchantRewardsViewControllerDelegate {
    func merchantRewardsViewController(_ viewController: MerchantRewardsViewController, didUpdateRewards updatedHeight: CGFloat, needToFlag: Bool)
}

class MerchantRewardsViewController: UIViewController {

    @IBOutlet weak fileprivate var tableView: UITableView!
    fileprivate var currentMerchant: Merchant?
    
    var delegate: MerchantRewardsViewControllerDelegate?
    fileprivate var cellsHeight : CGFloat = 0.0
    fileprivate var newHeightApplied = true
    fileprivate var updateTableHeight = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let merchants = RealmManager.realm?.objects(Merchant.self).filter("code = \(SelectionSession.sharedInstance.merchantCode)")
        let dummyMerchant = merchants![0]
        currentMerchant = dummyMerchant
        
        self.tableView.reloadData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Device.isiPhone5OrLess {
            let rewards = RealmManager.realm!.objects(Reward.self).filter("merchantCode = \(SelectionSession.sharedInstance.merchantCode)")
            self.delegate?.merchantRewardsViewController(self, didUpdateRewards: CGFloat(rewards.count * 100), needToFlag: false)
        }
        
        getAllMerchantRewards()
    }
    
    //Private Functions
    func getAllMerchantRewards(){
        
        cellsHeight = 0.0
        newHeightApplied = false
        updateTableHeight = false
        if let merchantCode = currentMerchant?.code {
            MerchantService.getMerchantRewards(merchantCode: merchantCode, completion: { (response, error) in
                
                if let error = error {
                    print(error)
                } else {

                    let rewardsList = response?["rewards"]
                    let rewardsArray = rewardsList?.array
                    
                    for merchant in rewardsArray! {
                        let newReward = Reward(info: merchant.dictionaryObject!)
                        RealmManager.save(object: newReward)
                    }
//
//                    if Device.isiPhone5OrLess {
//                        let rewards = RealmManager.realm!.objects(Reward.self).filter("merchantCode = \(SelectionSession.sharedInstance.merchantCode)")
//                        self.delegate?.merchantRewardsViewController(self, didUpdateRewards: CGFloat(rewards.count * 100))
//                    }
                    
                    self.tableView.reloadData()
                }
            })
        }
    }
    
    func getAllTableCells() -> CGFloat {
        
        var sumHeight : CGFloat = 0.0
        
        let cells = self.tableView.visibleCells as! Array<UITableViewCell>
        for cell in cells {
            sumHeight += cell.frame.height
        }
        
        return sumHeight
        
    }
    
    //User Interactions

}

extension MerchantRewardsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let merchantCode = currentMerchant?.code {
            if RealmManager.realm!.objects(Reward.self).filter("merchantCode = \(merchantCode)").count == 0 {
                let emptyCell = tableView.dequeueReusableCell(withIdentifier: "emptyCell", for: indexPath)
                return emptyCell
            }
        }
        
        let rewardCell = tableView.dequeueReusableCell(withIdentifier: "rewardCell", for: indexPath) as! MerchantRewardCell
        if let merchantCode = currentMerchant?.code {
            let rewards = RealmManager.realm!.objects(Reward.self).filter("merchantCode = \(merchantCode)")
            let currentReward = rewards[indexPath.row]
            rewardCell.rewardCount.text = "\(currentReward.lylAmount ?? "")"
            rewardCell.rewardDescription.text = currentReward.name
            
            if indexPath.row == 0 {
                cellsHeight = 0.0
            }
            
            cellsHeight += rewardCell.frame.height
            self.delegate?.merchantRewardsViewController(self, didUpdateRewards: cellsHeight, needToFlag: false)
            
            if rewards.count - 1 == indexPath.row && !newHeightApplied {
                newHeightApplied = true
                self.delegate?.merchantRewardsViewController(self, didUpdateRewards: cellsHeight, needToFlag: true)
            }
            
        }
        
        return rewardCell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let merchantCode = currentMerchant?.code {
            if RealmManager.realm!.objects(Reward.self).filter("merchantCode = \(merchantCode)").count == 0 {
                self.delegate?.merchantRewardsViewController(self, didUpdateRewards: 0.0, needToFlag: true)
                return 1
            }
            return RealmManager.realm!.objects(Reward.self).filter("merchantCode = \(merchantCode)").count
        }
        return RealmManager.realm!.objects(Reward.self).filter("merchantCode = \(1)").count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
}
