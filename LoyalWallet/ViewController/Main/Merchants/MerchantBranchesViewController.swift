//
//  MerchantBranchesViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 04/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import MapKit

protocol MerchantBranchesViewControllerDelegate {
    func merchantBranchesViewController(_ viewController: MerchantBranchesViewController, didTapBranch index: Int)
    func merchantBranchesViewController(_ viewController: MerchantBranchesViewController, didUpdateBranches branchesCount: Int)
    func merchantBranchesViewController(_ viewController: MerchantBranchesViewController, didGetAllCells tableHeight: CGFloat)
}

class MerchantBranchesViewController: UIViewController {

    @IBOutlet weak fileprivate var tableView: UITableView!
    
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    fileprivate var currentMerchant: Merchant?
    fileprivate var cellsHeight : CGFloat = 0.0
    
    var delegate: MerchantBranchesViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.sizeToFit()
        let merchants = RealmManager.realm?.objects(Merchant.self).filter("code = \(SelectionSession.sharedInstance.merchantCode)")
        currentMerchant = merchants?.first
        
        initLocationManager()
        //refreshMerchantBranchList()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Private Functions
    
    func initLocationManager(){
        locManager.requestAlwaysAuthorization()
        locManager.requestWhenInUseAuthorization()
        locManager.delegate = self
        
        if CLLocationManager.locationServicesEnabled() {
            locManager.desiredAccuracy = kCLLocationAccuracyBest
            locManager.startUpdatingLocation()
            currentLocation = locManager.location
            SelectionSession.sharedInstance.currentLocation = currentLocation
        }
    }
    
    func refreshMerchantBranchList() {
        
        if (CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways){
            currentLocation = locManager.location
            getAllMerchantLocationsWithCoor(userLatString: "\(currentLocation.coordinate.latitude)", userLongString: "\(currentLocation.coordinate.longitude)")
        }
        
        if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse &&
            CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedAlways){
            getAllMerchantLocationsWithCoor(userLatString: "", userLongString: "")
        }
    }
    
    func getAllMerchantLocationsWithCoor(userLatString: String = "", userLongString: String = "") {
        
        self.cellsHeight = 0.0
        
        if let merchantCode = currentMerchant?.code {
            
            MerchantService.getMerchantLocations(userLat: userLatString, userLong: userLongString, merchantCode: merchantCode) { (response, error) in
                
                //print(response as Any)
                
                if error == nil {
                    let locationsList = response?["locations"]
                    if let locationArray = locationsList?.array {
                        print(RealmManager.realm!.objects(MerchantBranch.self).filter("merchantCode = \(merchantCode)").count)
                        for location in locationArray {
                            let newLocation = MerchantBranch(info: location.dictionaryObject!)
                            RealmManager.save(object: newLocation)
                        }
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func getAllCellHeights() -> CGFloat {
        
        var tableHeight: CGFloat = 0.0
        
        if let merchantCode = currentMerchant?.code {
            
            let newLocationsArray = RealmManager.realm!.objects(MerchantBranch.self).filter("merchantCode = \(merchantCode)")
            
            var cellIndex = 0
            while cellIndex <  newLocationsArray.count {
                
                let cellIndexPath = IndexPath(item: cellIndex, section: 1)
                let cell = tableView.cellForRow(at: cellIndexPath)
                if let cellHeight = cell?.frame.height {
                    tableHeight +=  cellHeight
                    print(cellHeight)
                }
                
                cellIndex += 1
            }
        }
        
        return tableHeight
        
    }
    
    //MARK: - User Interactions
    @IBAction func didTapDirections(_ sender: Any) {
        let button = sender as? UIButton
        delegate?.merchantBranchesViewController(self, didTapBranch: (button?.tag)!)
    }
    
    @IBAction func didTapAppSettings(_ sender: Any) {
        if let url = URL(string: UIApplicationOpenSettingsURLString) {
            // If general location settings are enabled then open location settings for the app
            UIApplication.shared.openURL(url)
        }
    }
    
}

extension MerchantBranchesViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let merchantCode = currentMerchant?.code {
            if RealmManager.realm!.objects(MerchantBranch.self).filter("merchantCode = \(merchantCode)").count == 0 {
                let emptyCell = tableView.dequeueReusableCell(withIdentifier: "emptyCell", for: indexPath)
                return emptyCell
            }
        }
        
        let branchCell = tableView.dequeueReusableCell(withIdentifier: "merchantBranchCell", for: indexPath) as! MerchantBranchCell
        if let merchantCode = currentMerchant?.code {
            let rewards = RealmManager.realm!.objects(MerchantBranch.self).filter("merchantCode = \(merchantCode)")
            let currentBranch = rewards[indexPath.row]
            
            branchCell.branchDirectionButton.tag = indexPath.row
            branchCell.branchLocationLabel.text = currentBranch.name
            branchCell.branchAddressLabel.text = currentBranch.address
            branchCell.branchDistanceLabel.text = "\(currentBranch.distance)km"
            branchCell.branchScheduleLabel.text = currentBranch.bussHours
            branchCell.branchContactNoLabel.text = currentBranch.contactNo
            branchCell.branchDirectionButton.isHidden = false
            branchCell.appSettingsButton.isHidden = true
            branchCell.branchDistanceLabel.isHidden = false
            
            if (CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse &&
                CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedAlways){
                branchCell.branchDirectionButton.isHidden = true
                branchCell.appSettingsButton.isHidden = false
                branchCell.branchDistanceLabel.isHidden = true
            }
            
            if indexPath.row == 0 {
                cellsHeight = 0.0
            }
            
            cellsHeight += branchCell.frame.height
            self.delegate?.merchantBranchesViewController(self, didGetAllCells: self.cellsHeight)
            
            if rewards.count - 1 == indexPath.row {
                self.delegate?.merchantBranchesViewController(self, didGetAllCells: self.cellsHeight)
            }
            
        }
        
        return branchCell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let merchantCode = currentMerchant?.code {
            if RealmManager.realm!.objects(MerchantBranch.self).filter("merchantCode = \(merchantCode)").count == 0 {
                self.delegate?.merchantBranchesViewController(self, didGetAllCells: 0.0)
                return 1
            }
            return RealmManager.realm!.objects(MerchantBranch.self).filter("merchantCode = \(merchantCode)").count
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

}

extension MerchantBranchesViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //print("did Changed")
        currentLocation = manager.location
        SelectionSession.sharedInstance.currentLocation = currentLocation
        self.refreshMerchantBranchList()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations[0]
        SelectionSession.sharedInstance.currentLocation = currentLocation
    }
    
}
