//
//  MerchantListViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 03/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SwiftyJSON
import RealmSwift
import MapKit
import Alamofire
import AlamofireImage
import CoreLocation

protocol MerchantListViewControllerDelegate {
    func merchantListViewController(_ viewController: MerchantListViewController, didTapMerchant index: Int)
}

class MerchantListViewController: UIViewController {
    
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    var delegate: MerchantListViewControllerDelegate?
    @IBOutlet weak fileprivate var tableView: UITableView!
    fileprivate var searchString = ""
    fileprivate var searchFilter = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initLocationManager()
        
        if RealmManager.realm!.objects(Merchant.self).count == 0 {
            checkPrivacyFetch()
        }
        
        self.tableView.spr_setIndicatorHeader { [weak self] in
            self?.checkPrivacyFetch()
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //searchMerchant(searchKey: "")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //checkPrivacyFetch()
    }
    
    //MARK: Private Functions
    func initLocationManager(){
        locManager.requestAlwaysAuthorization()
        locManager.requestWhenInUseAuthorization()
        locManager.delegate = self
        
        if CLLocationManager.locationServicesEnabled() {
            locManager.desiredAccuracy = kCLLocationAccuracyBest
            locManager.startUpdatingLocation()
            currentLocation = locManager.location
            SelectionSession.sharedInstance.currentLocation = currentLocation
        }
    }
    
    func checkPrivacyFetch() {
        if CLLocationManager.locationServicesEnabled() {
            getAllMerchants()
        }
    }
    
    func getAllMerchants(){
        
        if CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse ||
           CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways {
            //currentLocation = CLLocationManager
            
            if currentLocation != nil {
                getAllMerchantWithCoor(latString: "\(currentLocation.coordinate.latitude)", longString: "\(currentLocation.coordinate.longitude)")
            } else {
                getAllMerchantWithCoor(latString: "", longString: "")
            }
        }
        
        if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse &&
            CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedAlways {
            getAllMerchantWithCoor(latString: "", longString: "")
        }

    }
    
    func getAllMerchantWithCoor(latString : String, longString : String) {
        
        self.tableView.spr_endRefreshing()
        
        MerchantService.getMerchantList(userLat: latString, userLong: longString) { (response, error) in
            print(response as Any)
            if let error = error {
                print(error)
            } else {
                
                let merchantList = response?["merchants"]
                if let merchantArray = merchantList?.array {
                    for merchant in merchantArray {
                        let newMerchant = Merchant(info: merchant.dictionaryObject!)
                        RealmManager.save(object: newMerchant)
                    }
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func searchMerchant(searchKey : String, retainPosition : Bool) {
        
        searchString = searchKey
        
        //searchFilter = "name CONTAINS[c] '\(self.searchString)' OR category CONTAINS[c] '\(self.searchString)'"

        if searchKey.count != 0 {
            searchFilter = "(name CONTAINS[c] '\(self.searchString)' OR category CONTAINS[c] '\(self.searchString)') AND status != 0"
        } else {
            searchFilter = "code >= 0 AND status != 0"
        }
        
       // self.tableView.reloadData()
        
        let verticalContentOffset  = tableView.contentOffset.y;
        self.tableView.reloadData()
        self.tableView.layoutIfNeeded()
        if retainPosition {
            self.tableView.setContentOffset(CGPoint(x:0, y:verticalContentOffset), animated: false)
        }
        //self.tableView.contentOffset = offset
        
//        let lastScrollOffset = tableView.contentOffset
//        tableView.beginUpdates()
//        tableView.reloadData()
//        tableView.endUpdates()
//        tableView.layer.removeAllAnimations()
//        tableView.setContentOffset(lastScrollOffset, animated: false)
        
        if searchKey.count != 0 && RealmManager.realm!.objects(Merchant.self).filter(searchFilter).count != 0 {
            self.tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: UITableViewScrollPosition.top, animated: false)
        }
        
    }
    
    //MARK: User Interactions
    
}

extension MerchantListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let merchantCell = tableView.dequeueReusableCell(withIdentifier: "merchantInfoCell", for: indexPath) as! MerchantCell
        let merchants = RealmManager.realm?.objects(Merchant.self).filter(searchFilter)
        let currentMerchant = merchants![indexPath.row]
        merchantCell.delegate = self
        merchantCell.merchantNameLabel.text = currentMerchant.name
        merchantCell.merchantCategoryLabel.text = currentMerchant.category
        merchantCell.merchantDistanceLabel.text = "\(currentMerchant.distance)km"
        merchantCell.merchantLocationLabel.text = currentMerchant.nearestBranch
        merchantCell.merchantRewardCountLabel.text = "\(currentMerchant.totalRewards)"
        merchantCell.merchantBranchCountLabel.text = "\(currentMerchant.totalBranches)"
        merchantCell.merchantLogoImageView.image = nil
        merchantCell.selectionStyle = .none
        
        merchantCell.merchantLocationView.isHidden = false
        merchantCell.appLocationSettings.isHidden = true
        merchantCell.merchantDistanceLabel.isHidden = false
        merchantCell.merchantDistanceLabel.adjustsFontSizeToFitWidth = true
        
        if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedWhenInUse &&
           CLLocationManager.authorizationStatus() != CLAuthorizationStatus.authorizedAlways {
            merchantCell.merchantLocationLabel.text = "--"
            merchantCell.merchantDistanceLabel.isHidden = true
            merchantCell.appLocationSettings.isHidden = false
        }
        
        ImageDownloaderManager.downloadImage(urlString: (currentMerchant.logoUrl)!) { (image) in
            merchantCell.merchantLogoImageView.image = image
        }
        
        return merchantCell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RealmManager.realm!.objects(Merchant.self).filter(searchFilter).count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let merchants = RealmManager.realm?.objects(Merchant.self).filter(searchFilter)
        let currentMerchant = merchants![indexPath.row]
        SelectionSession.sharedInstance.selectedMerchant = currentMerchant
        SelectionSession.sharedInstance.merchantCode = currentMerchant.code
        delegate?.merchantListViewController(self, didTapMerchant: indexPath.row)
    }
    
}

extension MerchantListViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //print("did Changed")
        currentLocation = manager.location
        SelectionSession.sharedInstance.currentLocation = currentLocation
        self.getAllMerchants()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations[0]
        SelectionSession.sharedInstance.currentLocation = currentLocation
    }
    
}

extension MerchantListViewController : MerchantCellDelegate {
    func merchantCell(_ cell: MerchantCell, didTapLocation index: Int) {
        if let url = URL(string: UIApplicationOpenSettingsURLString) {
            // If general location settings are enabled then open location settings for the app
            UIApplication.shared.openURL(url)
        }
    }
}
