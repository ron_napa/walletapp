//
//  MainHomeViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 03/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import iCarousel
import Crashlytics
import SWRevealViewController
import SwiftySound

class MainHomeViewController: MainViewController {
    
    @IBOutlet weak fileprivate var tab1Button: UIButton!
    @IBOutlet weak fileprivate var tab2Button: UIButton!
    @IBOutlet weak fileprivate var tab3Button: UIButton!
    @IBOutlet weak fileprivate var tab4Button: UIButton!
    @IBOutlet weak fileprivate var tab5Button: UIButton!
    
    @IBOutlet weak fileprivate var tab1Label: UILabel!
    @IBOutlet weak fileprivate var tab2Label: UILabel!
    @IBOutlet weak fileprivate var tab3Label: UILabel!
    @IBOutlet weak fileprivate var tab4Label: UILabel!
    @IBOutlet weak fileprivate var tab5Label: UILabel!
    
    @IBOutlet weak fileprivate var tab1ImageView: UIImageView!
    @IBOutlet weak fileprivate var tab2ImageView: UIImageView!
    @IBOutlet weak fileprivate var tab3ImageView: UIImageView!
    @IBOutlet weak fileprivate var tab4ImageView: UIImageView!
    @IBOutlet weak fileprivate var tab5ImageView: UIImageView!
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet weak var blockerView: UIView!
    
    @IBOutlet weak fileprivate var mainTitleLabel: UILabel!
    //@IBOutlet weak fileprivate var carouselView: iCarousel!
    
    var titleScreen: [String] = ["Merchants","Rewards","Pay","Wallet","Profile"]
    var buttonDefaultImage: [String] = ["merchant","rewards","pay","wallet","profile"]
    var buttonArray:[UIButton] = []
    var labelArray:[UILabel] = []
    var imageArray:[UIImageView] = []
    
    var screenPageView: ScreensPageViewController?
    
    var tapGesture = UITapGestureRecognizer()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //tab1Label.adjustsFontSizeToFitWidth = true
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(MainHomeViewController.myviewTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        blockerView.addGestureRecognizer(tapGesture)
        blockerView.isUserInteractionEnabled = true
        
        RegisterSession.sharedInstance.isHomeVerification = false
        
        buttonArray = [self.tab1Button, self.tab2Button, self.tab3Button, self.tab4Button, self.tab5Button]
        labelArray = [self.tab1Label, self.tab2Label, self.tab3Label, self.tab4Label, self.tab5Label]
        imageArray = [self.tab1ImageView, self.tab2ImageView, self.tab3ImageView, self.tab4ImageView, self.tab5ImageView]
        //setCurrentTab(index: 0)
        
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
 
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            if !currentUser.emailVerified {
                showEmailVerification()
            }
        }
        
        if UserSession.sharedInstance.userShouldRefreshBalance {
            LoginHelper.getUserProfile()
        }
        
        screenPageView?.searchItem(searchKey: "", retainPosition: true)
        
        for label in labelArray {
            if Device.isiPhone5 {
                label.font = UIFont.init(name: "OpenSans-Light", size: 8)
            } else {
                label.font = UIFont.init(name: "OpenSans-Light", size: 10)
            }
        }
        
        if Device.isiPhone5 {
            labelArray[0].font = UIFont.init(name: "OpenSans-SemiBold", size: 8)
        } else {
            labelArray[0].font = UIFont.init(name: "OpenSans-SemiBold", size: 10)
        }
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideSearchBar()
        RegisterSession.sharedInstance.isHomeVerification = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "merchantListSegue" {
            let merchantList = segue.destination as? MerchantListViewController
            merchantList?.delegate = self
        } else if segue.identifier == "pageViewControllerSegue" {
            screenPageView = segue.destination as? ScreensPageViewController
            screenPageView?.screenPageViewDelegate = self
        }
    }
    
    //MARK: Private Functions
    func setCurrentTab(index:Int) {
        flipPage(index: index)
        if let hasChanged = screenPageView?.profileViewController?.checkIfChangedInfo(), hasChanged{
            return
        }
        updateTab(index: index)
        //Crashlytics.sharedInstance().crash()
    }
    
    func updateTab(index:Int) {
        
        var ctr = 0
        while ctr < buttonDefaultImage.count {
            imageArray[ctr].image = UIImage(named:buttonDefaultImage[ctr])
            ctr+=1
        }
    
        for label in labelArray {
            if Device.isiPhone5 {
                label.font = UIFont.init(name: "OpenSans-Light", size: 8)
            } else {
                label.font = UIFont.init(name: "OpenSans-Light", size: 10)
            }
        }
    
        imageArray[index].image = UIImage(named:buttonDefaultImage[index] + "_active")
        
        if Device.isiPhone5 {
            labelArray[index].font = UIFont.init(name: "OpenSans-SemiBold", size: 8)
        } else {
            labelArray[index].font = UIFont.init(name: "OpenSans-SemiBold", size: 10)
        }
        
        if index == 2 { return }
        mainTitleLabel.text = titleScreen[index]

        searchButton.isHidden = false
        if index >= 2 {
            searchButton.isHidden = true
        }
        
    }
    
    func flipPage(index:Int) {
 
        //searchButton.isHidden = false
        if index == 2 {
            //show QR
            
            let supportView =  Bundle.main.loadNibNamed("UserQRView", owner: nil, options: nil)?[0] as! UserQRView
            let viewController = UIViewController()
            supportView.delegate = self
            viewController.view = supportView
            viewController.modalPresentationStyle = .overCurrentContext
            
            if let hasChanged = screenPageView?.profileViewController?.checkIfChangedInfo(), hasChanged{
                if let topViewController = Utils.topViewController() {
                    let refreshAlert = UIAlertController(title: "LoyalWallet", message: "Your changes will be discarded.", preferredStyle: UIAlertControllerStyle.alert)
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                        //self.searchButton.isHidden = true
                        self.screenPageView?.profileViewController?.updateDisplay()
                        self.screenPageView?.profileViewController?.selectedUserProfileImageView.image = nil
                        self.present(viewController, animated: false, completion: nil)
                        self.updateTab(index: 2)
                    }))
                    refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                        
                    }))
                    topViewController.present(refreshAlert, animated: true, completion: nil)
                    UserSession.sharedInstance.allAlertController.append(refreshAlert)
                }
            } else {
                //searchButton.isHidden = true
                present(viewController, animated: false, completion: nil)
            }
        } else if index >= 3 {
            //searchButton.isHidden = true
            screenPageView?.scrollToViewController(index: index-1)
            //carouselView.scrollToItem(at: index-1, animated: true)
        } else {
            screenPageView?.scrollToViewController(index: index)
            //carouselView.scrollToItem(at: index, animated: true)
        }
    }
    
    func hideSearchBar() {
        searchView.isHidden = true
        searchTextField.text = ""
        view.endEditing(true)
    }
    
    func showSearchBar() {
        searchView.isHidden = false
        searchTextField.text = ""
        searchTextField.becomeFirstResponder()
    }
    
    func showEmailVerification() {
        if let topViewController = Utils.topViewController() {
            let refreshAlert = UIAlertController(title: "LoyalWallet", message: "To fully utilize the features of LoyalWallet, you need to verify your email.", preferredStyle: UIAlertControllerStyle.alert)
            refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                RegisterSession.sharedInstance.isHomeVerification = true
                let vc = R.Storyboards.accountVerification.instantiateViewController(withIdentifier: "SendVerificationViewController") as! SendVerificationViewController
                vc.verificationType = .email
                self.navigationController?.pushViewController(vc, animated: true)
            }))
            refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                //print("Handle Cancel Logic here")
            }))
            topViewController.present(refreshAlert, animated: true, completion: nil)
            UserSession.sharedInstance.allAlertController.append(refreshAlert)
        }
    }
    
    func slideSideMenu(_ sender: Any) {
        let slideMenu = self.parent!.revealViewController()
        if (slideMenu != nil) {
            slideMenu?.delegate = self
            slideMenu?.revealToggle(sender)
        }
    }
    
    //MARK: - Search
    @objc func textFieldDidChange(textField: UITextField){
        screenPageView?.searchItem(searchKey: textField.text!, retainPosition: true)
    }
    
    //MARK: - User Interactions
    @IBAction func didTapTab(_ sender: Any) {
        screenPageView?.searchItem(searchKey: "", retainPosition: true)
        hideSearchBar()
        let buttonTab = sender as! UIButton
        setCurrentTab(index: buttonTab.tag - 1)
    }
    
    @IBAction func didTapMenu(_ sender: Any) {
        slideSideMenu(sender)
    }
    
    @IBAction func didTapShowSearch(_ sender: Any) {
        showSearchBar()
    }
    
    @IBAction func didTapClose(_ sender: Any) {
        hideSearchBar()
        screenPageView?.searchItem(searchKey: "", retainPosition: false)
    }
    
    @objc func myviewTapped(_ sender: UITapGestureRecognizer) {
        slideSideMenu(sender)
    }
    
}

extension MainHomeViewController : MerchantListViewControllerDelegate {
    func merchantListViewController(_ viewController: MerchantListViewController, didTapMerchant index: Int) {
        performSegue(withIdentifier: "merchantInfoSegue", sender: self)
    }
}

//MARK: - ScreenPage View
extension MainHomeViewController : ScreensPageViewControllerDelegate {
    func screensPageViewController(_ viewController: ScreensPageViewController, didUpdateTab index: Int) {
        if index >= 2 {
            self.updateTab(index: index+1)
        } else {
            self.updateTab(index: index)
        }
    }
    
    func screensPageViewController(_ viewController: ScreensPageViewController, showMerchantInfo index: Int) {
        performSegue(withIdentifier: "merchantInfoSegue", sender: self)
    }
    
    func screensPageViewController(_ viewController: ScreensPageViewController, walletAction index: Int) {
        
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            if !currentUser.emailVerified {
                showEmailVerification()
                return
            }
        }
        
        if index == 0 {
            performSegue(withIdentifier: "sendWalletSegue", sender: self)
        } else {
            performSegue(withIdentifier: "receiveWalletSegue", sender: self)
        }
    }
    
    func screensPageViewController(_ viewController: ScreensPageViewController, didSelectTransaction index: Int) {
        self.performSegue(withIdentifier: "showTransactionSegue", sender: self)
    }
}

extension MainHomeViewController : UserQRViewDelegate {
    func userQRView(_ viewController: UserQRView, didCloseQR: Bool) {
        if didCloseQR {
            if (screenPageView?.currentPageIndex)! >= 2 {
                self.updateTab(index: (screenPageView?.currentPageIndex)! + 1)
            } else {
                self.updateTab(index: (screenPageView?.currentPageIndex)!)
            }
            
        }
    }
}

extension MainHomeViewController : SWRevealViewControllerDelegate {
    func revealControllerPanGestureEnded(_ revealController: SWRevealViewController!) {
        //blockerView.isHidden = !blockerView.isHidden
    }
    
    func revealController(_ revealController: SWRevealViewController!, didMoveTo position: FrontViewPosition) {
        
    }
    
    func revealController(_ revealController: SWRevealViewController!, animateTo position: FrontViewPosition) {
        blockerView.isHidden = !blockerView.isHidden
    }
}
