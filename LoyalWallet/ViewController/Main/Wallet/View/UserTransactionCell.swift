
//
//  UserTransactionCell.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 25/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit

class UserTransactionCell: UITableViewCell {

    @IBOutlet weak var transactionDescription: UILabel!
    @IBOutlet weak var transactionStatus: UILabel!
    @IBOutlet weak var transactionDate: UILabel!
    @IBOutlet weak var transactionAmount: UILabel!
    @IBOutlet weak var transactionFlagView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        transactionFlagView.layer.cornerRadius = transactionFlagView.frame.size.width / 2
        transactionFlagView.clipsToBounds = true
        //transactionFlagView.layer.borderWidth = 1
        //transactionFlagView.layer.borderColor = UIColor.lightGray.cgColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
