//
//  SendWalletConfirmationViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 19/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftSpinner
import ActiveLabel

class SendWalletConfirmationViewController: MainViewController {
    
    @IBOutlet weak fileprivate var userBalanceLabel: UILabel!
    
    @IBOutlet weak fileprivate var userAmountTextField: SkyFloatingLabelTextField!
    @IBOutlet weak fileprivate var feeLabel: UILabel!
    @IBOutlet weak fileprivate var amountLabel: UILabel!
    
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var checkBoxButton: UIButton!
    
    @IBOutlet weak var termsActiveLabel: ActiveLabel!
    
    var selectedType = SendType.wallet
    var inputtedValue = ""
    fileprivate var totalAmount = 0.0
    fileprivate var totalFee = 0.0
    fileprivate var receiverName = ""
    fileprivate var updatedBalance = ""
    
    var isAcceptedTerms = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userAmountTextField.delegate = self
        setupActiveLabel()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        userAmountTextField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            userBalanceLabel.text = "LYL \(currentUser.lylBalance.format(f: ".6"))"
        }
        
        if !isAcceptedTerms {
            sendButton.backgroundColor = UIColor.gray
            sendButton.isEnabled = false
        }
    }
    
    // MARK: Private Functions
    func setupActiveLabel() {
        
//        let termsString = ActiveType.custom(pattern: "\\sTerms & Conditions\\b")
//        
//        termsActiveLabel.enabledTypes = [termsString]
//        
//        termsActiveLabel.handleCustomTap(for: termsString) { element in
//            
//        }
//        
//        termsActiveLabel.customColor[termsString] = UIColor.linkOutButton
//        
        let customType = ActiveType.custom(pattern: "\\sTerms & Conditions\\b")
        termsActiveLabel.enabledTypes.append(customType)
        
        termsActiveLabel.customize { label in
            termsActiveLabel.text = "I understand that LoyalCoin transfers are non-refundable and have read the Terms & Conditions"
            termsActiveLabel.numberOfLines = 0
            //label.lineSpacing = 4
            
            termsActiveLabel.textColor = UIColor.black
            //Custom types
            termsActiveLabel.customColor[customType] = UIColor.linkOutButton
            termsActiveLabel.customSelectedColor[customType] = UIColor.linkOutButton
            
            termsActiveLabel.configureLinkAttribute = { (type, attributes, isSelected) in
                var atts = attributes
                switch type {
                default: ()
                }
                
                return atts
            }
            
            termsActiveLabel.handleCustomTap(for: customType) { element in
                let vc = R.Storyboards.main.instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as! TermsAndConditionsViewController
                vc.isModal = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        
        totalAmount = totalFee + (userAmountTextField.text?.doubleValue)!
        feeLabel.text = "LYL 0"
        if userAmountTextField.text!.count == 0 {
            amountLabel.text = "LYL 0"
        } else {
            amountLabel.text = "LYL \(userAmountTextField.text!)"
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "successWalletSegue" {
            let successViewController = segue.destination as? SendWalletSuccessViewController
            successViewController?.receiverName = self.receiverName
            successViewController?.updatedBalance = self.updatedBalance
            successViewController?.sentAmount = self.userAmountTextField.text!
        }
    }
    
    func specificLetterCount(str:String, char:Character) ->Int {
        var letters = Array(str); var count = 0
        for letter in letters {
            if letter == char {
                count += 1
            }
        }
        return count
    }
    
    // MARK: User Interactions
    @IBAction func didTapCheckBox(_ sender: Any) {
        
        isAcceptedTerms = !isAcceptedTerms
        
        if isAcceptedTerms {
            checkBoxButton.setImage(UIImage(named: "checkbox-selected"), for: .normal)
            sendButton.backgroundColor = UIColor.defaultYellow
            sendButton.isEnabled = true
        } else {
            checkBoxButton.setImage(UIImage(named: "checkbox-default"), for: .normal)
            sendButton.backgroundColor = UIColor.gray
            sendButton.isEnabled = false
        }
        
    }
    
    @IBAction func didTapSend(_ sender: Any) {
        
        if userAmountTextField.text?.count == 0 {
            AlertviewManager.showAlertInfo(message: "Please enter amount to send.", view: self)
            return
        }
        
        if specificLetterCount(str: userAmountTextField.text!, char: ".") >= 2 {
            AlertviewManager.showAlertInfo(message: "Invalid amount.", view: self)
            return
        }
        
        if userAmountTextField.text?.last == "." {
            AlertviewManager.showAlertInfo(message: "Invalid amount.", view: self)
            return
        }
        
        if let valueArray = userAmountTextField.text?.split(separator: ".") {
            if valueArray.count < 1 || valueArray.count > 3 {
                AlertviewManager.showAlertInfo(message: "Invalid amount.", view: self)
                return
            }
        }
        
        if userAmountTextField.text!.doubleValue == 0.0 {
            AlertviewManager.showAlertInfo(message: "Amount should not be equal to 0.", view: self)
            return
        }
        
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            if currentUser.lylBalance < userAmountTextField.text!.doubleValue {
                AlertviewManager.showAlertInfo(message: "Entered amount is greater than current balance.", view: self)
                return
            }
        }
        
        SwiftSpinner.show("")
        UserService.sendFunds(type: selectedType.rawValue, value: inputtedValue, amount: "\(totalAmount)", completion: {(response , error) in
            
            SwiftSpinner.hide()
            if error != nil {
                //AlertviewManager.showAlertInfo(message: (error?.description)!, view: self)
            } else {
                if response!["code"].int != 200 {
                    AlertviewManager.showAlertInfo(message: response!["message"].string!, view: self)
                } else {
                    let transactionDetail = response!["transactionDetail"]
                    if let currentUser = RealmManager.realm?.objects(User.self).first {
                        try! RealmManager.realm?.write {
                            currentUser.lylBalance = transactionDetail["remainingBalance"].doubleValue
                        }
                    }
                    
                    self.updatedBalance = "\(transactionDetail["remainingBalance"].doubleValue.format(f: ".6"))"
                    self.receiverName = transactionDetail["receiverName"].stringValue
                    self.performSegue(withIdentifier: "successWalletSegue", sender: self)
                    
                }
            }
        })
    }
}

extension SendWalletConfirmationViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if specificLetterCount(str: userAmountTextField.text!, char: ".") >= 1 {
            if string == "." {
                return false
            }
        }
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= 11
    }
}
