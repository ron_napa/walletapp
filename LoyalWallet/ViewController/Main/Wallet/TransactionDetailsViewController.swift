//
//  TransactionDetailsViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 25/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class TransactionDetailsViewController: MainViewController {

    @IBOutlet weak var transactionImage: UIImageView!
    @IBOutlet weak var transactionFirstLabel: UILabel!
    @IBOutlet weak var transactionLastLabel: UILabel!
    @IBOutlet weak var rewardNameLabel: UILabel!
    @IBOutlet weak var rewardLabelConstraints: NSLayoutConstraint!
    @IBOutlet weak var transactionTypeLabel: UILabel!
    @IBOutlet weak var transactionStatusLabel: UILabel!
    @IBOutlet weak var referenceIdLabel: UILabel!
    @IBOutlet weak var transactionAmountLabel: UILabel!
    @IBOutlet weak var transactionDateLabel: UILabel!
    @IBOutlet weak var transactionTimeLabel: UILabel!
    
    let currentTransaction = UserSession.sharedInstance.selectedTransaction
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        transactionImage.layer.cornerRadius = transactionImage.frame.size.width / 2
        transactionImage.clipsToBounds = true
        transactionImage.layer.borderWidth = 1
        transactionImage.layer.borderColor = UIColor.lightGray.cgColor
        
        transactionDateLabel.text = currentTransaction?.transactionDate?.dateValue()?.stringValue(format: "MM/dd/yy")
        transactionTimeLabel.text = currentTransaction?.transactionDate?.dateValue()?.stringValue(format: "h:mm a")
        
        var imageURL = ""
        
        let currentAmount = currentTransaction?.lylAmount.cleanValue ?? ""
        
        if currentTransaction?.transactionType == TransactionType.redeem.rawValue {
            rewardNameLabel.text = currentTransaction?.rewardName!
            transactionFirstLabel.text = currentTransaction?.merchantName!
            transactionLastLabel.text = currentTransaction?.branchName!
            transactionTypeLabel.text = "Redeemed"
            transactionAmountLabel.text = "- LYL \(currentAmount)"
            transactionAmountLabel.textColor = UIColor.red
            imageURL = (currentTransaction?.merchantLogo)!
        } else if currentTransaction?.transactionType == TransactionType.purchase.rawValue {
            rewardLabelConstraints.constant = 0
            transactionFirstLabel.text = currentTransaction?.merchantName!
            transactionLastLabel.text = currentTransaction?.branchName!
            transactionTypeLabel.text = "Earned"
            imageURL = (currentTransaction?.merchantLogo)!
            transactionAmountLabel.text = "+ LYL \(currentAmount)"
            transactionAmountLabel.textColor = UIColor.successColor
        } else {
            rewardLabelConstraints.constant = 0

            if let currentUser = RealmManager.realm?.objects(User.self).first {
                if currentTransaction?.customerId == currentUser.id {
                    transactionFirstLabel.text = currentTransaction?.receiverFirstName
                    transactionLastLabel.text = currentTransaction?.receiverLastName
                    transactionTypeLabel.text = "Sent"
                    transactionAmountLabel.text = "- LYL \(currentAmount)"
                    transactionAmountLabel.textColor = UIColor.red
                } else {
                    transactionFirstLabel.text = currentTransaction?.senderFirstName
                    transactionLastLabel.text = currentTransaction?.senderLastName
                    transactionTypeLabel.text = "Received"
                    transactionAmountLabel.text = "+ LYL \(currentAmount)"
                    transactionAmountLabel.textColor = UIColor.successColor
                }
            }
            
            imageURL = (currentTransaction?.userImage)!
            
        }
        
        ImageDownloaderManager.downloadImage(urlString: imageURL) { (image) in
            self.transactionImage.image = image
        }
        
        transactionStatusLabel.text = "Successful"
        referenceIdLabel.text = "Reference ID: \(currentTransaction?.id ?? 0)"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
