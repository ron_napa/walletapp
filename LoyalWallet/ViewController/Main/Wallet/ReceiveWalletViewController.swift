//
//  ReceiveWalletViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 19/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import QRCode

class ReceiveWalletViewController: MainViewController {

    @IBOutlet weak var userQRImageView: UIImageView!
    @IBOutlet weak var lylWalletAddressLabel: UILabel!
    
    /*
     {
     "lylAddress" : "<some_address>",
     "customerId": "<some_id>"
     }
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            let fullAddres = "{ \"lylAddress\" : \"\(currentUser.lylAddress ?? "")\", \"customerId\": \"\(currentUser.id)\" }"
            let qrCode = QRCode(fullAddres)
            self.userQRImageView.image = qrCode?.image
            self.lylWalletAddressLabel.text = currentUser.lylAddress
        }
    }
    
    //MARK: Private Functions
    
    //MARK: User Interactions
    @IBAction func didTapCopyAddress(_ sender: Any) {
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            UIPasteboard.general.string = currentUser.lylAddress
        }
        AlertviewManager.showAlertInfo(message: "Copied LYL Address.", view: self)
    }

}
