//
//  SendWalletSelectionViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 19/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftSpinner

enum SendType: String {
    case wallet = "wallet"
    case phone = "phone"
}

class SendWalletSelectionViewController: MainViewController {

    @IBOutlet weak var lylAddressTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var lylAddressFlagImageView: UIImageView!
    @IBOutlet weak var mobileNumberTextField: UITextField!
    @IBOutlet weak var mobileNumberLineView: UIView!
    @IBOutlet weak var mobileNumberFlagImageView: UIImageView!
    
    @IBOutlet weak var nextButton: UIButton!
    
    var selectedType = SendType.wallet
    var inputedValue : String = ""
    var defaultColor = UIColor.defaultYellow
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mobileNumberTextField.delegate = self
        mobileNumberTextField.tag = 1
        mobileNumberTextField.addTarget(self, action: #selector(textFieldDidTap(textField:)), for: .allTouchEvents)
        mobileNumberTextField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        lylAddressTextField.tag = 0
        lylAddressTextField.addTarget(self, action: #selector(textFieldDidTap(textField:)), for: .allTouchEvents)
        lylAddressTextField.addTarget(self, action: #selector(textFieldDidChange(textField:)), for: .editingChanged)
        defaultColor = nextButton.backgroundColor!
        
        nextButton.isEnabled = false
        nextButton.backgroundColor = UIColor.gray
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lylAddressFlagImageView.isHidden = false
        mobileNumberFlagImageView.isHidden = true
        
        lylAddressTextField.text = ""
        mobileNumberTextField.text = ""
        inputedValue = ""
        
        nextButton.isEnabled = false
        nextButton.backgroundColor = UIColor.gray
        
    }
    
    // MARK: Private Functions
    
    @objc func textFieldDidTap(textField: UITextField){
        //print("print")
        lylAddressFlagImageView.isHidden = true
        mobileNumberFlagImageView.isHidden = true
        
        if textField.tag == 0 {
            selectedType = SendType.wallet
            lylAddressFlagImageView.isHidden = false
            mobileNumberTextField.text = ""
        } else {
            selectedType = SendType.phone
            mobileNumberFlagImageView.isHidden = false
            lylAddressTextField.text = ""
        }
        
        inputedValue = textField.text!
        
        nextButton.isEnabled = false
        nextButton.backgroundColor = UIColor.gray
        
        if inputedValue.count > 0 {
            nextButton.isEnabled = true
            nextButton.backgroundColor = defaultColor
        }
        
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        inputedValue = textField.text!
        
        if inputedValue.count > 0 {
            nextButton.isEnabled = true
            nextButton.backgroundColor = defaultColor
        } else {
            nextButton.isEnabled = false
            nextButton.backgroundColor = UIColor.gray
        }
    
    }
    
    func getUserProfile() {
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            UserService.getUserProfile(customerId: "\(currentUser.id)", completion: { (response, error) in
                print(response as Any)
                SwiftSpinner.hide()
                if error != nil {
                    //AlertviewManager.showAlertInfo(message: (error?.description)!, view: self)
                } else {
                    
                    let userResponse = response!["customer"]
                    
                    try! RealmManager.realm?.write {
                        currentUser.mobileNo = userResponse["mobile_no"].string ?? ""
                        currentUser.email = userResponse["email"].string ?? ""
                        currentUser.profileImage = userResponse["profile_image"].string ?? ""
                        currentUser.activationCode = userResponse["activation_code"].string ?? ""
                        currentUser.pinCode = userResponse["pin_code"].string ?? ""
                        currentUser.lylAddress = userResponse["lyl_address"].string ?? ""
                        currentUser.lylBalance = userResponse["lyl_balance"].double ?? 0.0
                        currentUser.firstName = userResponse["first_name"].string ?? ""
                        currentUser.middleName = userResponse["middle_name"].string ?? ""
                        currentUser.lastName = userResponse["last_name"].string ?? ""
                        currentUser.gender = userResponse["gender"].string ?? ""
                        currentUser.dateOfBirth = userResponse["dateofbirth"].string ?? ""
                        currentUser.birthPlace = userResponse["birthplace"].string ?? ""
                        currentUser.nationality = userResponse["nationality"].string ?? ""
                        currentUser.registeredLocation = userResponse["reg_location"].string ?? ""
                        currentUser.lastTransactionDate = userResponse["last_transaction_date"].string ?? ""
                        currentUser.phoneVerified = userResponse["phone_verified"].int == 1 ? true : false
                        currentUser.emailVerified = userResponse["email_verified"].int == 1 ? true : false
                        currentUser.addressVerified = userResponse["address_verified"].int == 1 ? true : false
                        currentUser.selfieVerified = userResponse["selfie_verified"].int == 1 ? true : false
                        currentUser.indentityVerified = userResponse["indentity_verified"].int == 1 ? true : false
                    }
                    
                    AlertviewManager.showAlertInfo(message: "You have successfully updated your profile", view: self)
                }
            })
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sendConfirmationSegue" {
            let confirmViewController = segue.destination as! SendWalletConfirmationViewController
            confirmViewController.selectedType = selectedType
            
            if selectedType == .phone {
                confirmViewController.inputtedValue = "+63" + inputedValue
            } else {
                confirmViewController.inputtedValue = inputedValue
            }
        }
    }
    
    // MARK: User Interactions
    @IBAction func didTapNext(_ sender: Any) {
        //sendConfirmationSegue
        
        var inputtedMobileNumber = ""
        
        if selectedType == SendType.phone {
            
            inputtedMobileNumber = "+63" + inputedValue
            
            if inputtedMobileNumber.count == 3 {
                AlertviewManager.showAlertInfo(message: "Enter recipient's mobile number.", view: self)
                return
            }
            
            if !inputtedMobileNumber.matchExists(for: R.RegexPattern.patternMobileNumber) {
                AlertviewManager.showAlertInfo(message: "Please enter valid mobile number.", view: self)
                return
            }
            
        } else {
            
            inputtedMobileNumber = inputedValue
            
            if inputedValue.count == 0 {
                AlertviewManager.showAlertInfo(message: "Enter recipient's wallet address.", view: self)
                return
            }
        }
        
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            if currentUser.mobileNo == inputtedMobileNumber || currentUser.lylAddress == inputtedMobileNumber {
                AlertviewManager.showAlertInfo(message: "You can't transfer LYL to yourself.", view: self)
                return
            }
        }
        
        SwiftSpinner.show("")
        UserService.customerValidity(type: selectedType.rawValue, value: inputtedMobileNumber, completion: { (response, error) in
            SwiftSpinner.hide()
            print(self.inputedValue)
            //print(response)
            if error != nil {
                //AlertviewManager.showAlertInfo(message: (error?.description)!, view: self)
            } else {
                if response!["code"].int != 200 {
                    AlertviewManager.showAlertInfo(message: response!["message"].string!, view: self)
                } else {
                    self.performSegue(withIdentifier: "sendConfirmationSegue", sender: self)
                }
            }
        })
    }
    
    @IBAction func didStartEdit(_ sender: Any) {
        mobileNumberLineView.backgroundColor = UIColor.defaultYellow
    }
    
    @IBAction func didStopEdit(_ sender: Any) {
        mobileNumberLineView.backgroundColor = UIColor.black
    }
}

extension SendWalletSelectionViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let protectedRange = NSMakeRange(0, 0)
        let intersection = NSIntersectionRange(protectedRange, range)
        
        if range.location == 10 {
            return false
        }
        
        if intersection.length > 0 {
            return false
        }
        
        return true
    }
    
}
