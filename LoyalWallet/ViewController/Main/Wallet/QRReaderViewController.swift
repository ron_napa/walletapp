//
//  QRReaderViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 24/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SwiftQRCode
import AVFoundation
import AudioToolbox
import SwiftyJSON
import SwiftSpinner

class QRReaderViewController: MainViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    @IBOutlet weak var cameraView: UIView!
    
    var captureDevice:AVCaptureDevice?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var captureSession:AVCaptureSession?
    var selectedType = SendType.wallet
    var inputedValue : String = ""
    var timer: Timer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        navigationItem.title = "Scanner"
        view.backgroundColor = .white
        startTimer()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        captureDevice = AVCaptureDevice.default(for: .video)
        // Check if captureDevice returns a value and unwrap it
        
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            displayQRReader()
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if granted {
                    self.displayQRReader()
                } else {
                    let refreshAlert = UIAlertController(title: "LoyalWallet", message: "Please accept the camera permission for QR reader. After you changed the camera permission, LoyalWallet will automatically restart.", preferredStyle: UIAlertControllerStyle.alert)
                    refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
                        if let url = URL(string: UIApplicationOpenSettingsURLString) {
                            // If general location settings are enabled then open location settings for the app
                            UIApplication.shared.openURL(url)
                        }
                    }))
                    refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
                        //print("Handle Cancel Logic here")
                        self.stopTimer()
                        self.navigationController?.popViewController(animated: true)
                    }))
                    self.present(refreshAlert, animated: true, completion: nil)
                    UserSession.sharedInstance.allAlertController.append(refreshAlert)
                }
            })
        }
        
    }
    //MARK: - Private Functions
    func displayQRReader() {
        if let captureDevice = self.captureDevice {
            do {
                let input = try AVCaptureDeviceInput(device: captureDevice)
                
                self.captureSession = AVCaptureSession()
                guard let captureSession = self.captureSession else { return }
                captureSession.addInput(input)
                
                let captureMetadataOutput = AVCaptureMetadataOutput()
                captureSession.addOutput(captureMetadataOutput)
                
                captureMetadataOutput.setMetadataObjectsDelegate(self, queue: .main)
                captureMetadataOutput.metadataObjectTypes = [.code128, .qr, .ean13,  .ean8, .code39] //AVMetadataObject.ObjectType
                
                captureSession.startRunning()
                
                self.videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                self.videoPreviewLayer?.videoGravity = .resize
                self.videoPreviewLayer?.frame = CGRect(x: 0, y: 0, width: Device.Size.width, height: Device.Size.height)
                self.videoPreviewLayer?.backgroundColor = UIColor.black.cgColor
                //view.layer.addSublayer(videoPreviewLayer!)
                self.cameraView.layer.addSublayer(self.videoPreviewLayer!)
            } catch {
                //print("Error Device Input")
            }
        }
    }
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count == 0 {
            return
        }
        
        let metadataObject = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        guard let stringCodeValue = metadataObject.stringValue else { return }
        
        //view.addSubview(codeFrame)
        
        guard let barcodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObject) else { return }
        
        // Call the function which performs navigation and pass the code string value we just detected
        parseJSONFromQR(scannedString: stringCodeValue)
        
        print(stringCodeValue)
        
    }
    
    func parseJSONFromQR(scannedString : String) {
        
        if let userCode:JSON = JSON.init(parseJSON: scannedString){
            if let lylAddress = userCode["lylAddress"].string {
                inputedValue = lylAddress
                
                if let currentUser = RealmManager.realm?.objects(User.self).first {
                    if currentUser.mobileNo == inputedValue || currentUser.lylAddress == inputedValue {
                        AlertviewManager.showAlertInfo(message: "You can't transfer LYL to yourself.", view: self)
                        return
                    }
                }
                
                captureSession?.stopRunning()
                
                SwiftSpinner.show("")
                UserService.customerValidity(type: selectedType.rawValue, value: inputedValue, completion: { (response, error) in
                    SwiftSpinner.hide()
                    if error != nil {
                        //AlertviewManager.showAlertInfo(message: (error?.description)!, view: self)
                    } else {
                        if response!["code"].int != 200 {
                            AlertviewManager.showAlertInfo(message: response!["message"].string!, view: self)
                        } else {
                            self.stopTimer()
                            self.performSegue(withIdentifier: "sendConfirmationSegue", sender: self)
                        }
                    }
                })
            } else {
                AlertviewManager.showAlertInfo(message: "Invalid QR Code.", view: self)
            }
        } else {
            AlertviewManager.showAlertInfo(message: "Invalid QR Code.", view: self)
        }
        
        //performSegue(withIdentifier: "sendConfirmationSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "sendConfirmationSegue" {
            let confirmViewController = segue.destination as! SendWalletConfirmationViewController
            confirmViewController.selectedType = selectedType
            confirmViewController.inputtedValue = inputedValue
        }
    }
    
    //MARK: - Private Functions
    func startTimer() {
        
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 30, target: self, selector: #selector(self.popView), userInfo: nil, repeats: true)
        }
    }
    
    func stopTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
    @objc func popView() {
        stopTimer()
        let refreshAlert = UIAlertController(title: "LoyalWallet", message: "Timeout occured.", preferredStyle: UIAlertControllerStyle.alert)
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            self.navigationController?.popViewController(animated: true)
        }))
        present(refreshAlert, animated: true, completion: nil)
        UserSession.sharedInstance.allAlertController.append(refreshAlert)
    }
    
}

