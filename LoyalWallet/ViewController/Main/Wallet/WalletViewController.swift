//
//  WalletViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 13/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftSpinner

protocol WalletViewControllerDelegate {
    func walletViewController(_ viewController: WalletViewController, didTapActionButton index: Int)
    func walletViewController(_ viewController: WalletViewController, didSelectTransaction index: Int)
    func walletViewController(_ viewController: WalletViewController, updatedTableViewHeight check: Bool)
}

class WalletViewController: UIViewController {

    @IBOutlet weak var userBalanceLabel: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    var delegate: WalletViewControllerDelegate?
    @IBOutlet weak var showMoreButton: UIButton!
    var currentTableHeight:CGFloat = 0.0
    let cellHeight:CGFloat = 80.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadUserTransactions()
        
        PushForegroundHelper.sharedInstance.delegate = self
        
        self.scrollView.spr_setIndicatorHeader { [weak self] in
            self?.clearTransactions()
            self?.loadUserTransactions()
        }
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            userBalanceLabel.text = "\(currentUser.lylBalance.format(f: ".6"))"
            userBalanceLabel.adjustsFontSizeToFitWidth = true
        }
        
        if UserSession.sharedInstance.userShouldRefreshTrans {
            self.clearTransactions()
            self.loadUserTransactions()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: - Private Functions
    func updateDisplay() {
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            userBalanceLabel.text = "\(currentUser.lylBalance.format(f: ".6"))"
            userBalanceLabel.adjustsFontSizeToFitWidth = true
        }
    }
    
    func clearTransactions() {
        UserSession.sharedInstance.userTransactionCount = 0
        UserSession.sharedInstance.userTransactionPage = 0
        UserSession.sharedInstance.userTransactionHistory.removeAll()
        self.tableView.reloadData()
    }
    
    func loadUserTransactions() {
        
        UserSession.sharedInstance.userShouldRefreshTrans = false
        currentTableHeight = 0.0
        
        UserService.getUserTransactions(count: UserSession.sharedInstance.userTransactionPerPage * UserSession.sharedInstance.userTransactionPage, completion: {(response, error) in
            print(response as Any)
            
            self.scrollView.spr_endRefreshing()
            
            if let error = error {
                print(error)
            } else {
                
                UserSession.sharedInstance.userTransactionCount = (response?["count"].int)!
                UserSession.sharedInstance.userTransactionPage += 1
                
                let transactionsHistory = response?["transactionsHistory"]
                if let transactionsArray = transactionsHistory?.array {
                    for transaction in transactionsArray {
                        let newTransaction = UserTransaction(info: transaction.dictionaryObject!)
                        UserSession.sharedInstance.userTransactionHistory.append(newTransaction)
                    }
                    self.tableView.reloadData()
                    self.updateTableViewConstraints()
                }
                
                if UserSession.sharedInstance.userTransactionCount
                    <= UserSession.sharedInstance.userTransactionPerPage * UserSession.sharedInstance.userTransactionPage {
                    self.showMoreButton.isHidden = true
                } else {
                    self.showMoreButton.isHidden = false
                }
                
            }
        })
    }
    
    func updateTableViewConstraints() {
        
        if UserSession.sharedInstance.userTransactionHistory.count <= 5 {
            let difference = CGFloat(5 - UserSession.sharedInstance.userTransactionHistory.count)
            self.tableViewHeightConstraint.constant = (CGFloat(UserSession.sharedInstance.userTransactionHistory.count) * cellHeight) + (((Device.Size.height/2) / 5 ) * CGFloat(difference))
            self.tableView.updateConstraints()
        } else {
            self.tableViewHeightConstraint.constant = CGFloat(UserSession.sharedInstance.userTransactionHistory.count) * cellHeight
            self.tableView.updateConstraints()
        }
        
    }
    
    //MARK: - User Interactions
    @IBAction func didTapSend(_ sender: Any) {
        delegate?.walletViewController(self, didTapActionButton: 0)
    }
    
    @IBAction func didTapReceive(_ sender: Any) {
        delegate?.walletViewController(self, didTapActionButton: 1)
    }
    
    @IBAction func didTapShowMore(_ sender: Any) {
        loadUserTransactions()
    }

}

extension WalletViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let userTransactionCell = tableView.dequeueReusableCell(withIdentifier: "userTransactionCell", for: indexPath) as! UserTransactionCell
        let userTransactions = UserSession.sharedInstance.userTransactionHistory
        userTransactionCell.transactionAmount.adjustsFontSizeToFitWidth = true
        
        if userTransactions.count == 0 {
            return userTransactionCell
        }
        
        let currentTransaction = userTransactions[indexPath.row]
        
        var transactionAmount = Int(currentTransaction.lylAmount.roundToInt()).formatUsingAbbrevation()
        
        if currentTransaction.lylAmount >= 999 {
            transactionAmount = Int(currentTransaction.lylAmount.roundToInt()).formatUsingAbbrevation()
        } else {
            if currentTransaction.lylAmount < 1 {
                transactionAmount = currentTransaction.lylAmount.format(f: "6")
            } else {
                transactionAmount = "\(currentTransaction.lylAmount.cleanValue)"
            }
        }
        
        switch currentTransaction.transactionType {
        case TransactionType.send.rawValue :
            if let currentUser = RealmManager.realm?.objects(User.self).first {
                if currentTransaction.customerId == currentUser.id {
                    userTransactionCell.transactionDescription.text = "You sent LYL to \(currentTransaction.receiverFirstName!) \(currentTransaction.receiverLastName!)"
                    userTransactionCell.transactionAmount.text = "- LYL \(transactionAmount)"
                    userTransactionCell.transactionAmount.textColor = UIColor.red
                } else {
                    userTransactionCell.transactionDescription.text = "You received LYL from \(currentTransaction.senderFirstName!) \(currentTransaction.senderLastName!)"
                    userTransactionCell.transactionAmount.text = "+ LYL \(transactionAmount)"
                    userTransactionCell.transactionAmount.textColor = UIColor.successColor
                }
            }
        case TransactionType.purchase.rawValue :
            userTransactionCell.transactionDescription.text = "You earned LYL from \(currentTransaction.merchantName!) \(currentTransaction.branchName!)"
            userTransactionCell.transactionAmount.text = "+ LYL \(transactionAmount)"
            userTransactionCell.transactionAmount.textColor = UIColor.successColor
        case TransactionType.redeem.rawValue :
            userTransactionCell.transactionDescription.text = "You redeemed \(currentTransaction.rewardName!)"
            userTransactionCell.transactionAmount.text = "- LYL \(transactionAmount)"
            userTransactionCell.transactionAmount.textColor = UIColor.red
        default:
            userTransactionCell.transactionDescription.text = ""
            userTransactionCell.transactionAmount.text = ""
            userTransactionCell.transactionAmount.textColor = UIColor.black
        }
        userTransactionCell.transactionDate.text = currentTransaction.transactionDate?.dateValue()?.stringValue(format: "MM/dd/yy")                                                                                                                                                      
        userTransactionCell.transactionStatus.text = "Successful"
        userTransactionCell.selectionStyle = .none
        
        return userTransactionCell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return UserSession.sharedInstance.userTransactionHistory.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let transactions = UserSession.sharedInstance.userTransactionHistory
        let currentTransaction = transactions[indexPath.row]
        UserSession.sharedInstance.selectedTransaction = currentTransaction
        delegate?.walletViewController(self, didSelectTransaction: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }

}

extension WalletViewController : PushForegroundHelperDelegate {
    func pushForegroundHelper(shouldRefreshWallet: Bool) {
        clearTransactions()
        loadUserTransactions()
    }
    
    func pushForegroundHelper(didUpdateBalance: Bool) {
        clearTransactions()
        loadUserTransactions()
        self.updateDisplay()
    }
    
}
