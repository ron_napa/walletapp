//
//  SendWalletSuccessViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 23/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SwiftySound

class SendWalletSuccessViewController: UIViewController {

    @IBOutlet weak fileprivate var successMessageLabel: UILabel!
    
    var receiverName = ""
    var updatedBalance = ""
    var sentAmount = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Sound.play(file: "notification_sound", fileExtension: "mp3", numberOfLoops: 0)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        var updateString = self.successMessageLabel.text?.replacingOccurrences(of: "lylAmount", with: "LYL \(sentAmount)")
        updateString = updateString?.replacingOccurrences(of: "lylReceiver", with: "\(receiverName)")
        updateString = updateString?.replacingOccurrences(of: "lylBalance", with: "LYL \(updatedBalance.doubleValue.format(f: ".6"))")
        self.successMessageLabel.text = updateString
        
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            try! RealmManager.realm?.write {
                currentUser.lylBalance = updatedBalance.doubleValue.format(f: ".6").doubleValue
            }
        }
    }
    
    //Private Functions
    
    //User Interactions
    @IBAction func didTapBackMainHome(_ sender: Any) {
        UserSession.sharedInstance.userShouldRefreshTrans = true
        for controller in (self.navigationController?.viewControllers)! {
            if controller.isKind(of: MainHomeViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }

}
