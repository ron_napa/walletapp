//
//  VerificationPinViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 19/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SwiftSpinner

class VerificationPinViewController: MainRegisterViewController {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var pin1TextField: UITextField!
    @IBOutlet weak var pin2TextField: UITextField!
    @IBOutlet weak var pin3TextField: UITextField!
    @IBOutlet weak var pin4TextField: UITextField!
    @IBOutlet weak var pin5TextField: UITextField!
    @IBOutlet weak var pin6TextField: UITextField!
    
    var pinArray : [UITextField] = []
    var currentInput = ""
    
    var isResetPassword : Bool = false
    var haveResend : Bool = false
    var resetEmail = ""
    var verificationType : VerificationType = VerificationType.phone
    var loginCustomerID = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.adjustsFontSizeToFitWidth = true
        pinArray = [pin1TextField,
                    pin2TextField,
                    pin3TextField,
                    pin4TextField,
                    pin5TextField,
                    pin6TextField]
        
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            loginCustomerID = currentUser.id
        }
        
        LoginHelper.clearUserSession()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let verificationString = verificationType == .email ? "EMAIL" : "SMS"
        
        titleLabel.setAttributedText(primaryString: "Enter the verification code sent to you via ", textColor: UIColor.black, font: UIFont(name: "OpenSans-Regular", size: 14.0)!, secondaryString: verificationString, secondaryTextColor: UIColor.black, secondaryFont: UIFont(name: "OpenSans-Bold", size: 14.0)!)
        
        UserSession.sharedInstance.isMobileVerified = true
        
    }
    
    //MARK: Private Functions
    func displayCurrentCode() {
        var index = 0
        while index < currentInput.count {
            pinArray[index].text = "\(currentInput.character(at: index)!)"
            index = index + 1
        }
    }
    
    func clearCode() {
        for pins in pinArray {
            pins.text = ""
        }
        currentInput = ""
    }
    
    //MARK: User Interactions
    @IBAction func didTapNumber(_ sender: Any) {
        
        let numberButton = sender as? UIButton
        
        if currentInput.count <= 5 {
            currentInput += (numberButton?.titleLabel?.text)!
            displayCurrentCode()
        } else {
            
        }
        
        if currentInput.count == 6 {
            //request for verify code
            
            SwiftSpinner.show("")
            
            if isResetPassword {
                RegisterService.verifyPasswordCode(verificationCode: currentInput) { (response, error) in
                    print(response as Any)
                    SwiftSpinner.hide()
                    if error == nil {
                        SwiftSpinner.hide()
                        if response!["code"].int == 200 {
                            UserDefaults.standard.set("\(response!["token"])", forKey: R.CacheKeys.userToken)
                            UserDefaults.standard.synchronize()
                            self.performSegue(withIdentifier: "resetPasswordSegue", sender: self)
                        } else {
                            AlertviewManager.showAlertInfo(message: response!["message"].string!, view: self)
                            self.clearCode()
                        }
                    }
                }
                return
            }
            
            RegisterService.verifyCode(verificationCode: currentInput, completion: { (response, error) in
                print(response as Any)
                
                if error != nil {
                    SwiftSpinner.hide()
                    //AlertviewManager.showAlertInfo(message: (error?.description)!, view: self)
                } else {
                    
                    if response!["code"].int == 200 {
                        if !self.isResetPassword {
                            
                            UserService.getUserProfile(customerId: "\(self.loginCustomerID)", completion: { (response, error) in
                                print(response as Any)
                                SwiftSpinner.hide()
                                if error == nil {
                                    let userResponse = response!["customer"]
                                    
                                    let user = User()
                                    user.id = userResponse["cust_id"].int ?? 0
                                    user.mobileNo = userResponse["mobile_no"].string ?? ""
                                    user.email = userResponse["email"].string ?? ""
                                    user.profileImage = userResponse["profile_image"].string ?? ""
                                    user.activationCode = userResponse["activation_code"].string ?? ""
                                    user.pinCode = userResponse["pin_code"].string ?? ""
                                    user.lylAddress = userResponse["lyl_address"].string ?? ""
                                    user.lylBalance = userResponse["lyl_balance"].double ?? 0.0
                                    user.firstName = userResponse["first_name"].string ?? ""
                                    user.middleName = userResponse["middle_name"].string ?? ""
                                    user.lastName = userResponse["last_name"].string ?? ""
                                    user.gender = userResponse["gender"].string ?? ""
                                    user.dateOfBirth = userResponse["dateofbirth"].string ?? ""
                                    user.birthPlace = userResponse["birthplace"].string ?? ""
                                    user.nationality = userResponse["nationality"].string ?? ""
                                    user.registeredLocation = userResponse["reg_location"].string ?? ""
                                    user.lastTransactionDate = userResponse["last_transaction_date"].string ?? ""
                                    user.phoneVerified = userResponse["phone_verified"].int == 1 ? true : false
                                    user.emailVerified = userResponse["email_verified"].int == 1 ? true : false
                                    user.addressVerified = userResponse["address_verified"].int == 1 ? true : false
                                    user.selfieVerified = userResponse["selfie_verified"].int == 1 ? true : false
                                    user.indentityVerified = userResponse["indentity_verified"].int == 1 ? true : false
                                    
                                    RealmManager.save(object: user)
                                    
                                    self.performSegue(withIdentifier: "createPinSegue", sender: self)
                                    
                                }
                            })
                            
                        } else {
                            SwiftSpinner.hide()
                            self.performSegue(withIdentifier: "resetPasswordSegue", sender: self)
                        }
                    } else {
                        SwiftSpinner.hide()
                        AlertviewManager.showAlertInfo(message: response!["message"].string!, view: self)
                        self.clearCode()
                    }
                }
            })

        }
        
    }
    
    @IBAction func didTapClear(_ sender: Any) {
        clearCode()
    }
    
    @IBAction func didTapResendCode(_ sender: Any) {
        
        SwiftSpinner.show("")
        
        if !haveResend && isResetPassword && verificationType == .email {
            haveResend = true
            AccountVerificationService.sendEmailCode(email: resetEmail, completion: { (response, error) in
                SwiftSpinner.hide()
                print(response as Any)
                if error != nil {
                    //AlertviewManager.showAlertInfo(message: (error?.description)!, view: self)
                } else {
                    if response!["code"].int == 200 {
                        AlertviewManager.showAlertInfo(message: "Code resent successfully.", view: self)
                    } else {
                        AlertviewManager.showAlertInfo(message: response!["message"].string!, view: self)
                    }
                }
            })
            return
        }
        
        RegisterService.resendCode( completion: { (response, error) in
            print(response as Any)
            SwiftSpinner.hide()
            if error != nil {
                //AlertviewManager.showAlertInfo(message: (error?.description)!, view: self)
            } else {
                if response!["code"].int == 200 {
                    AlertviewManager.showAlertInfo(message: "Code resent successfully.", view: self)
                } else {
                    AlertviewManager.showAlertInfo(message: response!["message"].string!, view: self)
                }
            }
        })
    }
    
}
