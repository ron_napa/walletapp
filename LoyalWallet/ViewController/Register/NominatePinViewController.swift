//
//  NominatePinViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 10/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SwiftSpinner
import Realm
import RealmSwift

class NominatePinViewController: MainViewController {
    
    @IBOutlet weak var pin1TextField: UITextField!
    @IBOutlet weak var pin2TextField: UITextField!
    @IBOutlet weak var pin3TextField: UITextField!
    @IBOutlet weak var pin4TextField: UITextField!
    
    @IBOutlet weak var logoutDetailView: UIView!
    @IBOutlet weak var userNumberLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    var pinArray : [UITextField] = []
    var currentInput = ""
    var nominatedPIN = ""
    
    var isLockOut = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pinArray = [pin1TextField,
                    pin2TextField,
                    pin3TextField,
                    pin4TextField]
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        logoutDetailView.isHidden = true
        if let activeUser = RealmManager.realm?.objects(User.self).first,
            activeUser.pinCode?.count == 4 {
            messageLabel.text = "Please enter your PIN"
            logoutDetailView.isHidden = false
            self.userNumberLabel.text = activeUser.mobileNo
        } else {
            if nominatedPIN.count != 0 {
                messageLabel.text = "Confirm PIN"
            } else {
                nominatedPIN = ""
                messageLabel.text = "Create your PIN"
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Private Functions
    func loginUser() {
        if let currentUser = RegisterSession.sharedInstance.currentUser {
            RegisterService.login(mobileNumber: currentUser.mobileNumber!, password: currentUser.password!, completion: { (response, error) in
                
                SwiftSpinner.hide()
                if error != nil {
                    //AlertviewManager.showAlertInfo(message: (error?.description)!, view: self)
                } else {
                    if response!["code"].int == 200 {
                        let userResponse = response!["user"]
                        let user = User(info: userResponse.dictionaryValue)
                        RealmManager.save(object: user)
                        
                        self.showMainDashBoard()
                    } else {
                        AlertviewManager.showAlertInfo(message: response!["message"].string!, view: self)
                    }
                }
            })
        }
    }
    
    func showMainDashBoard() {
        let vc = R.Storyboards.main.instantiateViewController(withIdentifier: "SWRevealView")
        Utils.setRootViewController(vc)
    }
    
    func displayCurrentCode() {
        
        var index = 0
        
        while index < currentInput.count {
            pinArray[index].text = "*"
            index = index + 1
        }
    }
    
    func clearInput() {
        for pins in pinArray {
            pins.text = ""
        }
        currentInput = ""
    }
    
    func logoutClearPin() {
        let refreshAlert = UIAlertController(title: "LoyalWallet", message: "Account logged out, please re-login and create new PIN.", preferredStyle: UIAlertControllerStyle.alert)
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            SwiftSpinner.show("")
            RegisterService.clearPin { (response, error) in
                SwiftSpinner.hide()
                print(response as Any)
                if error != nil {
                    
                } else {
                    LoginHelper.clearUserSession()
                    self.redirectToLogin()
                }
            }
        }))
        present(refreshAlert, animated: true, completion: nil)
        UserSession.sharedInstance.allAlertController.append(refreshAlert)
    }
    
    func redirectToLogin() {
        let vc = R.Storyboards.register.instantiateViewController(withIdentifier: "RegisterCarouselViewController") as! RegisterCarouselViewController
        UserSession.sharedInstance.willShowLogin = true
        let navController = UINavigationController(rootViewController: vc)
        Utils.setRootViewController(navController)
    }
    //MARK: User Interactions
    @IBAction func didTapNumber(_ sender: Any) {
        
        let numberButton = sender as? UIButton
        
        if currentInput.count <= 3 {
            currentInput += (numberButton?.titleLabel?.text)!
            displayCurrentCode()
        } else {
            
        }
        
        if currentInput.count == 4 {
            
            if let activeUser = RealmManager.realm?.objects(User.self).first,
                activeUser.pinCode?.count == 4 {
                print(activeUser)
                if self.currentInput == activeUser.pinCode {
                    if isLockOut {
                        self.dismiss(animated: false, completion: nil)
                        for alertView in UserSession.sharedInstance.allAlertController {
                            alertView.view.transform = CGAffineTransform.init(translationX: -500, y: -500)
                            alertView.dismiss(animated: false, completion: nil)
                        }
                    } else {
                        showMainDashBoard()
                    }
                    UserDefaults.standard.set(0 , forKey: R.CacheKeys.exhaustCount + activeUser.mobileNo!)
                    UserDefaults.standard.synchronize()
                } else {
                    clearInput()
                    if let exhaustCount = UserDefaults.standard.object(forKey: R.CacheKeys.exhaustCount + activeUser.mobileNo!) as? Int {
                        
                        let newCount = exhaustCount + 1
                        
                        if newCount >= R.ErrorCount.exhaustLimit {
                            self.logoutClearPin()
                        } else {
                            let attemp = R.ErrorCount.exhaustLimit - newCount == 1 ? "Attempt" : "Attempts"
                            AlertviewManager.showAlertInfo(message: "Invalid pin. \(R.ErrorCount.exhaustLimit - newCount) \(attemp) Left", view: self)
                        }
                        
                        UserDefaults.standard.set(newCount , forKey: R.CacheKeys.exhaustCount + activeUser.mobileNo!)
                        UserDefaults.standard.synchronize()
                    } else {
                        UserDefaults.standard.set(1 , forKey: R.CacheKeys.exhaustCount + activeUser.mobileNo!)
                        UserDefaults.standard.synchronize()
                        AlertviewManager.showAlertInfo(message: "Invalid pin. 4 Attempts Left", view: self)
                    }
                    //AlertviewManager.showAlertInfo(message: "Wrong PIN", view: self)
                }
            } else {
                if self.nominatedPIN.count == 0 {
                    let vc = R.Storyboards.register.instantiateViewController(withIdentifier: "NominatePinViewController") as! NominatePinViewController
                    vc.nominatedPIN = self.currentInput
                    self.navigationController?.pushViewController(vc, animated: true)
                } else {
                    if self.currentInput == self.nominatedPIN {
                        //request for verify code
                        SwiftSpinner.show("")
                        RegisterService.createPin(pinCode: currentInput, completion: { (response, error) in
                            print(response as Any)
                            if error != nil {
                                SwiftSpinner.hide()
                                //AlertviewManager.showAlertInfo(message: (error?.description)!, view: self)
                            } else {
                                LoginHelper.loginUser(viewController: self, willVerify: false)
//                                if let _ = RealmManager.realm?.objects(User.self).first {
//                                    LoginHelper.updateUserInfo(viewController: self, completion: { (response, error) in
//                                        if error != nil {
//
//                                            } else {
//                                            LoginHelper.showMainDashBoard()
//                                        }
//                                    })
//                                } else {
//                                    LoginHelper.loginUser(viewController: self)
//                                }
                            }
                        })
                    } else {
                        clearInput()
                        AlertviewManager.showAlertInfo(message: "PIN did not match, please try again!", view: self)
                    }
                }
            }
        }
    
    }
    
    @IBAction func didTapClear(_ sender: Any) {
        clearInput()
    }
    
    @IBAction func didTapLogout(_ sender: Any) {
        let refreshAlert = UIAlertController(title: "LoyalWallet", message: "Are you sure you want to log out?", preferredStyle: UIAlertControllerStyle.alert)
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action: UIAlertAction!) in
            SwiftSpinner.show("")
            RegisterService.logout { (response, error) in
                SwiftSpinner.hide()
                print(response as Any)
                if error != nil {
                    
                } else {
                    
                    LoginHelper.clearUserSession()
                    self.redirectToLogin()
                    
                }
            }
        }))
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action: UIAlertAction!) in
            //print("Handle Cancel Logic here")
        }))
        present(refreshAlert, animated: true, completion: nil)
        UserSession.sharedInstance.allAlertController.append(refreshAlert)
    }
}
