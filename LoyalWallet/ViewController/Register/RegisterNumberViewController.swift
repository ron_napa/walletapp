//
//  RegisterNumberViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 10/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftSpinner

class RegisterNumberViewController: MainRegisterViewController {

    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var mobileNumberLineView: UIView!
    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Private Functions
    
    func createAccount() {
        
        let inputtedMobileNumber = "+63" + numberTextField.text!
        
        if inputtedMobileNumber.count == 3 &&
            passwordTextField.text!.count == 0 {
            AlertviewManager.showAlertInfo(message: "Please fill in required fields.", view: self)
            return
        }
        
        if inputtedMobileNumber.count == 3 {
            AlertviewManager.showAlertInfo(message: "Mobile Number is required.", view: self)
            return
        }
        
        if passwordTextField.text!.count == 0 {
            AlertviewManager.showAlertInfo(message: "Password is required.", view: self)
            return
        }
        
        if !inputtedMobileNumber.matchExists(for: R.RegexPattern.patternMobileNumber) {
            AlertviewManager.showAlertInfo(message: "Please provide valid mobile number.", view: self)
            return
        }
        
        if !passwordTextField.text!.matchExists(for: R.RegexPattern.patternPasswordWithSpecial) ||
            (passwordTextField.text?.matches(for: "(?:\\s)").count)! > 0 {
            AlertviewManager.showAlertInfo(message: "Please provide correct password format.", view: self)
            return
        }
        
        RegisterSession.sharedInstance.currentUser?.mobileNumber = inputtedMobileNumber
        RegisterSession.sharedInstance.currentUser?.password     = passwordTextField.text!
        RegisterSession.sharedInstance.currentUser?.type         = RegisterSession.sharedInstance.loginType.rawValue
        
        LoginHelper.createAccount(viewController: self)
        
    }
    
    //MARK: - User Interactions
    @IBAction func didTapCreateAccount(_ sender: Any) {
        createAccount()
    }
    
    @IBAction func didTapTerms(_ sender: Any) {
        let vc = R.Storyboards.main.instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as! TermsAndConditionsViewController
        vc.isModal = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func didStartEdit(_ sender: Any) {
        mobileNumberLineView.backgroundColor = UIColor.defaultYellow
    }
    
    @IBAction func didStopEdit(_ sender: Any) {
        mobileNumberLineView.backgroundColor = UIColor.black
    }
    
}
