//
//  MainRegisterViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 15/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class MainRegisterViewController: UIViewController {
    
    @IBOutlet weak var mainTitleLabel: UILabel!
    @IBOutlet weak var mainPasswordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var mainPasswordShowButton: UIButton!
    
    var passwordShown = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapPasswordVisibility(_ sender: Any) {
        if !passwordShown {
            passwordShown = true
            //mainPasswordShowButton.setImage(UIImage(named: "icon_not_visible"), for: UIControlState.normal)
            mainPasswordShowButton.setBackgroundImage(UIImage(named: "icon_not_visible"), for: UIControlState.normal)
            mainPasswordTextField.isSecureTextEntry = false
        } else {
            passwordShown = false
            //mainPasswordShowButton.setImage(UIImage(named: "icon_visible"), for: UIControlState.normal)
            mainPasswordShowButton.setBackgroundImage(UIImage(named: "icon_visible"), for: UIControlState.normal)
            mainPasswordTextField.isSecureTextEntry = true
        }
    }
    
}
