//
//  CreateAccountViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 19/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftSpinner

class CreateAccountViewController: MainRegisterViewController {

    @IBOutlet weak fileprivate var mobileNumberTextField: UITextField!
    @IBOutlet weak var mobileNumberLineView: UIView!
    @IBOutlet weak fileprivate var passwordTextField: SkyFloatingLabelTextField!
    @IBOutlet weak fileprivate var firstNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak fileprivate var lastNameTextField: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserSession.sharedInstance.willShowCreate = false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Private Functions
    func makePrefix() {
        let attributedString = NSMutableAttributedString(string: "+63 | ")
        mobileNumberTextField.attributedText = attributedString
    }
    
    func createAccount() {
        
        let inputtedMobileNumber = "+63" + mobileNumberTextField.text!
        
        if inputtedMobileNumber.count == 3 &&
           passwordTextField.text!.count == 0 &&
           firstNameTextField.text!.count == 0 &&
           lastNameTextField.text!.count == 0 {
            AlertviewManager.showAlertInfo(message: "Please fill in required fields.", view: self)
            return
        }
        
        if inputtedMobileNumber.count == 3 {
            AlertviewManager.showAlertInfo(message: "Mobile Number is required.", view: self)
            return
        }
        
        if passwordTextField.text!.count == 0 {
            AlertviewManager.showAlertInfo(message: "Password is required.", view: self)
            return
        }
        
        if firstNameTextField.text!.count == 0 {
            AlertviewManager.showAlertInfo(message: "First Name is required.", view: self)
            return
        }
        
        if lastNameTextField.text!.count == 0 {
            AlertviewManager.showAlertInfo(message: "Last Name is required.", view: self)
            return
        }
        
        if !inputtedMobileNumber.matchExists(for: R.RegexPattern.patternMobileNumber) {
            AlertviewManager.showAlertInfo(message: "Please provide valid mobile number.", view: self)
            return
        }
        
        if !firstNameTextField.text!.matchExists(for: R.RegexPattern.patternName){
            AlertviewManager.showAlertInfo(message: "Please provide valid first name.", view: self)
            return
        }
        
        if !lastNameTextField.text!.matchExists(for: R.RegexPattern.patternName){
            AlertviewManager.showAlertInfo(message: "Please provide valid last name.", view: self)
            return
        }
        
        if !passwordTextField.text!.matchExists(for: R.RegexPattern.patternPasswordWithSpecial) ||
            (passwordTextField.text?.matches(for: "(?:\\s)").count)! > 0 {
            AlertviewManager.showAlertInfo(message: "Please provide correct password format.", view: self)
            return
        }
        
        RegisterSession.sharedInstance.currentUser = RegisterUser.init(inputtedMobileNumber,
                                                                       passwordTextField.text!,
                                                                       LoginType.phone.rawValue,
                                                                       firstNameTextField.text!,
                                                                       lastNameTextField.text!,
                                                                       "",
                                                                       "",
                                                                       "",
                                                                       "",
                                                                       "")
        
        LoginHelper.createAccount(viewController: self)
        
    }
    
    //MARK: - User Interactions
    @IBAction func didTapCreateAccount(_ sender: Any) {
        createAccount()
    }
    
    @IBAction func didTapTerms(_ sender: Any) {
        
        let vc = R.Storyboards.main.instantiateViewController(withIdentifier: "TermsAndConditionsViewController") as! TermsAndConditionsViewController
        vc.isModal = true
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func didStartEdit(_ sender: Any) {
        mobileNumberLineView.backgroundColor = UIColor.defaultYellow
    }
    
    @IBAction func didStopEdit(_ sender: Any) {
        mobileNumberLineView.backgroundColor = UIColor.black
    }
    
}
