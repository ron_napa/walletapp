//
//  SignInSelectionViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 15/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit

@objc protocol SignInSelectionViewControllerDelegate {
    @objc optional func signInSelectionViewController(_ signViewController: SignInSelectionViewController, didVerifyOTP: Bool)
    func signInSelectionViewController(_ signViewController: SignInSelectionViewController, actionID: Int)
}

class SignInSelectionViewController: MainRegisterViewController {

    var socialHelper : SocialLoginHelper?
    var delegate: SignInSelectionViewControllerDelegate?
    @IBOutlet weak var signUpWithFBLabel: UILabel!
    @IBOutlet weak var signUpWithGoogleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        socialHelper = SocialLoginHelper(self)
        socialHelper?.delegate = self
        RegisterSession.sharedInstance.isNewUser = false
        
        signUpWithFBLabel.adjustsFontSizeToFitWidth = true
        signUpWithGoogleLabel.adjustsFontSizeToFitWidth = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        RegisterSession.sharedInstance.existingUserPhone = ""
    }
    
    //MARK: - User Interactions
    @IBAction func didTapSignFacebook(_ sender: Any) {
        socialHelper?.loginWithFacebook()
    }
    
    @IBAction func didTapSignGoogle(_ sender: Any) {
        socialHelper?.loginWithGoogle()
    }
    
    @IBAction func didTapSignIn(_ sender: Any) {
        delegate?.signInSelectionViewController(self, actionID: 0)
    }
    
    @IBAction func didTapCreateAccount(_ sender: Any) {
        delegate?.signInSelectionViewController(self, actionID: 1)
        RegisterSession.sharedInstance.loginType = LoginType.phone
    }
    
}

extension SignInSelectionViewController: SocialLoginHelperDelegate {
    func socialLoginHelper(_ helper: SocialLoginHelper) {
        //delegate?.signInSelectionViewController(self, actionID: 2)
        //performSegue(withIdentifier: "registerAccountSegue", sender: self)
    }
    
    func socialLoginHelper(_ helper: SocialLoginHelper, socialIdExist: Bool) {
        if socialIdExist {
            delegate?.signInSelectionViewController(self, actionID: 0)
        } else {
            delegate?.signInSelectionViewController(self, actionID: 2)
        }
    }
    
}

