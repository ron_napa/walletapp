//
//  LoginViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 19/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftSpinner

class LoginViewController: MainRegisterViewController {

    @IBOutlet weak var userMobileNumberTextField: UITextField!
    @IBOutlet weak var mobileNumberLineView: UIView!
    @IBOutlet weak var userPasswordTextField: SkyFloatingLabelTextField!
    
    var testMobile : [String] = ["+639985905579",
                                 "+639566092090",
                                 "+639199542583"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //LoginHelper.clearUserSession()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if !UserSession.sharedInstance.isMobileVerified {
            if let currentUser = RealmManager.realm?.objects(User.self).first {
                SwiftSpinner.show("")
                LoginHelper.verifyMobile(mobileNumber: currentUser.mobileNo!, viewController: self)
            }
        }
        
        if !RegisterSession.sharedInstance.existingUserPhone.isEmpty {
            userMobileNumberTextField.text = RegisterSession.sharedInstance.existingUserPhone
        }
        
        userPasswordTextField.text = ""
        
    }
    
    //MARK: Private Functions
    func loginUser() {
        
        let inputtedMobileNumber = "+63" + userMobileNumberTextField.text!
        
        if inputtedMobileNumber.count == 3 &&
           userPasswordTextField.text!.count == 0 {
            AlertviewManager.showAlertInfo(message: "Please fill in required fields.", view: self)
            return
        }
        
        if inputtedMobileNumber.count == 3 {
            AlertviewManager.showAlertInfo(message: "Mobile Number is required.", view: self)
            return
        }
        
        if !inputtedMobileNumber.matchExists(for: R.RegexPattern.patternMobileNumber) {
            AlertviewManager.showAlertInfo(message: "Please enter valid mobile number.", view: self)
            return
        }
        
        if userPasswordTextField.text!.count == 0 {
            AlertviewManager.showAlertInfo(message: "Password is required.", view: self)
            return
        }
        
        SwiftSpinner.show("")
        
        RegisterSession.sharedInstance.existingUserPhone = ""
        RegisterSession.sharedInstance.currentUser = RegisterUser.init(inputtedMobileNumber,
                                                                       userPasswordTextField.text!,
                                                                       "",
                                                                       "",
                                                                       "",
                                                                       "",
                                                                       "",
                                                                       "",
                                                                       "",
                                                                       "")
        
        //Debug to True
        for mobileNum in testMobile {
            if inputtedMobileNumber == mobileNum {
                LoginHelper.loginUser(viewController: self, willVerify: false)
                return
            }
        }
        
        LoginHelper.loginUser(viewController: self, willVerify: true)
        
    }
    
    //MARK: User Interactions
    @IBAction func didTapSignIn(_ sender: Any) {
        loginUser()
    }
    
    @IBAction func didTapForgotPassword(_ sender: Any) {
        
    }
    
    @IBAction func didTapSignUP(_ sender: Any) {
        //UserSession.sharedInstance.willShowCreate = true
        self.navigationController?.popViewController(animated: false)
    }
    
    @IBAction func didStartEdit(_ sender: Any) {
        mobileNumberLineView.backgroundColor = UIColor.defaultYellow
    }
    
    @IBAction func didStopEdit(_ sender: Any) {
        mobileNumberLineView.backgroundColor = UIColor.black
    }
    
}
