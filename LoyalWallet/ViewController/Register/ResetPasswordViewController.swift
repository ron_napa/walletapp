//
//  ResetPasswordViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 30/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftSpinner

class ResetPasswordViewController: MainRegisterViewController {

    @IBOutlet weak var passwordTextField: SkyFloatingLabelTextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Private Functions
    func resetPassword() {
        print(UserDefaults.standard.object(forKey: R.CacheKeys.userToken))
        
        SwiftSpinner.show("")
        RegisterService.resetPassword(newPassword: passwordTextField.text!, completion: { (response, error) in
            SwiftSpinner.hide()
            print(response as Any)
            if error != nil {
                //AlertviewManager.showAlertInfo(message: (error?.description)!, view: self)
            } else {
                if response!["code"].int == 200 {
                    self.performSegue(withIdentifier: "resetPasswordResultSegue", sender: self)
                } else {
                    AlertviewManager.showAlertInfo(message: response!["message"].string!, view: self)
                }
                
            }
        })
    }
    
    //MARK: - User Interactions
    @IBAction func didTapSendCode(_ sender: Any) {
        for controller in (self.navigationController?.viewControllers)! {
            if controller.isKind(of: ForgotPasswordViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    @IBAction func didTapResetPassword(_ sender: Any) {
        
        if passwordTextField.text?.count == 0 {
            AlertviewManager.showAlertInfo(message: "Please provide your new password.", view: self)
            return
        }
        
        if !passwordTextField.text!.matchExists(for: R.RegexPattern.patternPasswordWithSpecial) ||
            (passwordTextField.text?.matches(for: "(?:\\s)").count)! > 0 {
            AlertviewManager.showAlertInfo(message: "Invalid password format.", view: self)
            return
        }
        
        resetPassword()
    }
    
}
