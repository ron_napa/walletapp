//
//  RegisterCarouselViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 11/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import iCarousel

class RegisterCarouselViewController: MainRegisterViewController {

    @IBOutlet weak var carouselView: iCarousel!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var skipButton: UIButton!
    
    let titleString : [String] = ["LoyalCoalition", "LoyalWallet", "LoyalCoin"]
    let messageString : [String] = ["A coalition of forward-looking enterprises that will support the issuance of LoyalCoin as customer reward", "The primary consumer hub for earning and spending LYL as well as holding supported currencies", "The digital asset to be circulated within the economy"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.setTheme(color: UIColor.white)
        
        carouselView.delegate = self
        carouselView.dataSource = self
        carouselView.bounces = false
        carouselView.type = .linear
        carouselView.isPagingEnabled = true
        
        //Auto Skip
        //carouselView.scrollToItem(at: 3, animated: false)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserSession.sharedInstance.willShowLogin {
            UserSession.sharedInstance.willShowLogin = false
            self.performSegue(withIdentifier: "signInSegue", sender: self)
        }
        
        if UserSession.sharedInstance.willShowCreate {
            UserSession.sharedInstance.willShowCreate = false
            self.performSegue(withIdentifier: "createAccountSegue", sender: self)
        }

        if !UserSession.sharedInstance.isMobileVerified {
            self.performSegue(withIdentifier: "signInSegue", sender: self)
        }
        
        RegisterSession.sharedInstance.existingUserPhone = ""
        
    }
    
    //MARK: Private Functions
    
    //MARK: User Interactions
    @IBAction func didTapSkip(_ sender: Any) {
        carouselView.scrollToItem(at: 3, animated: true)
    }

}

//MARK: - iCarouselDataSource
extension RegisterCarouselViewController : iCarouselDataSource {
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return 4
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        if index == 3 {
            let login = storyboard?.instantiateViewController(withIdentifier: "SignInSelectionViewController") as! SignInSelectionViewController
            login.delegate = self
            return login.view
        }
        
        let contentView = storyboard?.instantiateViewController(withIdentifier: "RegisterCarouselContentViewController") as! RegisterCarouselContentViewController
        contentView.view.bounds = carousel.bounds
        contentView.contentImageView.image = UIImage(named: "splash_\(index+1)")
        contentView.contentTitleLabel.text = titleString[index]
        contentView.contentMessageLabel.text = messageString[index]
        
        return contentView.view!
        
    }
    
}

//MARK: - iCarouselDelegate
extension RegisterCarouselViewController : iCarouselDelegate {
    
    func carouselDidEndScrollingAnimation(_ carousel: iCarousel) {
        
        pageControl.currentPage = carousel.currentItemIndex
        
        if carousel.currentItemIndex != 3 {
            skipButton.isHidden = false
        } else {
            skipButton.isHidden = true
        }
        
    }
}

extension RegisterCarouselViewController : SignInSelectionViewControllerDelegate {
    
    func signInSelectionViewController(_ signViewController: SignInSelectionViewController, actionID: Int) {
        
        switch actionID {
        case 0:
            self.performSegue(withIdentifier: "signInSegue", sender: self)
        case 1:
            self.performSegue(withIdentifier: "createAccountSegue", sender: self)
        case 2:
            self.performSegue(withIdentifier: "registerAccountSegue", sender: self)
        default:
            self.performSegue(withIdentifier: "signInSegue", sender: self)
        }
        
    }
    
}
