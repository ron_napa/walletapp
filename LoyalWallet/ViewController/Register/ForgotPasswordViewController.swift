//
//  ForgotPasswordViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 30/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import SwiftSpinner

class ForgotPasswordViewController: MainRegisterViewController {

    
    @IBOutlet weak var userInputtedTextField: SkyFloatingLabelTextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UserDefaults.standard.set("", forKey: R.CacheKeys.userToken)
        UserDefaults.standard.synchronize()
    }
    
    //MARK: Private Functions
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "verificationPinSegue" {
            let verificationPinVC = segue.destination as? VerificationPinViewController
            verificationPinVC?.isResetPassword = true
            verificationPinVC?.resetEmail = userInputtedTextField.text!
            if userInputtedTextField.text!.matchExists(for: R.RegexPattern.patternMobileNumber) {
                verificationPinVC?.verificationType = VerificationType.phone
            } else {
                verificationPinVC?.verificationType = VerificationType.email
            }
        }
    }
    
    func backToMainScreen() {
        for controller in (self.navigationController?.viewControllers)! {
            if controller.isKind(of: RegisterCarouselViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
    func sendVerificationCode() {
        
        SwiftSpinner.show("")
        RegisterService.mobileSendCode(mobileNumber: userInputtedTextField.text!, completion: { (response, error) in
            SwiftSpinner.hide()
            print(response as Any)
            if error == nil {
                if response!["code"].int == 200 {
                    UserDefaults.standard.set("\(response!["token"])", forKey: R.CacheKeys.userToken)
                    UserDefaults.standard.synchronize()
                    self.performSegue(withIdentifier: "verificationPinSegue", sender: self)
                } else {
                    AlertviewManager.showAlertInfo(message: response!["message"].string!, view: self)
                }
            }
        })
        
    }
    
    func sendEmailVerificationCode() {
        
        SwiftSpinner.show("")
        AccountVerificationService.sendEmailCode(email: userInputtedTextField.text!, completion: { (response, error) in
            SwiftSpinner.hide()
            print(response as Any)
            if error == nil {
                if response!["code"].int == 200 {
                    UserDefaults.standard.set("\(response!["token"])", forKey: R.CacheKeys.userToken)
                    UserDefaults.standard.synchronize()
                    self.performSegue(withIdentifier: "verificationPinSegue", sender: self)
                } else {
                    AlertviewManager.showAlertInfo(message: response!["message"].string!, view: self)
                }
            }
        })
    }
    
    //MARK: User Interactions
    @IBAction func didTapSignUp(_ sender: Any) {
        backToMainScreen()
    }
    
    @IBAction func didTapSendCode(_ sender: Any) {
        
        userInputtedTextField.text = userInputtedTextField.text!.trim()
        if !userInputtedTextField.text!.matchExists(for: R.RegexPattern.patternMobileNumber) &&
            !userInputtedTextField.text!.matchExists(for: R.RegexPattern.patternEmail) {
            AlertviewManager.showAlertInfo(message: "Please enter valid email address or mobile number.", view: self)
            return
        }
        
        if userInputtedTextField.text?.count == 0 {
            AlertviewManager.showAlertInfo(message: "Please enter your email address or mobile number.", view: self)
            return
        }
        
        sendVerificationCode()
        
//        if userInputtedTextField.text!.matchExists(for: R.RegexPattern.patternEmail) {
//            sendEmailVerificationCode()
//            return
//        }
        
    }
    
}
