//
//  ResetPasswordResultViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 30/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit

class ResetPasswordResultViewController: MainRegisterViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK: Private Functions
    
    //MARK: User Interactions
    @IBAction func didTapBackToSignIn(_ sender: Any) {
        for controller in (self.navigationController?.viewControllers)! {
            if controller.isKind(of: LoginViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    
}
