//
//  TermsAndConditionsViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 26/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import WebKit
import SwiftSpinner

class TermsAndConditionsViewController: MainViewController {

    @IBOutlet weak var webView: WKWebView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var qrShowButton: UIButton!
    var isModal = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupWebView(link: "https://loyalcoin.io/terms")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let _ = RealmManager.realm?.objects(User.self).first {
            
        } else {
            qrShowButton.isHidden = true
        }
        
    }
    
    //MARK: - Private Functions
    
    //MARK: - User Interactions
    func setupWebView(link: String) {
        loadingIndicator.isHidden = false
        let url = URL(string: link)
        let request = URLRequest(url: url! as URL)
        webView.load(request)
        webView.addObserver(self, forKeyPath: "loading", options: .new, context: nil)
        //webView.addObserver(self, forKeyPath: #keyPath(WKWebView.loading), options: .new, context: nil)

    }

    // To handle spinner stop;  was testing other stuff, could simplify
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        guard let _ = object as? WKWebView else { return }
        guard let keyPath = keyPath else { return }
        guard let change = change else { return }
        
        switch keyPath {
        case "loading":
            if let val = change[NSKeyValueChangeKey.newKey] as? Bool {
                //do something!
                loadingIndicator.isHidden = true
            }
        default:
            break
        }
    }
    
    @IBAction func didTapClose(_ sender: Any) {
        
        if isModal {
            self.navigationController?.popViewController(animated: true)
        } else {
            self.dismiss(animated: false, completion: nil)
        }
        
    }

}
