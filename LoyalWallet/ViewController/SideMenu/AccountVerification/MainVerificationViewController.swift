//
//  MainVerificationViewController.swift
//  GongchaWallet
//
//  Created by Ronald Napa on 09/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit

class MainVerificationViewController: UIViewController {

    @IBOutlet weak var mainTitleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func didTapBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapQRCode(_ sender: Any) {
        let supportView =  Bundle.main.loadNibNamed("UserQRView", owner: nil, options: nil)?[0] as! UserQRView
        let viewController = UIViewController()
        viewController.view = supportView
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: false, completion: nil)
    }
}
