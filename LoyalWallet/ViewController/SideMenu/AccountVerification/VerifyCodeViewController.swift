//
//  VerifyCodeViewController.swift
//  GongchaWallet
//
//  Created by Ronald Napa on 09/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SwiftSpinner

class VerifyCodeViewController: MainVerificationViewController {
    
    @IBOutlet weak var pin1TextField: UITextField!
    @IBOutlet weak var pin2TextField: UITextField!
    @IBOutlet weak var pin3TextField: UITextField!
    @IBOutlet weak var pin4TextField: UITextField!
    @IBOutlet weak var pin5TextField: UITextField!
    @IBOutlet weak var pin6TextField: UITextField!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    var pinArray : [UITextField] = []
    var currentInput = ""
    var inputtedEmail = ""
    var verificationType: VerificationType = VerificationType.email
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.adjustsFontSizeToFitWidth = true
        pinArray = [pin1TextField,
                    pin2TextField,
                    pin3TextField,
                    pin4TextField,
                    pin5TextField,
                    pin6TextField]
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let verificationString = verificationType == .email ? "EMAIL" : "SMS"
        
        titleLabel.setAttributedText(primaryString: "Enter the verification code sent to you via ", textColor: UIColor.black, font: UIFont(name: "OpenSans-Regular", size: 14.0)!, secondaryString: verificationString, secondaryTextColor: UIColor.black, secondaryFont: UIFont(name: "OpenSans-Bold", size: 14.0)!)
        
    }
    
    //MARK: Private Functions
    func displayCurrentCode() {
        
        var index = 0
        
        while index < currentInput.count {
            pinArray[index].text = "\(currentInput.character(at: index)!)"
            index = index + 1
        }
    }
    
    func clearCode() {
        for pins in pinArray {
            pins.text = ""
        }
        currentInput = ""
    }
    
    func sendEmailCode() {
        SwiftSpinner.show("")
        AccountVerificationService.sendEmailCode(email: inputtedEmail, completion: { (response, error) in
            SwiftSpinner.hide()
            if error != nil {
                //AlertviewManager.showAlertInfo(message: (error?.description)!, view: self)
            } else {
                if response!["code"].int == 200 {
                    AlertviewManager.showAlertInfo(message: "Code resent successfully.", view: self)
                } else {
                    AlertviewManager.showAlertInfo(message: response!["message"].string!, view: self)
                }
            }
        })
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "verificationResultSegue" {
            let vc = segue.destination as? VerificationResultViewController
            vc?.verificationType = verificationType
        }
    }
    
    //MARK: User Interactions
    @IBAction func didTapNumber(_ sender: Any) {
        
        let numberButton = sender as? UIButton
        
        if currentInput.count <= 5 {
            currentInput += (numberButton?.titleLabel?.text)!
            displayCurrentCode()
        } else {
            
        }
        
        if currentInput.count == 6 {
            //request for verify code
            
            if verificationType == .email {
                SwiftSpinner.show("")
                AccountVerificationService.verifyEmailCode(code: currentInput, completion: { (response, error) in
                    print(response as Any)
                    SwiftSpinner.hide()
                    if error != nil {
                        //AlertviewManager.showAlertInfo(message: (error?.description)!, view: self)
                    } else {
                        if response!["code"].int != 200 {
                            AlertviewManager.showAlertInfo(message: response!["message"].string!, view: self)
                            self.clearCode()
                        } else {
                            self.performSegue(withIdentifier: "verificationResultSegue", sender: self)
                        }
                    }
                })
            } else {
                SwiftSpinner.show("")
                RegisterService.verifyCode(verificationCode: currentInput, completion: { (response, error) in
                    print(response as Any)
                    SwiftSpinner.hide()
                    if error != nil {
                        //AlertviewManager.showAlertInfo(message: (error?.description)!, view: self)
                    } else {
                        if response!["code"].int != 200 {
                            AlertviewManager.showAlertInfo(message: response!["message"].string!, view: self)
                            self.clearCode()
                        } else {
                            self.performSegue(withIdentifier: "verificationResultSegue", sender: self)
                        }
                    }
                })
            }
        }
    }
    
    @IBAction func didTapClear(_ sender: Any) {
        clearCode()
    }
    
    @IBAction func didTapResendCode(_ sender: Any) {
        sendEmailCode()
    }
    
}
