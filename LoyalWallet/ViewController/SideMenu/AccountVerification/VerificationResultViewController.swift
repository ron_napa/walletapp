//
//  VerificationResultViewController.swift
//  GongchaWallet
//
//  Created by Ronald Napa on 09/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SwiftSpinner

class VerificationResultViewController: MainVerificationViewController {

    var verificationType = VerificationType.email
    @IBOutlet weak var verificationTitleLabel: UILabel!
    @IBOutlet weak var verificationMessageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        verificationTitleLabel.text = verificationType.rawValue
        let verificationString = verificationType == .email ? "email" : "sms"
        let newString = verificationMessageLabel.text?.replacingOccurrences(of: "credential", with: verificationString)
        verificationMessageLabel.text = newString
    }
    
    //MARK: - Private Functions
    
    func getUserProfile() {
        SwiftSpinner.show("")
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            UserService.getUserProfile(customerId: "\(currentUser.id)", completion: { (response, error) in
                print(response as Any)
                SwiftSpinner.hide()
                if error != nil {
                    //AlertviewManager.showAlertInfo(message: (error?.description)!, view: self)
                } else {
                    
                    let userResponse = response!["customer"]
                    
                    try! RealmManager.realm?.write {
                        currentUser.mobileNo = userResponse["mobile_no"].string ?? ""
                        currentUser.email = userResponse["email"].string ?? ""
                        currentUser.profileImage = userResponse["profile_image"].string ?? ""
                        currentUser.activationCode = userResponse["activation_code"].string ?? ""
                        currentUser.pinCode = userResponse["pin_code"].string ?? ""
                        currentUser.lylAddress = userResponse["lyl_address"].string ?? ""
                        currentUser.lylBalance = userResponse["lyl_balance"].double ?? 0.0
                        currentUser.firstName = userResponse["first_name"].string ?? ""
                        currentUser.middleName = userResponse["middle_name"].string ?? ""
                        currentUser.lastName = userResponse["last_name"].string ?? ""
                        currentUser.gender = userResponse["gender"].string ?? ""
                        currentUser.dateOfBirth = userResponse["dateofbirth"].string ?? ""
                        currentUser.birthPlace = userResponse["birthplace"].string ?? ""
                        currentUser.nationality = userResponse["nationality"].string ?? ""
                        currentUser.registeredLocation = userResponse["reg_location"].string ?? ""
                        currentUser.lastTransactionDate = userResponse["last_transaction_date"].string ?? ""
                        currentUser.phoneVerified = userResponse["phone_verified"].int == 1 ? true : false
                        currentUser.emailVerified = userResponse["email_verified"].int == 1 ? true : false
                        currentUser.addressVerified = userResponse["address_verified"].int == 1 ? true : false
                        currentUser.selfieVerified = userResponse["selfie_verified"].int == 1 ? true : false
                        currentUser.indentityVerified = userResponse["indentity_verified"].int == 1 ? true : false
                    }
                    
                    self.backToMainScreen()
                    
                }
            })
        }
    }
    
    func backToMainScreen() {
        
        if RegisterSession.sharedInstance.isHomeVerification {
            for controller in (self.navigationController?.viewControllers)! {
                if controller.isKind(of: MainHomeViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        } else {
            for controller in (self.navigationController?.viewControllers)! {
                if controller.isKind(of: AccountVerificationViewController.self) {
                    self.navigationController!.popToViewController(controller, animated: true)
                    break
                }
            }
        }
        
    }
    
    //MARK: - User Interactions
    @IBAction func didTapDone(_ sender: Any) {
        getUserProfile()
    }

}
