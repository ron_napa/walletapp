//
//  IDVerificationsViewController.swift
//  GongchaWallet
//
//  Created by Ronald Napa on 12/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit

class IDVerificationsViewController: MainVerificationViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - User Interactions
    @IBAction func didTapDone(_ sender: Any) {
        performSegue(withIdentifier: "sourceFundsSegue", sender: self)
    }

}
