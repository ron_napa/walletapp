//
//  VerificationCell.swift
//  GongchaWallet
//
//  Created by Ronald Napa on 08/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit

protocol VerificationTypeCellDelegate {
    func didTapVerify(_ sender:VerificationTypeCell, cellTag:Int)
}

class VerificationTypeCell: UITableViewCell {

    @IBOutlet weak var verificationTypeLabel: UILabel!
    @IBOutlet weak var verificationTypeButton: UIButton!
    @IBOutlet weak var verificationFlagLabel: UILabel!
    var delegate: VerificationTypeCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - User Interactions
    @IBAction func didTapVerify(_ sender: Any) {
        let tappedButton = sender as? UIButton
        delegate?.didTapVerify(self, cellTag: (tappedButton?.tag)!)
    }
    
}
