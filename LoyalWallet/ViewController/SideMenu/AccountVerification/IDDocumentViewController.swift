//
//  IDDocumentViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 02/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import Alamofire

class IDDocumentViewController: UIViewController {

    @IBOutlet weak var sampleIdImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Private Functions
    
    //MARK: User Interactions
    @IBAction func didTapUpload(_ sender: Any) {
    
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        imagePicker.allowsEditing = false
        present(imagePicker, animated: true, completion: nil)
 
    }
    
    @IBAction func didTapDone(_ sender: Any) {
        
        //token
        /*    eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaWQiOjE4OCwibW9iaWxlX251bWJlciI6Iis2Mzk5ODU5MDU1NzkiLCJpYXQiOjE1MjI2NDEyMzd9.s48Q9aOgzwRlgDNf5bncrOBZb4vYsRv8G7ESqY6atJA
         */
        
        let params: [String:Any] = ["first_name" : "Ron",
                                    "middle_name" : "Ron",
                                    "last_name" : "Ron",
                                    "gender" : "m",
                                    "dateofbirth" : "07/01/1992",
                                    "birthplace" : "Philippines",
                                    "nationality" : "Philippines",
                                    "emp_status" : "Student",
                                    "occupation" : "Student",
                                    "company" : "Student",
                                    "position" : "Student",
                                    "gov_idcard" : "SSS",
                                    "gov_idcardno" : "12345123",
                                    "gov_expiration" : "10/10/2020"]
        
        
        let url = URL.init(string: "http://api.loyalcoin.io/customer/identityVerification")
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                
                print(params)
                
                DispatchQueue.main.async { // Correct
                    let imageData = UIImagePNGRepresentation(self.sampleIdImageView.image!)
                    multipartFormData.append(imageData!, withName: "gov_image")
                }
                
                for (key, value) in params {
                    multipartFormData.append(key.data(using: .utf8)!, withName: value as! String)
                }
                
                
        },
            to: url!,
            headers:["Authorization": "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjaWQiOjE4OCwibW9iaWxlX251bWJlciI6Iis2Mzk5ODU5MDU1NzkiLCJpYXQiOjE1MjI2NDEyMzd9.s48Q9aOgzwRlgDNf5bncrOBZb4vYsRv8G7ESqY6atJA",
                     "Content-Type": "application/x-www-form-urlencoded"],
            encodingCompletion: { encodingResult in
                
                switch encodingResult {
                case .success(let upload, _, _):
                    upload.responseString { response in
                        print(response)
                        }
                        .uploadProgress { progress in
                            // main queue by default
                            //self.img1Progress.alpha = 1.0
                            //self.img1Progress.progress = Float(progress.fractionCompleted)
                            
                            //print("Upload Progress: \(progress.fractionCompleted)")
                    }
                    return
                case .failure(let encodingError):
                    print(encodingError)
                }
        })
    }
}

extension IDDocumentViewController : UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print(info)
        let selectedImage = info["UIImagePickerControllerOriginalImage"] as! UIImage
        sampleIdImageView.image = selectedImage
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

extension IDDocumentViewController : UINavigationControllerDelegate {
    
}
