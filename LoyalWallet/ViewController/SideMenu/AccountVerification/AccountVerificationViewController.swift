//
//  AccountVerificationViewController.swift
//  GongchaWallet
//
//  Created by Ronald Napa on 08/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit

class AccountVerificationViewController: UIViewController {

    @IBOutlet weak fileprivate var tableView: UITableView!
    let verificationType : [String] = ["Phone Verification",
                                       "Email Verification"]
    var verificationFlag : [Bool] = []
    /*
     "Identity Verification",
     "Selfie Verification",
     "Address Verification"
     */
    
    var selectedType : VerificationType = VerificationType.phone
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = true
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            verificationFlag = [true, currentUser.emailVerified]
            tableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "sendVerificationSegue" {
            let destination = segue.destination as? SendVerificationViewController
            destination?.verificationType = selectedType
        }
    }
    
    //MARK: - User Interactions
    @IBAction func didTapBack(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func didTapQRCode(_ sender: Any) {
        let supportView =  Bundle.main.loadNibNamed("UserQRView", owner: nil, options: nil)?[0] as! UserQRView
        let viewController = UIViewController()
        viewController.view = supportView
        viewController.modalPresentationStyle = .overCurrentContext
        present(viewController, animated: false, completion: nil)
    }

}

extension AccountVerificationViewController : UITableViewDataSource, UITableViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.verificationType.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let typeCell = tableView.dequeueReusableCell(withIdentifier: "verificationTypeCell", for: indexPath) as? VerificationTypeCell
        typeCell?.verificationTypeLabel.text = self.verificationType[indexPath.row]
        typeCell?.verificationTypeButton.tag = indexPath.row
        typeCell?.verificationFlagLabel.isHidden = true
        typeCell?.verificationTypeButton.isHidden = true
        
        if verificationFlag[indexPath.row] {
            typeCell?.verificationFlagLabel.isHidden = false
        } else {
            typeCell?.verificationTypeButton.isHidden = false
        }
        
        typeCell?.delegate = self
        
        return typeCell!
    }
    
}

extension AccountVerificationViewController : VerificationTypeCellDelegate {
    func didTapVerify(_ sender: VerificationTypeCell, cellTag: Int) {
        
        switch cellTag {
        case 0:
            selectedType = VerificationType.phone
            performSegue(withIdentifier: "sendVerificationSegue", sender: self)
        case 1:
            selectedType = VerificationType.email
            performSegue(withIdentifier: "sendVerificationSegue", sender: self)
        case 2:
            selectedType = VerificationType.indentity
            performSegue(withIdentifier: "verificationViewSegue", sender: self)
        case 3:
            selectedType = VerificationType.selfie
            performSegue(withIdentifier: "sendVerificationSegue", sender: self)
        case 4:
            selectedType = VerificationType.address
            performSegue(withIdentifier: "sendVerificationSegue", sender: self)
        default:
            selectedType = VerificationType.phone
            performSegue(withIdentifier: "sendVerificationSegue", sender: self)
        }
    }
}
