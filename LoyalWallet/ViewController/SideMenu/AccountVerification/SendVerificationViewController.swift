//
//  SendVerificationViewController.swift
//  GongchaWallet
//
//  Created by Ronald Napa on 09/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import SwiftSpinner
import SkyFloatingLabelTextField
import Alamofire
import AlamofireImage

class SendVerificationViewController: MainVerificationViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var userInputTextField: SkyFloatingLabelTextField!
    var verificationType: VerificationType = VerificationType.email
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = verificationType.rawValue
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        userInputTextField.isEnabled = true
        
        if let currentUser = RealmManager.realm?.objects(User.self).first {
            if verificationType == .email {
                if currentUser.email?.count != 0 {
                    userInputTextField.text = currentUser.email
                    userInputTextField.isEnabled = false
                }
            } else {
                userInputTextField.text = currentUser.mobileNo
                userInputTextField.isEnabled = false
            }
        }
        
        if verificationType == .email {
            userInputTextField.placeholder = "Email Address"
            userInputTextField.selectedTitle = "Email Address"
            userInputTextField.keyboardType = .emailAddress
        } else {
            userInputTextField.placeholder = "Mobile Number"
            userInputTextField.selectedTitle = "Mobile Address"
            userInputTextField.keyboardType = .phonePad
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - Private Functions
    func sendEmailCode() {
        SwiftSpinner.show("")
        
        
        if verificationType == .email {
            AccountVerificationService.sendEmailCode(email: userInputTextField.text!, completion: { (response, error) in
                SwiftSpinner.hide()
                print(response as Any)
                if error != nil {
                    //AlertviewManager.showAlertInfo(message: (error?.description)!, view: self)
                } else {
                    if response!["code"].int != 200 {
                        if response!["code"].int == 400 {
                            if "Please Provide valid email to send code" == response!["message"].string!{
                                AlertviewManager.showAlertInfo(message: "Please provide valid email address.", view: self)
                            } else {
                                AlertviewManager.showAlertInfo(message: "Email address provided is already registered.", view: self)
                            }
                        } else {
                            AlertviewManager.showAlertInfo(message: response!["message"].string!, view: self)
                        }
                    } else {
                        self.performSegue(withIdentifier: "verifyCodeSegue", sender: self)
                    }
                }
            })
        } else {
            RegisterService.mobileSendCode(mobileNumber: userInputTextField.text!, completion: { (response, error) in
                SwiftSpinner.hide()
                if error != nil {
                    //AlertviewManager.showAlertInfo(message: (error?.description)!, view: self)
                } else {
                    if response!["code"].int != 200 {
                        if self.verificationType == .email {
                            AlertviewManager.showAlertInfo(message: "Email address provided is already registered.", view: self)
                        } else {
                            AlertviewManager.showAlertInfo(message: "Mobile number provided is already registered.", view: self)
                        }
                    } else {
                        self.performSegue(withIdentifier: "verifyCodeSegue", sender: self)
                    }
                }
            })
        }
    
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "verifyCodeSegue" {
            let verifyCode = segue.destination as! VerifyCodeViewController
            verifyCode.inputtedEmail = userInputTextField.text!
            verifyCode.verificationType = verificationType
        }
    }
    
    //MARK: - User Interactions
    @IBAction func didTapRequest(_ sender: Any) {
        
        if verificationType == .email {
            userInputTextField.text = userInputTextField.text!.trim()
            if !userInputTextField.text!.matchExists(for: R.RegexPattern.patternEmail) {
                AlertviewManager.showAlertInfo(message: "Please provide valid email address.", view: self)
                return
            }
        } else {
            if !userInputTextField.text!.matchExists(for: R.RegexPattern.patternMobileNumber) {
                AlertviewManager.showAlertInfo(message: "Please provide valid mobile number.", view: self)
                return
            }
        }
        
        //request
        sendEmailCode()
    }
    
    @IBAction func didTapResend(_ sender: Any) {
        
    }
    
}
