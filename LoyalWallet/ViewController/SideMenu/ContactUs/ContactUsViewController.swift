//
//  ContactUsViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 26/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsViewController: MainVerificationViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func didTapMailTo(_ sender: Any) {
        
        if MFMailComposeViewController.canSendMail() {
            let picker = MFMailComposeViewController()
            picker.mailComposeDelegate = self
            picker.setToRecipients(["support@loyalcoin.io"])
            picker.setMessageBody("<b>Hi LoyalCoin Team,</b>", isHTML: true)
            picker.setSubject("Customer Assistance")
            self.present(picker, animated: true, completion: nil)
        } else {
            AlertviewManager.showAlertInfo(message: "Email is not available on this device", view: self)
        }

    }
    
    @IBAction func didTapClose(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
}

extension ContactUsViewController : MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true, completion: nil)
    }
}
