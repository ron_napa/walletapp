//
//  AnnouncementCell.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 26/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit

class AnnouncementCell: UITableViewCell {

    @IBOutlet weak var merchantLogoImageView: UIImageView!
    @IBOutlet weak var announcementTitleLabel: UILabel!
    @IBOutlet weak var announcementDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        merchantLogoImageView.layer.cornerRadius = merchantLogoImageView.frame.size.width / 2
        merchantLogoImageView.clipsToBounds = true
        merchantLogoImageView.layer.borderWidth = 1
        merchantLogoImageView.layer.borderColor = UIColor.lightGray.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
