//
//  AnnouncementsListViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 26/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SwiftyJSON
import SwiftSpinner
import Realm
import RealmSwift

class AnnouncementsListViewController: MainVerificationViewController {

    @IBOutlet weak var tableView: UITableView!
    var selectedAnnoucement : Announcement?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if RealmManager.realm!.objects(Announcement.self).count == 0 {
            getAnnouncements()
        }
        
        self.tableView.spr_setIndicatorHeader { [weak self] in
            self?.getAnnouncements()
        }
        
        //self.tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0);
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK : Private Functions
    func getAnnouncements() {
        
        //SwiftSpinner.show("")
        AnnouncementService.getAnnouncements(code: "", completion: { (response, error) in
            //print(response as Any)
            self.tableView.spr_endRefreshing()
            //SwiftSpinner.hide()
            if let error = error {
                print(error)
            } else {
                
                let announcementList = response?["announcements"]
                if let announcementArray = announcementList?.array {
                    for announcement in announcementArray {
                        let newAnnouncement = Announcement(info: announcement.dictionaryObject!)
                        RealmManager.save(object: newAnnouncement)
                    }
                    self.tableView.reloadData()
                }
            }
        })
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "announcementDetailSegue" {
            let announcementDetailVC = segue.destination as? AnnouncementDetailsViewController
            announcementDetailVC?.selectedAnnouncement = selectedAnnoucement
        }
    }
    
    //MARK : User Interactions
    @IBAction func didTapClose(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
}

extension AnnouncementsListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let announcementCell = tableView.dequeueReusableCell(withIdentifier: "announcementCell", for: indexPath) as! AnnouncementCell
        let announcements = RealmManager.realm?.objects(Announcement.self)
        let currentannouncement = announcements![indexPath.row]
        announcementCell.announcementTitleLabel.text = currentannouncement.title
        announcementCell.announcementDescriptionLabel.text = currentannouncement.shortDescription
        announcementCell.selectionStyle = .none
        
        ImageDownloaderManager.downloadImage(urlString: (currentannouncement.logoUrl)!) { (image) in
            announcementCell.merchantLogoImageView.image = image
        }
        
        return announcementCell
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RealmManager.realm!.objects(Announcement.self).count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let announcement = RealmManager.realm?.objects(Announcement.self)
        let currentAnnouncement = announcement![indexPath.row]
        selectedAnnoucement = currentAnnouncement
        self.performSegue(withIdentifier: "announcementDetailSegue", sender: self)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }
    
}
