//
//  AnnouncementDetailsViewController.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 26/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class AnnouncementDetailsViewController: MainVerificationViewController {

    @IBOutlet weak var announcementNameLabel: UILabel!
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var merchantLogoImageView: UIImageView!
    @IBOutlet weak var announcementTitleLabel: UILabel!
    @IBOutlet weak var announcementDescriptionLabel: UILabel!
    
    var selectedAnnouncement : Announcement?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        merchantLogoImageView.layer.cornerRadius = merchantLogoImageView.frame.size.width / 2
        merchantLogoImageView.clipsToBounds = true
        merchantLogoImageView.layer.borderWidth = 1
        merchantLogoImageView.layer.borderColor = UIColor.lightGray.cgColor
        
        updateDisplay()
    }
    
    //MARK : Private Functions
    func updateDisplay() {
        
        announcementNameLabel.text = selectedAnnouncement?.merchantName
        announcementTitleLabel.text = selectedAnnouncement?.title
        announcementDescriptionLabel.text = selectedAnnouncement?.longDescription
        
        ImageDownloaderManager.downloadImage(urlString: (selectedAnnouncement?.logoUrl)!) { (image) in
            self.merchantLogoImageView.image = image
        }
        
        ImageDownloaderManager.downloadImage(urlString: (selectedAnnouncement?.bannerUrl)!) { (image) in
            self.bannerImageView.image = image
        }
        
    }
    
    //MARK : User Interactions
    
}
