//
//  Announcement.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 26/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class Announcement: Object {
    
    @objc dynamic var code : Int = 0
    @objc dynamic var merchantCode : Int = 0
    @objc dynamic var name : String? = ""
    @objc dynamic var title : String? = ""
    @objc dynamic var shortDescription : String? = ""
    @objc dynamic var longDescription : String? = ""
    @objc dynamic var bannerUrl : String? = ""
    @objc dynamic var logoUrl : String? = ""
    @objc dynamic var merchantName : String? = ""
    
    convenience init(info: [String: Any]) {
        self.init()
        
        code = info["ann_code"] as? Int ?? 0
        merchantCode = info["merchant_code"] as? Int ?? 0
        name = info["name"] as? String ?? ""
        title = info["title"] as? String ?? ""
        shortDescription = info["short_description"] as? String ?? ""
        longDescription = info["description"] as? String ?? ""
        bannerUrl = info["banner_url"] as? String ?? ""
        logoUrl = info["logo_url"] as? String ?? ""
        merchantName = info["merchant_name"] as? String ?? ""
        
    }
    
    override static func primaryKey() -> String? {
        return "code"
    }
}
