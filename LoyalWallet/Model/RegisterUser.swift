//
//  RegisterUser.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 19/03/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class RegisterUser: NSObject {
    
    @objc dynamic var id : String? = ""
    @objc dynamic var mobileNumber : String? = ""
    @objc dynamic var password : String?  = ""
    @objc dynamic var type : String? = LoginType.phone.rawValue
    @objc dynamic var firstName : String? = ""
    @objc dynamic var lastname : String? = ""
    @objc dynamic var socialId : String? = ""
    @objc dynamic var dateOfBirth : String? = "" //MM/DD/YYYY
    @objc dynamic var gender : String? = ""
    @objc dynamic var imageURL : String? = ""
    @objc dynamic var email : String? = ""
    
    init(_ mobileNumber: String,_ password: String,_ type: String,_ firstName: String,_ lastname: String,_ socialId: String,_ dateOfBirth: String,_ gender: String,_ imageURL: String,_  email: String) {
        
        self.mobileNumber = mobileNumber
        self.password = password
        self.type = type
        self.firstName = firstName
        self.lastname = lastname
        self.socialId = socialId
        self.dateOfBirth = dateOfBirth
        self.gender = gender
        self.imageURL = imageURL
        self.email = email
        
    }
    
    init(info: [String: Any]) {
        self.firstName = info["first_name"] as? String ?? ""
        self.lastname = info["last_name"] as? String ?? ""
        self.socialId = info["id"] as? String ?? ""
        self.gender = info["gender"] as? String ?? ""
        self.email = info["email"] as? String ?? ""
        
        let pictureLib = info["picture"] as? Dictionary<String, Any>
        let pictureData = pictureLib!["data"] as? Dictionary<String, Any>
        self.imageURL = pictureData!["url"] as? String ?? ""
    }
    
}
