//
//  User.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 03/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Foundation
import RealmSwift

class User : Object {
    
    @objc dynamic var id : Int = 0
    @objc dynamic var mobileNo : String? = ""
    @objc dynamic var email : String?  = ""
    @objc dynamic var profileImage : String? = ""
    @objc dynamic var activationCode : String? = ""
    @objc dynamic var pinCode : String? = ""
    @objc dynamic var lylAddress : String? = ""
    @objc dynamic var lylBalance : Double = 0.0
    @objc dynamic var firstName : String? = ""
    @objc dynamic var middleName : String? = ""
    @objc dynamic var lastName : String? = ""
    @objc dynamic var gender : String? = ""
    @objc dynamic var dateOfBirth : String? = ""
    @objc dynamic var birthPlace : String? = ""
    @objc dynamic var nationality : String? = ""
    @objc dynamic var registeredLocation : String? = ""
    @objc dynamic var lastTransactionDate : String? = ""
    @objc dynamic var indentityVerified : Bool = false
    @objc dynamic var selfieVerified : Bool = false
    @objc dynamic var emailVerified : Bool = false
    @objc dynamic var addressVerified : Bool = false
    @objc dynamic var phoneVerified : Bool = false
    
    convenience init(info: [String: Any]) {
        self.init()
        
        id = info["cust_id"] as? Int ?? 0
        mobileNo = info["mobile_no"] as? String ?? ""
        email = info["email"] as? String ?? ""
        activationCode = info["activation_code"] as? String ?? ""
        pinCode = info["pin_code"] as? String ?? ""
        lylAddress = info["lyl_address"] as? String ?? ""
        lylBalance = info["lyl_balance"] as? Double ?? 0.0
        firstName = info["first_name"] as? String ?? ""
        middleName = info["middle_name"] as? String ?? ""
        lastName = info["last_name"] as? String ?? ""
        gender = info["gender"] as? String ?? ""
        dateOfBirth = info["dateofbirth"] as? String ?? ""
        birthPlace = info["birthplace"] as? String ?? ""
        nationality = info["nationality"] as? String ?? ""
        registeredLocation = info["reg_location"] as? String ?? ""
        lastTransactionDate = info["last_transaction_date"] as? String ?? ""
        
        //Address
        
        /*
         "state" : null,
         "gov_expiration" : null,
         "selfie_image" : null,
         "doc_type" : null,
         "position" : null,
         "postalcode" : null,
         "company" : null,
         "city" : null,
         "permanent_city" : null,
         "street" : null,
         "emp_status" : null,
         "gov_idcardno" : null,
         "doc_expiration" : null,
         "permanent_state" : null,
         "permanent_country" : null,
         "gov_idcard" : null,
         "profile_image" : "",
         "gov_image" : null,
         "last_transaction_loc" : null,
         "idcardno" : null,
         "doc_image" : null,
         "permanent_street" : null,
         "permanent_postalcode" : null,
         "status" : 0,
         "occupation" : null,
         "idcard_type" : null,
         "country" : null
         */
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
