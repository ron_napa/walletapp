//
//  MerchantBranch.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 04/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import RealmSwift

class MerchantBranch: Object {
    
    @objc dynamic var code : Int = 0
    @objc dynamic var merchantCode : Int = 0
    @objc dynamic var name : String? = ""
    @objc dynamic var address : String? = ""
    @objc dynamic var contactNo : String? = ""
    @objc dynamic var bussHours : String? = ""
    @objc dynamic var branchCode : String? = ""
    @objc dynamic var imageUrl : String? = ""
    @objc dynamic var latitude : Double = 0.0
    @objc dynamic var longitude : Double = 0.0
    @objc dynamic var distance : Double = 0.0
    
    convenience init(info: [String: Any]) {
        self.init()
        
        code = info["location_code"] as? Int ?? 0
        merchantCode = info["merchant_code"] as? Int ?? 0
        name = info["name"] as? String ?? ""
        address = info["address"] as? String ?? ""
        contactNo = info["contact_no"] as? String ?? ""
        bussHours = info["buss_hours"] as? String ?? ""
        imageUrl = info["imageurl"] as? String ?? ""
        branchCode = info["branch_code"] as? String ?? ""
        latitude = info["latitude"] as? Double ?? 0.0
        longitude = info["longitude"] as? Double ?? 0.0
        distance = info["distance"] as? Double ?? 0.0
        
    }
    
    override static func primaryKey() -> String? {
        return "code"
    }
}
