//
//  Merchant.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 03/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift

class Merchant : Object {
    
    @objc dynamic var code : Int = 0
    @objc dynamic var status : Int = 0
    @objc dynamic var name : String? = ""
    @objc dynamic var category : String?  = ""
    @objc dynamic var logoUrl : String? = ""
    @objc dynamic var bannerUrl : String? = ""
    @objc dynamic var merchantDescription : String? = ""
    @objc dynamic var shortDescription : String? = ""
    @objc dynamic var totalBranches : Int = 0
    @objc dynamic var totalRewards : Int = 0
    @objc dynamic var distance : Double = 0.0
    @objc dynamic var nearestBranch : String? = ""
    
    convenience init(info: [String: Any]) {
        self.init()
        code = info["merchant_code"] as? Int ?? 0
        status = info["status"] as? Int ?? 0
        name = info["name"] as? String ?? ""
        category = info["category"] as? String ?? ""
        logoUrl = info["logo_url"] as? String ?? ""
        bannerUrl = info["banner_url"] as? String ?? ""
        merchantDescription = info["description"] as? String ?? ""
        shortDescription = info["short_description"] as? String ?? ""
        totalBranches = info["total_branch"] as? Int ?? 0
        totalRewards = info["total_rewards"] as? Int ?? 0
        nearestBranch = info["nearest_branch"] as? String ?? " "
        distance = info["distance"] as? Double ?? 0.0
    }
    
    override static func primaryKey() -> String? {
        return "code"
    }
}
