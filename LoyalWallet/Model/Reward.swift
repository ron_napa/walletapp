//
//  Reward.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 04/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//


import Foundation
import RealmSwift

class Reward : Object {
    
    @objc dynamic var code : Int = 0
    @objc dynamic var merchantCode : Int = 0
    @objc dynamic var name : String? = ""
    @objc dynamic var lylAmount : String? = ""
    @objc dynamic var rewardDescription : String? = ""
    @objc dynamic var terms : String? = ""
    @objc dynamic var imageUrl : String? = ""
    @objc dynamic var logoUrl : String? = ""
    
    convenience init(info: [String: Any]) {
        self.init()
        
        code = info["reward_code"] as? Int ?? 0
        merchantCode = info["merchant_code"] as? Int ?? 0
        name = info["name"] as? String ?? ""
        lylAmount = "\(info["lyl_amount"]! ?? "")" 
        rewardDescription = info["description"] as? String ?? ""
        terms = info["terms"] as? String ?? ""
        imageUrl = info["imageurl"] as? String ?? ""
        logoUrl = info["logo_url"] as? String ?? ""
        
    }
    
    override static func primaryKey() -> String? {
        return "code"
    }
}
