//
//  UserTransaction.swift
//  LoyalWallet
//
//  Created by Ronald Napa on 25/04/2018.
//  Copyright © 2018 Appsolutely Inc. All rights reserved.
//

import UIKit
import RealmSwift

enum TransactionType : String {
    case send = "send"
    case purchase = "purchase"
    case redeem = "Redemption"
}

class UserTransaction: Object {
    
    @objc dynamic var id : Int = 0
    @objc dynamic var referenceId : String? = ""
    @objc dynamic var lylAmount : Double = 0.0
    @objc dynamic var transactionType : String? = ""
    @objc dynamic var transactionAmount : Double  = 0.0
    @objc dynamic var transactionDate : String? = ""
    @objc dynamic var receiverFirstName : String? = ""
    @objc dynamic var receiverLastName : String? = ""
    @objc dynamic var senderFirstName : String? = ""
    @objc dynamic var senderLastName : String? = ""
    @objc dynamic var status : String? = ""
    @objc dynamic var userImage : String? = ""
    @objc dynamic var merchantLogo : String? = ""
    @objc dynamic var branchName : String? = ""
    @objc dynamic var rewardName : String? = ""
    @objc dynamic var merchantName : String? = ""
    @objc dynamic var merchantCode : String? = ""
    @objc dynamic var customerId : Int = 0
    
    convenience init(info: [String: Any]) {
        self.init()
        
        id = info["id"] as? Int ?? 0
        referenceId = info["reference_id"] as? String ?? ""
        lylAmount = info["lyl_amount"] as? Double ?? 0.0
        transactionType = info["transaction_type"] as? String ?? ""
        transactionAmount = info["transaction_amount"] as? Double ?? 0.0
        transactionDate = info["transaction_date"] as? String ?? ""
        receiverFirstName = info["receiver_first_name"] as? String ?? ""
        receiverLastName = info["receiver_last_name"] as? String ?? ""
        senderFirstName = info["sender_first_name"] as? String ?? ""
        senderLastName = info["sender_last_name"] as? String ?? ""
        status = info["status"] as? String ?? ""
        customerId = info["customer_id"] as? Int ?? 0
        userImage = info["user_image"] as? String ?? ""
        merchantLogo = info["merchant_logo"] as? String ?? ""
        merchantName = info["merchant_name"] as? String ?? ""
        merchantCode = info["merchant_code"] as? String ?? ""
        rewardName = info["reward_name"] as? String ?? ""
        branchName = info["branch_name"] as? String ?? ""
        //Address
        
    }
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
